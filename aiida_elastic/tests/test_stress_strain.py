import numpy as np
"""
This is a sketch of the kind of testing we should use, where we isolate the
derivation of the elastic tensor from the QE-workflow. I have provided a simple
example of a failing Al result. I strongly suspect, but do not know, whether the
provided stresses & strains are sufficient to derive the elastic constants, but
currently I'm getting total nonsense, so we should at least get closer to values I
have provided

The following data is based on worknode uuid: 07d78a98-c098-4c38-b0b0-e9949bb4ca3a
"""
def get_test_Al_cubicfcc_strain():
    test_strain = np.array(
      [[[ 9.12732550e-04, -3.68311498e-03, -7.22920790e-03,
          3.85606688e-04, -4.90908375e-03, -3.01535083e-03],
        [ 6.84549413e-04, -2.76233623e-03, -5.42190593e-03,
          2.89205016e-04, -3.68181281e-03, -2.26151312e-03],
        [ 4.56366275e-04, -1.84155749e-03, -3.61460395e-03,
          1.92803344e-04, -2.45454188e-03, -1.50767542e-03],
        [ 2.28183138e-04, -9.20778745e-04, -1.80730198e-03,
          9.64016720e-05, -1.22727094e-03, -7.53837708e-04],
        [-2.28183138e-04,  9.20778745e-04,  1.80730198e-03,
         -9.64016720e-05,  1.22727094e-03,  7.53837708e-04],
        [-4.56366275e-04,  1.84155749e-03,  3.61460395e-03,
         -1.92803344e-04,  2.45454188e-03,  1.50767542e-03],
        [-6.84549413e-04,  2.76233623e-03,  5.42190593e-03,
         -2.89205016e-04,  3.68181281e-03,  2.26151312e-03],
        [-9.12732550e-04,  3.68311498e-03,  7.22920790e-03,
         -3.85606688e-04,  4.90908375e-03,  3.01535083e-03]]])
    return test_strain

def get_test_Al_cubicfcc_stress():
    test_stress = np.array(
      [[[[ 0.59562836,  0.10576853,  0.17696738],
         [ 0.10576853,  0.84864906, -0.0145634 ],
         [ 0.17696738, -0.0145634 ,  1.04047405]],

        [[ 0.44661094,  0.0787012 ,  0.13062929],
         [ 0.0787012 ,  0.63328726, -0.01059156],
         [ 0.13062929, -0.01059156,  0.77553784]],

        [[-0.12651035, -0.02530207, -0.04104231],
         [-0.02530207, -0.18491105,  0.00323631],
         [-0.04104231,  0.00323631, -0.2300723 ]],

        [[-0.26420068, -0.05030993, -0.08076067],
         [-0.05030993, -0.37908972,  0.00617841],
         [-0.08076067,  0.00617841, -0.4685296 ]],

        [[ 0.15549004,  0.02574338,  0.04221915],
         [ 0.02574338,  0.21580311, -0.00338342],
         [ 0.04221915, -0.00338342,  0.2619941 ]],

        [[-0.39953733, -0.07487647, -0.1193022 ],
         [-0.07487647, -0.56929656,  0.00897341],
         [-0.1193022 ,  0.00897341, -0.70198532]],

        [[ 0.29994721,  0.05192808,  0.08576225],
         [ 0.05192808,  0.42233861, -0.00691394],
         [ 0.08576225, -0.00691394,  0.51604453]],

        [[-0.5326674 , -0.0990017 , -0.15681399],
         [-0.0990017 , -0.75567866,  0.0116213 ],
         [-0.15681399,  0.0116213 , -0.93058657]]]])
    return test_stress

def get_computed_Al_cubicfcc_elastic(stresses, strains):
# at time of writing, the 'computed' elastic tensor was:
#array([[51.47763727, 27.95307146, 27.95307146,  0.        ,  0.        ,
#         0.        ],
#       [27.95307146, 51.47763727, 27.95307146,  0.        ,  0.        ,
#         0.        ],
#       [27.95307146, 27.95307146, 51.47763727,  0.        ,  0.        ,
#         0.        ],
#       [ 0.        ,  0.        ,  0.        , 15.47948081,  0.        ,
#         0.        ],
#       [ 0.        ,  0.        ,  0.        ,  0.        , 15.47948081,
#         0.        ],
#       [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
#        15.47948081]])
    error = 2
    C11 = 111.50563094609527 + error
    C12 = 60.460177201642680 + error
    C44 = 33.760609897267635 + error

    computed_elastic = np.array([
       [C11, C12, C12,  0.,  0., 0.],
       [C12, C11, C12,  0.,  0., 0.],
       [C12, C12, C11,  0.,  0., 0.],
       [ 0.,  0.,  0., C44,  0., 0.],
       [ 0.,  0.,  0.,  0., C44, 0.],
       [ 0.,  0.,  0.,  0.,  0., C44]
                               ])
    return computed_elastic

def get_expected_Al_cubicfcc_elastic():
    C11 = 111.50563094609527
    C12 = 60.460177201642680
    C44 = 33.760609897267635

    expected_elastic = np.array([
       [C11, C12, C12,  0.,  0., 0.],
       [C12, C11, C12,  0.,  0., 0.],
       [C12, C12, C11,  0.,  0., 0.],
       [ 0.,  0.,  0., C44,  0., 0.],
       [ 0.,  0.,  0.,  0., C44, 0.],
       [ 0.,  0.,  0.,  0.,  0., C44]
                               ])
    return expected_elastic



def test_Al_cubicfcc_elastictensor():
    strain = get_test_Al_cubicfcc_strain()
    stress = get_test_Al_cubicfcc_stress()

    computed_elastic = get_computed_Al_cubicfcc_elastic(stress, strain)

    expected_elastic = get_expected_Al_cubicfcc_elastic()

    rtol = 5./100. #must be within 5%
    atol = 1e-08
    assert np.allclose(expected_elastic, computed_elastic, rtol=rtol, atol=atol)


