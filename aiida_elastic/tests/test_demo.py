"""
This should be later removed, but I keep it here as a demo test to confirm the user has
pytest installed. It is good to have at least one test that 'should' pass
"""
def f():
    return 3

def test_function():
    assert f() == 3
