from multiprocessing import Pool

import numpy as np
import scipy.optimize as spopt


def gram_schmidt_rows(X):
    '''
    do a gram-schmidt orthonormalization on the rows of X using QR (take Q)
    :param X: 2-dimensional numpy array whose rows will be orthonormalized
    :return: 2-dimensional orthonormal array with the same shape as X
    '''
    Q, R = np.linalg.qr(X.transpose())
    return Q.transpose()


def get_strain_sets(nSets, nStrains, nDims=6, orthonormal=False):
    '''
    generate random lagrangian strain sets
    :param nSets: number of strain sets
    :param nStrains: number of strains in each strain set
    :param nDims: number of strain components in each strain
    :param orthonormal: option to orthonormalize the strain sets
    :return: (nSets * nStrains) x nDims matrix of strain sets
    '''
    strainSets = np.random.rand(nSets * nStrains, nDims) * 2 - 1
    strainSets /= np.linalg.norm(strainSets, axis=1)[:, np.newaxis]
    strainSets = strainSets.reshape(nSets, nStrains, nDims)
    if orthonormal and nStrains > 1:
        pool = Pool()
        orthStrainSets = np.array(pool.map(gram_schmidt_rows, strainSets))
        return orthStrainSets
    else:
        return strainSets


def get_cij_vect(cij, symm_mat):
    '''
    return the cij vector in the order of symmetries given by
        symmMat
    '''
    cij_vect = [[u for u in np.unique(cij * symm) if u][0]
                for symm in symm_mat]
    return np.array(cij_vect)



def norm_constraint_lower(x0, strain_n, num_strains, num_dims, tol):
    '''
    constrain the norm of the strain to be > 1 - tol
    '''
    E = x0.reshape(num_strains, num_dims)
    e = E[strain_n]
    constraints = []
    n = np.linalg.norm(e) - (1 - tol)
    constraints.append(n)
    return min(constraints)


def norm_constraint_upper(x0, strain_n, num_strains, num_dims, tol):
    '''
    constrain the norm of the strain to be < 1 + tol
    '''
    E = x0.reshape(num_strains, num_dims)
    e = E[strain_n]
    constraints = []
    n = (1 + tol) - np.linalg.norm(e)
    constraints.append(n)
    return min(constraints)


def norm_constraint_lower_jac(x0, strain_n, num_strains, num_dims, tol):
    E = x0.reshape(num_strains, num_dims)
    jac = np.zeros((num_strains, num_dims))
    jac[strain_n] = E[strain_n]
    jac_norm = -jac / np.linalg.norm(jac)
    return jac_norm


def get_E_matrix(x0, symm_mat, num_strains, num_dims=6):
    '''
    We build up the experiment matrices E from the linear expressions
        a(gamma) = sum (sum M eta(gamma)) c with E(gamma) = (sum M eta(gamma))
    c is a vector with length N_a (independent elastic constants)
    E(gamma) is a 6 x N_a marix, obtained by multiplication of the symmetry
    matrix symm_mat(alpha, 6x6) for the lauegroup times the components of the
        deformation (6x1)
    In total we will stack num_strains E matrices together to form a
        6N_gamma x N_a matrix
    This is the important E matrix of the article.

    Careful: the way we stack together is a bit non-intuitive from numpy,
        but ok when done consistently, eg. when we stack the stresses
        (first derivatives along gamma)

    test :
    array([[[ 1,  2,  3,  4],
            [ 5,  6,  7,  8]],

           [[11, 12, 13, 14],
            [15, 16, 17, 18]]])

    np.vstack(test.T).T :
    array([[ 1,  5,  2,  6,  3,  7,  4,  8],
          [11, 15, 12, 16, 13, 17, 14, 18]])


    :param x0: strains careful with stacking and reshaping
    :param symm_mat: symmetry matrices for Laue group: (N_a x 6 x 6)
    :param num_strains: number of strains gamma (N_gamma in the article)
    :param nDims: number of strain components in each strain

    '''

    Eta = x0.reshape(num_strains, num_dims)
    o_bar = symm_mat.copy()
    # this is transposed as it is of shape N_a x 6 x num_strains
    E_barT = np.matmul(o_bar, Eta.T)  
    # here we already have the right shape: 6num_strains x N_a
    E_bar_stacked = np.vstack(E_barT.T)  

    return E_bar_stacked


def doe_cost(x0, symm_mat, num_strains, num_dims=6, optimality='D', cij=None,
             normalize=False):
    '''
    1. Experiment matrices E were built in get_E_matrix
    2. Build the relevant ET E product
    3. Scale with cijguess
    4. Return the cost function according to optimality criterion

    :param x0: strains careful with stacking and reshaping
    :param symm_mat: symmetry matrices for Laue group: (N_a x 6 x 6)
    :param num_strains: number of strains gamma (N_gamma in the article)
    :param num_dims: number of strain components in each strain
    '''
    xuse = x0.copy()
    if normalize:
        D_strains = xuse.reshape(num_strains, num_dims)
        for n, strain in enumerate(D_strains):
            D_strains[n] /= np.linalg.norm(strain)

        xuse = D_strains.reshape(num_strains * num_dims)

    E = get_E_matrix(xuse, symm_mat, num_strains, num_dims=num_dims)

    EE = np.matmul(E.T, E)

    if cij is not None:
        o_bar = symm_mat.copy().astype(float)
        cij_vect = get_cij_vect(cij, o_bar)
        EE = EE * np.outer(cij_vect, cij_vect)

    # Do some linalg tricks with the inverse!!!

    if optimality == 'D':
        return 1. / np.max([1e-12, np.abs(np.linalg.det(EE))])

    elif optimality == 'A':
        try:
            EE_inv = np.linalg.inv(EE)

        except np.linalg.LinAlgError as err:
            if 'Singular matrix' in str(err):
                return 10 ** 9
            else:
                raise err

        return np.trace(EE_inv)

    elif optimality == 'E':
        try:
            EE_inv = np.linalg.inv(EE)

        except np.linalg.LinAlgError as err:
            if 'Singular matrix' in str(err):
                return 10 ** 9
            else:
                raise err

        return max(np.linalg.eigvals(EE_inv))


def optimize_locally(x0, symm_mat, num_strains, num_dims=6, cij_guess=None,
                     optimality='D', method='COBYLA', tol=0.01):
    '''
    Locally optimize the strain directions by minimizing a design of experiments
        cost function (i.e. D, A or E optimality)
    :param x0: strains
    :param symm_mat: symmetry matrices for Laue group
    :param num_strains: number of strains gamma
    :param num_dims: number of strain components in each strain
    :param cij_guess: guess for elastic constants matrix
    :param optimality: type of doe optimality, D, A, or E
    :param method: scipy minimization method, must accept constraints
    :tol: tolerance on minimization
    '''
    constraints = []
    for strain_n in range(num_strains):
        constraints += [{'type': 'ineq',
                         'fun': norm_constraint_lower,
                         'args': (strain_n, num_strains, num_dims, tol)},
                        {'type': 'ineq',
                         'fun': norm_constraint_upper,
                         'args': (strain_n, num_strains, num_dims, tol)}]

    args = (symm_mat, num_strains, num_dims, optimality, cij_guess)
    D_minimize = spopt.minimize(doe_cost, x0, args=args, method=method,
                                constraints=constraints, options = {'maxiter': 200, 'rhobeg': 0.001})
    D_strains = D_minimize.x.reshape(num_strains, num_dims)
    for n, strain in enumerate(D_strains):
        D_strains[n] /= np.linalg.norm(strain)

    return D_strains.reshape(num_strains * num_dims)


def optimize_basin_hopping(x0, symm_mat, num_strains, num_dims=6,
                           cij_guess=None, optimality='D', method='COBYLA',
                           tol=0.01, num_itermax=1000000, stepsize=0.2,
                           temp=20., ortho=False, verbosity=False):
    def take_normalized_step(x, dims, self):
        """
        We might be concerned with taking steps that are not isotropic if we
            rescale.
        This however seems not so problematic I would say as anyways the strain
            has his own kind of geometry and it is unclear how to interprete
            homogeneous sampling e.g. in strain space
        """
        x0 = x.copy()

        newx = x0 + (np.random.rand(len(x0)) - 0.5) * self.stepsize

        D_strains = newx.reshape(len(x) / dims, dims)
        for n, strain in enumerate(D_strains):
            D_strains[n] /= np.linalg.norm(strain)

        return D_strains.reshape(len(x))

    def take_normalized_step_ortho(x, dims, self):
        """
        We might be concerned with taking steps that are not isotropic if we
            rescale.
        This however seems not so problematic I would say as anyways the strain
            has his own kind of geometry and it is
            unclear how to interprete homogeneous sampling e.g. in strain space
        """
        x0 = x.copy()

        newx = x0 + (np.random.rand(len(x0)) - 0.5) * self.stepsize

        D_strains = newx.reshape(len(x) / dims, dims)
        for n, strain in enumerate(D_strains):
            D_strains[n] /= np.linalg.norm(strain)

        Xortho = gram_schmidt_rows(D_strains)
        return Xortho.reshape(len(x))

    # initialize random strain vectors
    for n, strain in enumerate(x0):
        x0[n] /= np.linalg.norm(strain)

    x0 = x0.reshape(num_strains * num_dims)

    take_normalized_step.func_defaults = (x0, num_dims, take_normalized_step)
    take_normalized_step.stepsize = stepsize

    take_normalized_step_ortho.func_defaults = (x0, num_dims,
        take_normalized_step_ortho)
    take_normalized_step_ortho.stepsize = stepsize

    constraints = []
    for strain_n in range(num_strains):
        constraints += [{'type': 'ineq',
                         'fun': norm_constraint_lower,
                         'args': (strain_n, num_strains, num_dims, tol)},
                        {'type': 'ineq',
                         'fun': norm_constraint_upper,
                         'args': (strain_n, num_strains, num_dims, tol)}]

    minimizer_kwargs = {'args': (symm_mat, num_strains, num_dims, optimality,
                                 cij_guess),
                        'method': method,
                        'constraints': constraints,
                        'options': {'maxiter': 200},
                        'tol': 0.0001}
    if ortho:
        resbasin = spopt.basinhopping(doe_cost, x0, niter=num_itermax, T=temp,
                                      stepsize=stepsize,
                                      minimizer_kwargs=minimizer_kwargs,
                                      take_step=take_normalized_step_ortho,
                                      accept_test=None, callback=None,
                                      interval=50, disp=verbosity,
                                      niter_success=500)
    else:
        resbasin = spopt.basinhopping(doe_cost, x0, niter=num_itermax, T=temp,
                                      stepsize=stepsize,
                                      minimizer_kwargs=minimizer_kwargs,
                                      take_step=take_normalized_step,
                                      accept_test=None, callback=None,
                                      interval=50, disp=verbosity,
                                      niter_success=500)
    #print resbasin
    D_strains = resbasin.x.reshape(num_strains, num_dims)
    for n, strain in enumerate(D_strains):
        D_strains[n] /= np.linalg.norm(strain)

    return D_strains.reshape(num_strains * num_dims)



