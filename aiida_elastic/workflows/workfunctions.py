from aiida_elastic.utils.elastic_data import ulics, laue_names, elastic_symmetries, olics
from aiida_elastic.utils.elastic_utils import (standardize_pymatgen, symmetry_pymatgen,
                           deform_pymatgen, tensor2voigt)

import numpy as np

from aiida.work.workfunctions import workfunction
from aiida.orm.data.structure import StructureData
from aiida.orm.data.array import ArrayData
from aiida.orm.data.base import Int, Float, Str, Bool

# calcfunction creates nodes
# workfunction returns existing node
# move workfunctions each to one file
# think about moving logic into workfunctions


@workfunction
def get_ulics(laue):
    '''
    Look up the lagrangian ULICS strains for the
        given alue classification
    :param laue: laue classification string
    :return: nx1x6 matrix of lagrangian strain voigt vectors
    '''
    laue = str(laue)
    if laue in laue_names.keys():
        ulics_array = ArrayData()
        ulics_array.set_array('strain_directions', ulics[laue])
        return ulics_array
    else:
        raise ValueError('Invalid Laue classification')

@workfunction
def get_olics(laue):
    '''
    Look up the lagrangian OLICS strains for the
        given alue classification
    :param laue: laue classification string
    :return: nx1x6 matrix of lagrangian strain voigt vectors
    '''
    laue = str(laue)
    if laue in laue_names.keys():
        olics_array = ArrayData()
        olics_array.set_array('strain_directions', olics[laue])
        return olics_array
    else:
        raise ValueError('Invalid Laue classification')

@workfunction
def get_elastic_symmetries(laue):
    '''
    Look up the elastic symmetries for the given
        laue classification
    :param laue: laue classification string
    :return: mx6x6 elastic symmetry matrix
    '''
    laue = str(laue)
    if laue in laue_names.keys():
        symmetries = elastic_symmetries[laue]
        symmetries_data = ArrayData()
        symmetries_data.set_array('symmetries', symmetries)
        return symmetries_data
    else:
        return ValueError('Invalid Laue classification')


@workfunction
def get_all_strains(max_magnitude, num_magnitudes, strain_directions):
    '''
    Consruct all the strains defined by the maximum magnitude, number of
        absolute magnitudes, and strain directions
    :param max_magnitude: maximum strain magnitude (e.g. for 2%, 0.02)
    :param num_magnitudes: the number of magnitudes >0 (e.g. 4 will give 4
        strains > 0 and symmetrically 4 strains < 0)
    :param strain_directions: array of strain directions from e.g. get_ulics
        or get_olics
    :returns: strains data mapping
        {'strains_data': ArrayData('strains'),
         'strain_magnitudes_data': ArrayData('strain_magnitudes')}
    '''
    max_magnitude = np.abs(max_magnitude.value)
    num_magnitudes = num_magnitudes.value
    strain_directions = strain_directions.get_array('strain_directions')

    strain_magnitudes = np.linspace(-max_magnitude, max_magnitude,
                                    num_magnitudes * 2 + 1)
    # remove the 0-magnitude entry
    strain_magnitudes = np.delete(strain_magnitudes, num_magnitudes)

    strains = []
    for strain_direction in strain_directions:
        strains_tmp = np.array([strain_magnitude * strain_direction
                                for strain_magnitude in strain_magnitudes])
        strains.append(strains_tmp)
    strains = np.array(strains)

    strain_magnitudes_data = ArrayData()
    strain_magnitudes_data.set_array('strain_magnitudes', strain_magnitudes)
    strains_data = ArrayData()
    strains_data.set_array('strains', strains)

    strains_data_mapping = {'strains_data': strains_data,
                            'strain_magnitudes_data': strain_magnitudes_data}

    return strains_data_mapping


@workfunction
def get_stress(output_parameters, name=Str('stress')):
    '''
    Get the stress output of a PwRelaxWorkchain while maintaining provenance
        via the workfunction
    :param relax_workchain: WorkChain object with a get_outputs_dict() method,
        an output_parameters object in the outputs dict, and a stress entry
        in the output parameters
    :param name: Name of the stress array in the returned ArrayData
    :return: ArrayData containing output stress
    '''
    output_parameters_dict = output_parameters.get_dict()
    stress = np.array(output_parameters_dict[u'stress'])

    stress_data = ArrayData()
    stress_data.set_array(name.value, stress)

    return stress_data


@workfunction
def standardize_structuredata(og_structuredata, to_primitive=Bool(False),
                              symprec=Float(0.05)):
    '''
    Standardize an AiiDA StructureData object via pymatgen Structure
        using spglib
    :param og_structuredata: original StructureData
    :param to_primitive: whether to primitivize
    :param symprec: symmetry tolerance
    '''
    og_structure = og_structuredata.get_pymatgen()
    standard_structure = standardize_pymatgen(og_structure,
                                              to_primitive=bool(to_primitive),
                                              symprec=float(symprec))
    standard_structuredata = StructureData(
        pymatgen_structure=standard_structure)
    return standard_structuredata


@workfunction
def symmetry_structuredata(structuredata, symprec=Float(0.01),
                           angle_tolerance=Float(5.0)):
    '''
    Get symmetry info for a StructureData
    :param: structuredata: AiiDA StructureData
    :param symprec: symmetry tolerance
    :param angle_tolerance: angle tolerance
    :return: space group symbol, space group number, laue group symbol
    '''

    structure = structuredata.get_pymatgen()
    space_group_number, space_group_symbol, laue_symbol = symmetry_pymatgen(
        structure, float(symprec), float(angle_tolerance))
    space_group_number = Int(space_group_number)
    space_group_symbol = Str(space_group_symbol)
    laue_symbol = Str(laue_symbol)

    symmetry_data = {
        'space_group_number': space_group_number,
        'space_group_symbol': space_group_symbol,
        'laue_symbol': laue_symbol
    }

    return symmetry_data


@workfunction
def deform_structuredata(og_structuredata, lagrangian_data,
                         array_name):
    '''
    Deform an AiiDA StructureData object via its pymatgen structure
        using a lagrangian strain
    :param og_structure: undeformed StructureData
    :param langrangian: 1x6 voigt notation lagrangian strain
        (without / 2 terms) as an ArrayData
    :param array_name: name of lagrangian in lagrangian_data
    :return: deformed StructureData
    '''
    og_structure = og_structuredata.get_pymatgen()
    lagrangian = lagrangian_data.get_array(array_name)
    deformed_structure = deform_pymatgen(og_structure, lagrangian)
    deformed_structuredata = StructureData(
        pymatgen_structure=deformed_structure)

    return deformed_structuredata


@workfunction
def do_polyfit(strain_magnitudes, stresses, degree):
    '''
    Fit strain magnitudes against stresses to find an 'average' stress
        for strains along one direction
    :param strain_magnitudes: strain magnitudes lambda
    :type strain_magnitudes: ArrayData
    :param stresses: tensor stresses along one strain direction
    :type stresses: ArrayData
    '''
    strain_magnitudes = strain_magnitudes.get_array('strain_magnitudes')
    stresses = stresses.get_array('stresses')
    degree = degree.value

    voigt_stresses = [tensor2voigt(stress) for stress in stresses]
    polyfit = np.polyfit(strain_magnitudes, voigt_stresses, deg=degree)

    polyfit_data = ArrayData()
    polyfit_data.set_array('polyfit', polyfit)
    return polyfit_data
