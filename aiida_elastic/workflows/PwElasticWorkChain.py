# -*- coding: utf-8 -*-

from aiida_elastic.utils.elastic_utils import (
                           get_elastic_tensor, tensor2voigt, voigt2tensor,
                           lagrangian2deformation,
                           physical_stress2lagrangian_stress)
from aiida_elastic.workflows.workfunctions import (
                           get_olics, get_ulics,
                           get_all_strains, get_stress,
                           standardize_structuredata, symmetry_structuredata,
                           deform_structuredata, do_polyfit,
                           get_elastic_symmetries)

import numpy as np

from aiida.common.extendeddicts import AttributeDict
from aiida.orm.data.base import Str, Bool, Int, Float
from aiida.orm.data.parameter import ParameterData
from aiida.orm.data.structure import StructureData
from aiida.orm.data.array import ArrayData
from aiida.orm.utils import WorkflowFactory
from aiida.work.workchain import WorkChain, ToContext, if_, append_

PwBaseWorkChain = WorkflowFactory('quantumespresso.pw.base')
PwRelaxWorkChain = WorkflowFactory('quantumespresso.pw.relax')

# TODO: switch to Nico's symmetry finder
# TODO: use pymatgen to find monoclinic b/c
# TODO: switch to pymgtgen.symmetryanalyzer.get_conventional...
# TODO: in the future, use the elastic transformer from pymatgen
# TODO: implement vaidators for each of the inputs
# TODO: implement serializers for each of the inputs
# TODO: investigate whether PortNamespaces should be used
# TODO: implement exit codes
# TODO: consider modular design:
#       PWElasticWorkChain
#           OLICSWorkChain
#           DeformationWorkChain
#           ElasticFittingWorkChain

# TODO: implement leave-one-out to quantify uncertainty
# TODO: implement an error or warning if the laue group changes after
#       structure primitivization


class PwElasticWorkChain(WorkChain):

    @classmethod
    def define(cls, spec):
        super(PwElasticWorkChain, cls).define(spec)

        spec.expose_inputs(PwRelaxWorkChain, namespace='initial_relax',
                           exclude=('structure', 'clean_workdir'))
        spec.expose_inputs(PwRelaxWorkChain, namespace='elastic_relax',
                           exclude=('structure', 'clean_workdir'))

        spec.input('structure', valid_type=StructureData)
        spec.input('num_strain_magnitudes', valid_type=Int)
        spec.input('max_strain_magnitude', valid_type=Float)
        spec.input('strain_type', valid_type=Str, default=Str('olics'))
        spec.input('get_c_guess', valid_type=Bool, default=Bool(False))
        spec.input('c_guess', valid_type=ArrayData, required=False)
        spec.input('symprec', valid_type=Float, default=Float(0.01))
        spec.input('angle_tolerance', valid_type=Float, default=Float(5.0))
        spec.input('polyfit_degree', valid_type=Int, default=Int(4))
        # spec.input('num_strain_directions', valid_type=Int, required=False)
        spec.input('clean_workdir', valid_type=Bool, default=Bool(False))
        spec.input('do_initial_relax', valid_type=Bool, default=Bool(True))

        spec.outline(
            cls.setup,
            cls.get_symmetry_info,
            if_(cls.should_do_initial_relax)(
                cls.run_initial_relax,
                cls.inspect_initial_relax,
            ).else_(
                cls.set_ground_state_structure
            ),
            cls.get_ground_state_standard,
            if_(cls.should_get_c_guess)(
                cls.get_c_guess
            ),
            if_(cls.should_get_olics)(
                cls.get_olics
            ).else_(
                cls.get_ulics
            ),
            cls.get_strains,
            cls.get_deformed_structures,
            cls.run_deformed_relaxes,
            cls.inspect_deformed_relaxes,
            cls.convert_stresses,
            cls.fit_polynomial,
            cls.fit_elastic_constants,
            cls.results
        )

        spec.exit_code(401, 'ERROR_SUB_PROCESS_FAILED_RELAX',
                       message='one of the PwRelaxWorkChain subprocesses'
                       ' failed')

        spec.output('ground_state_structure', valid_type=StructureData)
        spec.output('strains', valid_type=ArrayData)
        spec.output('stresses', valid_type=ArrayData)
        spec.output('strain_directions', valid_type=ArrayData)
        spec.output('strain_magnitudes', valid_type=ArrayData)
        spec.output('polyfit_stresses', valid_type=ArrayData)
        spec.output('elastic_tensor', valid_type=ArrayData)

    def setup(self):
        '''
        Standardize the input structure and set the initial structure
        '''
        self.ctx.initial_structure = standardize_structuredata(
            self.inputs.structure, to_primitive=Bool(False),
            symprec=self.inputs.symprec)

    def get_symmetry_info(self):
        '''
        Get the space group number, space group symbol, and laue symbol
        for the initial structure
        '''
        symmetry_data = \
            symmetry_structuredata(self.ctx.initial_structure,
                                   symprec=self.inputs.symprec,
                                   angle_tolerance=self.inputs.angle_tolerance)

        self.ctx.space_group_number = symmetry_data['space_group_number']
        self.ctx.space_group_symbol = symmetry_data['space_group_symbol']
        self.ctx.laue = symmetry_data['laue_symbol']

    def should_do_initial_relax(self):
        '''
        Check whether to run the initial relax calculation
        '''
        if self.inputs.do_initial_relax:
            return True
        else:
            return False

    def run_initial_relax(self):
        '''
        Run a relax/vc-relax calculation to find the ground state structure
        '''
        inputs = AttributeDict(self.exposed_inputs(PwRelaxWorkChain,
                                                   namespace='initial_relax'))
        inputs.structure = self.ctx.initial_structure
        inputs.clean_workdir = self.inputs.clean_workdir

        future = self.submit(PwRelaxWorkChain, **inputs)
        self.report('Launching PwRelaxWorkChain<{}>'.format(future.pk))

        self.to_context(initial_relax_workchain=future)

    def inspect_initial_relax(self):
        '''
        Verify that the PwRelaxWorkChain finished successfully
        '''
        workchain = self.ctx.initial_relax_workchain

        if not workchain.is_finished_ok:
            self.report('PwRelaxWorkChain<{}> failed with exit status {}'
                        .format(workchain.pk, workchain.exit_status))
            return self.exit_codes.ERROR_SUB_PROCESS_FAILED_RELAX
        else:
            outputs_dict = workchain.get_outputs_dict()
            output_parameters_dict = outputs_dict[
                u'output_parameters'].get_dict()
            stress = np.array(output_parameters_dict['stress'])

            self.ctx.ground_state_structure = workchain.out.output_structure
            self.ctx.ground_state_stress = stress

    def set_ground_state_structure(self):
        '''
        Set the ground state structure if the initial relax step is not
            run
        '''
        self.ctx.ground_state_structure = self.ctx.initial_structure

    def get_ground_state_standard(self):
        '''
        Standardize the ground state structure
        '''
        self.ctx.ground_state_structure = standardize_structuredata(
            self.ctx.ground_state_structure, symprec=self.inputs.symprec)

    # run zero strain relax

    # inspect zero strain relax

    def should_get_c_guess(self):
        return False

    def get_c_guess(self):
        pass

    def should_get_olics(self):
        strain_type = str(self.inputs.strain_type)
        if str.lower(strain_type) == 'olics':
            return True
        else:
            return False

    def get_olics(self):
        '''
        Use elastic_data.py to look up the d-optimized linearly independent
            coupling strains
        '''
        laue = self.ctx.laue
        strain_directions_data = get_olics(laue)

        self.ctx.strain_directions_data = strain_directions_data
        self.ctx.strain_directions = strain_directions_data.get_array(
                'strain_directions')

    def get_ulics(self):
        '''
        Use elastic_data.py to look up the universal linearly independent
            coupling strains
        '''
        laue = self.ctx.laue
        strain_directions_data = get_ulics(laue)

        self.ctx.strain_directions_data = strain_directions_data
        self.ctx.strain_directions = strain_directions_data.get_array(
            'strain_directions')

    def get_strains(self):
        '''
        Combine OLICS or ULICS with the input strain magnitude parameters to
            generate the full set of strains to be applied to the ground state
            structure
        Returns a multidimensional array where the first dimension is the
            strain direction, the second is the strain magnitude, and
            the third is the strain component
        i.e. it's a list of strain directions where each entry is that
            that direction multiplied by all the strain magnitudes
        e.g. for strains [[1, 2], [3, 4]] and magnitudes [-0.1, 0.1]:
            [[[-0.1, -0.2], [ 0.1,  0.2]],
             [[-0.3, -0.4], [ 0.3,  0.4]]]
        '''

        strains_data_mapping = get_all_strains(
            self.inputs.max_strain_magnitude,
            self.inputs.num_strain_magnitudes,
            self.ctx.strain_directions_data)

        strains_data = strains_data_mapping['strains_data']
        strain_magnitudes_data = strains_data_mapping['strain_magnitudes_data']

        strains = strains_data.get_array('strains')
        strain_magnitudes = strain_magnitudes_data.get_array(
            'strain_magnitudes')

        self.ctx.strains_data = strains_data
        self.ctx.strains = strains
        self.ctx.strain_magnitudes_data = strain_magnitudes_data
        self.ctx.strain_magnitudes = strain_magnitudes

    def get_deformed_structures(self):
        '''
        Apply the full set of strains to obtain a list of strained structures
            ready to be relaxed
        '''
        ground_state_structure = self.ctx.ground_state_structure
        strains = self.ctx.strains

        self.ctx.deformed_structures = []
        for dir_strains in strains:
            tmp_structures = []
            for strain in dir_strains:
                strain_data = ArrayData()
                strain_data.set_array('strain', strain)
                deformed_structure = deform_structuredata(
                    ground_state_structure, strain_data, Str('strain'))
                self.report('created deformed StructureData<{}>'.format(
                    deformed_structure.pk))
                tmp_structures.append(deformed_structure)
            self.ctx.deformed_structures.append(tmp_structures)

    def run_deformed_relaxes(self):
        '''
        Run relax workflows for each deformed structure
        '''
        deformed_structures = self.ctx.deformed_structures

        for dir_structures in deformed_structures:
            for deformed_structure in dir_structures:
                inputs = AttributeDict(self.exposed_inputs
                                       (PwRelaxWorkChain,
                                        namespace='elastic_relax'))
                inputs.structure = deformed_structure
                inputs.clean_workdir = self.inputs.clean_workdir

                future = self.submit(PwRelaxWorkChain, **inputs)

                self.report(
                    'launching PwRelaxWorkChain<{}>'.format(future.pk))
                self.to_context(deformed_workchains=append_(future))

    def inspect_deformed_relaxes(self):
        '''
        Retrieve final stress from the defomed structure relax workflows
        Workchains are added to the context in a flat list which needs to be
            reshaped to resemble the structure of the strains
            i.e. 2-d array # strain directions x # strain magnitudes
        '''
        deformed_workchains = self.ctx.deformed_workchains

        stresses = []
        for workchain in deformed_workchains:
            if not workchain.is_finished_ok:
                self.report('PwRelaxWorkChain failed with exit status {}'
                            .format(workchain.exit_status))
                return self.exit_codes.ERROR_SUB_PROCESS_FAILED_RELAX
            else:
                outputs_dict = workchain.get_outputs_dict()
                output_parameters = outputs_dict[u'output_parameters']
                stress_data = get_stress(output_parameters)
                stress = stress_data.get_array('stress')
                stresses.append(stress)

        stresses = np.array(stresses).reshape(
            # self.inputs.num_strain_directions.value,
            len(self.ctx.strain_directions),
            self.inputs.num_strain_magnitudes.value * 2,
            3, 3)

        self.ctx.stresses = stresses

    def convert_stresses(self):
        '''
        Convert the cauchy stresses from QE to lagrangian stresses for
            elastic constants fitting
        '''
        stresses = self.ctx.stresses
        strains = self.ctx.strains

        lagrangian_stresses = []
        for dir_stresses, dir_strains in zip(stresses, strains):
            dir_lagrangian_stresses = []
            for stress, strain in zip(dir_stresses, dir_strains):
                deformation = lagrangian2deformation(strain)
                lagrangian_stress = physical_stress2lagrangian_stress(
                    stress, deformation)
                dir_lagrangian_stresses.append(lagrangian_stress)
            lagrangian_stresses.append(dir_lagrangian_stresses)
        lagrangian_stresses = np.array(lagrangian_stresses)

        self.ctx.lagrangian_stresses = lagrangian_stresses

    def fit_polynomial(self):
        '''
        Fit the stresses against strain magnitude to get an 'average' stress
            for each normalized strain direction
        '''
        stresses = self.ctx.lagrangian_stresses
        strain_magnitudes_data = self.ctx.strain_magnitudes_data

        polyfit_stresses = []
        for strain_direction_stresses in stresses:
            stresses_data = ArrayData()
            stresses_data.set_array('stresses', strain_direction_stresses)
            polyfit_data = do_polyfit(strain_magnitudes_data,
                                      stresses_data,
                                      self.inputs.polyfit_degree)
            polyfit = polyfit_data.get_array('polyfit')
            a = polyfit[-2]
            polyfit_stresses.append(a)

        self.ctx.polyfit_stresses = np.array([voigt2tensor(stress)
                                              for stress in polyfit_stresses])

    def fit_elastic_constants(self):
        '''
        Fit the 'average' stresses from the polynomial fits against the
            normalized strain directions, taking into account the material's
            laue symmetry using, a least squares fit via pseudoinversion and
            finding the elastic tensor
        '''
        symmetries_data = get_elastic_symmetries(self.ctx.laue)
        symmetries = symmetries_data.get_array('symmetries')

        # NOT a workfunction yet
        elastic_tensor = get_elastic_tensor(
            [self.ctx.polyfit_stresses],
            [self.ctx.strain_directions],
            symmetries)

        self.ctx.elastic_tensor = elastic_tensor

    def results(self):
        strains = ArrayData()
        strains.set_array('strains', np.array(self.ctx.strains))
        stresses = ArrayData()
        stresses.set_array('stresses', np.array(self.ctx.stresses))
        strain_directions = ArrayData()
        strain_directions.set_array('strain_directions',
                                    np.array(self.ctx.strain_directions))
        strain_magnitudes = self.ctx.strain_magnitudes_data
        polyfit_stresses = ArrayData()
        polyfit_stresses.set_array('polyfit_stresses',
                                   np.array(self.ctx.polyfit_stresses))
        elastic_tensor = ArrayData()
        elastic_tensor.set_array('elastic_tensor',
                                 np.array(self.ctx.elastic_tensor))
        self.out('ground_state_structure', self.ctx.ground_state_structure)
        self.out('strains', strains)
        self.out('stresses', stresses)
        self.out('strain_directions', strain_directions)
        self.out('strain_magnitudes', strain_magnitudes)
        self.out('polyfit_stresses', polyfit_stresses)
        self.out('elastic_tensor', elastic_tensor)
