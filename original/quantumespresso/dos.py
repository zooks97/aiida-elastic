# -*- coding: utf-8 -*-
from aiida.orm.workflow import Workflow
from aiida.orm import CalculationFactory, Code, DataFactory, Group
from aiida.workflows.user.epfl_theos.quantumespresso.pw import PwWorkflow
from aiida.workflows.user.epfl_theos.quantumespresso import helpers
from aiida.workflows.user.epfl_theos.quantumespresso.ph import PhWorkflow
from aiida.common.example_helpers import test_and_get_code
from aiida.orm.data.array.kpoints import _default_epsilon_length,_default_epsilon_angle

__copyright__ = u"Copyright (c), This file is part of the AiiDA-EPFL Pro platform. For further information please visit http://www.aiida.net/. All rights reserved"
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file."
__version__ = "0.1.0"
__authors__ = "Nicolas Mounet, Andrea Cepellotti, Giovanni Pizzi."


ParameterData = DataFactory('parameter')
KpointsData = DataFactory('array.kpoints')
StructureData = DataFactory('structure')
PwCalculation = CalculationFactory('quantumespresso.pw')
DOSCalculation = CalculationFactory('quantumespresso.dos')

class DosWorkflow(Workflow):
    """
    Workflow to compute the phonon dispersion from the raw initial unrelaxed
    structure.
    
    Results posted in results depend on the input parameters. 
    The largest set of data that can be put in the results consists in:
    * relaxed structure, if relaxed
    * pw_calculation, if the total energy needed to be computed
    * band_structure or band_structure1, band_structure2 (for spin polarized 
      calculations), containing the electronic band structure
    * ph_folder, i.e. the folder with all dynamical matrices
    * ph_calculation, if the phonon calculation was not parallelized over qpoints
    * phonon_dispersion, a BandsData object
    
    Input description.
    The input follows closely the input of the various subworkflows
    
    Electronic part:
    'structure': structure,
    'pseudo_family': pseudo_family,    
    'pw_codename': pw_codename,
    'pw_settings': settings,
    'pw_parameters': pw_input_dict,
    'pw_calculation_set': set_dict,
    'pw_kpoints': kpoints,
    'pw_input':{'relaxation_scheme': relaxation_scheme,
                  'volume_convergence_threshold': 1.e-2,
                  },

    OR

    'pw_calculation': load_node(60),


     'ph_calculation': load_node(157),
     
     'ph_codename': ph_codename,
     'ph_parameters': ph_input_dict,
     'ph_settings': settings,
     'ph_calculation_set': set_dict,
     'ph_qpoints': qpoints,
     'ph_input': {'use_qgrid_parallelization': True},
     
     'dispersion_matdyn_codename': matdyn_codename,
     'dispersion_q2r_codename': q2r_codename,
     'dispersion_calculation_set': set_dict,
     'dispersion_settings': settings,
     'dispersion_input':{'distance_qpoints_in_dispersion':0.01,
                   'asr': asr,
                   'zasr': asr,
                   'threshold_length_for_Bravais_lat': 1e-4,
                   'threshold_angle_for_Bravais_lat': 1e-4,
                   }
    """
    
    _qpoint_distance_in_dispersion = 0.01
    _default_epsilon_length = _default_epsilon_length
    _default_epsilon_angle = _default_epsilon_angle
    _clean_workdir = False
    
    def __init__(self,**kwargs):
        super(DosWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        """
        Checks the parameters
        """
        self.append_to_report("Checking input parameters")
        

        mandatory_dos_keys = [('dos_calculation_set',dict,'A dictionary with resources, walltime, ...'),
                             ('dos_parameters',dict,"A dictionary with the DOS input parameters"),
                             ]
        
        main_params = self.get_parameters()
        
        # validate the codes
        for kind,key in [['quantumespresso.dos','dos_codename']
                         ]:
            try:
                test_and_get_code(main_params[key], kind, use_exceptions=True)
            except KeyError:
                # none of the codes is always required
                pass
        
        # case of restart from phonon calculation
        if 'parent_pw_calculation' in main_params:
            if isinstance(main_params['parent_pw_calculation'], PwCalculation):
                helpers.validate_keys(main_params, mandatory_dos_keys)
                self.next(self.run_dos)
                return
            else:
                raise TypeError("parameter 'pw_calculation' should be a "
                                "PwCalculation")
 
        # validate Pw keys
        #helpers.validate_keys(main_params, mandatory_pw_keys)
        
        # start from Pw calculation
        #self.next(self.run_pw)
    
                
    @Workflow.step
    def run_dos(self):
        """
        Launch the PwWorkflow
        """
        main_params = self.get_parameters()
        # take the parameters needed for the PW computation
        dos_params = {}
        
        
        
        for k,v in main_params.iteritems():
            if k.startswith('dos_'):
                new_k = k[4:] # remove pw_
                dos_params[new_k] = v
            else:
                dos_params[k] = v
        
	dosinput_parameters = ParameterData(dict= dos_params['parameters'])
	#dosinput_parameters.store()

        code = Code.get_from_string(dos_params['codename'])
        dos_calc = code.new_calc()
        dos_calc.use_parameters(dosinput_parameters)
        old_pw_calc = main_params['parent_pw_calculation']
        settings_dict = old_pw_calc.inp.settings.get_dict()        
        settings = ParameterData(dict=settings_dict)
        dos_calc.use_settings(settings)
        
        setdictin = {}
	newparallel = True
	if newparallel:
	    setdictin =  dos_params['calculation_set']
	    setdictin['max_wallclock_seconds'] = 29 * 60
	    setdictin['custom_scheduler_commands'] = setdictin['custom_scheduler_commands'] + "\n#SBATCH --partition=debug"
	else:
	    for name in dos_params['calculation_set'].keys():
		if name == 'custom_scheduler_commands':
		    setdictin[name] = dos_params['calculation_set'][name]
		elif name == 'resources':
		    setdictin[name] = old_pw_calc.get_resources()
		elif name == 'max_wallclock_seconds':
		    setdictin[name] = old_pw_calc.get_max_wallclock_seconds() 
		    
        dos_calc = helpers.set_the_set(dos_calc, 
                                       setdictin)
	
	dos_calc.use_parent_calculation(old_pw_calc)
        dos_calc.store_all()
        
        self.append_to_report("Launching DOS (pk: {})".format(dos_calc.pk))
        self.attach_calculation(dos_calc)
	
	
        
        self.next(self.run_dos_analysis)
        


    @Workflow.step
    def run_dos_analysis(self):
        
        main_params = self.get_parameters()
        
        # Retrieve the MATDYN calculation  
        dos_calc = self.get_step_calculations(self.run_dos)[0]
        
        #
        #self.append_to_report("Phonon dispersions done (bandsdata pk: {})"
        #                      "".format(bandsdata.pk))
        #
        self.add_result("dos_calculation", dos_calc)
        #bandsdata.label = "Phonon bands"
        #bandsdata.description = ("Phonon dispersion calculated with"
        #                         " the workflow {}".format(self.pk))
        """
        group_name = main_params.get('dos_group_name',None)
        if group_name is not None:
            # create or get the group
            group, created = Group.get_or_create(name=group_name)
            if created:
                self.append_to_report("Created group '{}'".format(group_name))
            # put the bands data into the group
            group.add_nodes(bandsdata)
            self.append_to_report("Adding bands to group '{}'".format(group_name))
        
        # clean scratch leftovers, if requested
        if main_params.get('dispersion_input',{}).get('clean_workdir',self._clean_workdir):
            self.append_to_report("Cleaning scratch directories")
            save_calcs = []
            try:
                # Note that the order is important!
                save_calcs.append( self.get_result('ph_calculation') )
                save_calcs.append( self.get_result('pw_calculation') )
            except (NameError, ValueError):
                pass
            helpers.wipe_all_scratch(self, save_calcs)
        """
        self.next(self.exit)
