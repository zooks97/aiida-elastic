#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 16:01:54 2017

@author: nh_dbpath
"""


import numpy as np
import itertools
import time
from scipy.spatial import Delaunay, Voronoi, ConvexHull


from aiida.orm import DataFactory
StructureData = DataFactory('structure')


def reciprocal_cell(matrix):
    rec_cell = []
    for i in range(3):
        nv = 2*np.pi*np.cross(matrix[(i+1)%3], matrix[(i+2)%3])
        rec_cell.append(nv/np.linalg.det(matrix))
    return np.array(rec_cell)

    
def reduce_angle_GS(rec_matrix, count = 0):
    
    axlen = [np.linalg.norm(a) for a in rec_matrix]
    newax = [i for i in rec_matrix]
    sl = np.argsort(axlen)
    '''
    reducelen_longest =  int(np.round(np.dot(newax[sl[0]],rec_matrix[sl[2]])/(axlen[sl[0]])**2))
    
    newax[sl[2]] = rec_matrix[sl[2]] - reducelen_longest*newax[sl[0]]

    reducelen2 = int(np.round(np.dot(newax[sl[0]],rec_matrix[sl[1]])/(np.linalg.norm(newax[sl[0]])**2)))
    
    reducelen3 = int(np.round(np.dot(newax[sl[2]],rec_matrix[sl[1]])/(np.linalg.norm(newax[sl[2]])**2)))
    
    
    newax[sl[1]] = rec_matrix[sl[1]] - reducelen2*newax[sl[0]] - reducelen3*newax[sl[2]]

    print reducelen_longest, reducelen2, reducelen3, np.linalg.det(rec_matrix)/np.linalg.det(np.array(newax))
    '''
    reducelen_longest =  int(np.round(np.dot(newax[sl[0]],rec_matrix[sl[1]])/(axlen[sl[0]])**2))
    
    newax[sl[1]] = rec_matrix[sl[1]] - reducelen_longest*newax[sl[0]]

    reducelen2 = int(np.round(np.dot(newax[sl[0]],rec_matrix[sl[2]])/(np.linalg.norm(newax[sl[0]])**2)))
    
    reducelen3 = int(np.round(np.dot(newax[sl[1]],rec_matrix[sl[2]])/(np.linalg.norm(newax[sl[1]])**2)))
    
    
    newax[sl[2]] = rec_matrix[sl[2]] - reducelen2*newax[sl[0]] - reducelen3*newax[sl[1]]

    #print reducelen_longest, reducelen2, reducelen3, np.linalg.det(rec_matrix)/np.linalg.det(np.array(newax))
    #print axlen, [np.linalg.norm(a) for a in newax]
    if reducelen_longest == 0 and reducelen2 == 0 and reducelen3 == 0:
        #print 'Done after {} iterations'.format(count)
        return newax
    elif count == 4:
        #print 'Max iterations'
        return newax
    
    else:
        return reduce_angle_GS(newax, count = count+1)
    


def reduce_angle(rec_matrix):
    """
    THIS is from pymatgen ll reduction
    """
    def calculate_lll(lattice, delta=0.75):
        from numpy import pi, dot, transpose, radians
        """
        Performs a Lenstra-Lenstra-Lovasz lattice basis reduction to obtain a
        c-reduced basis. This method returns a basis which is as "good" as
        possible, with "good" defined by orthongonality of the lattice vectors.

        This basis is used for all the periodic boundary condition calculations.

        Args:
            delta (float): Reduction parameter. Default of 0.75 is usually
                fine.

        Returns:
            Reduced lattice matrix, mapping to get to that lattice.
        """
        # Transpose the lattice matrix first so that basis vectors are columns.
        # Makes life easier.
        matrix = np.array(lattice)
        a = matrix.T

        b = np.zeros((3, 3))  # Vectors after the Gram-Schmidt process
        u = np.zeros((3, 3))  # Gram-Schmidt coeffieicnts
        m = np.zeros(3)  # These are the norm squared of each vec.

        b[:, 0] = a[:, 0]
        m[0] = dot(b[:, 0], b[:, 0])
        for i in range(1, 3):
            u[i, 0:i] = dot(a[:, i].T, b[:, 0:i]) / m[0:i]
            b[:, i] = a[:, i] - dot(b[:, 0:i], u[i, 0:i].T)
            m[i] = dot(b[:, i], b[:, i])

        k = 2

        mapping = np.identity(3, dtype=np.double)
        while k <= 3:
            # Size reduction.
            for i in range(k - 1, 0, -1):
                q = round(u[k - 1, i - 1])
                if q != 0:
                    # Reduce the k-th basis vector.
                    a[:, k - 1] = a[:, k - 1] - q * a[:, i - 1]
                    mapping[:, k - 1] = mapping[:, k - 1] - q * \
                        mapping[:, i - 1]
                    uu = list(u[i - 1, 0:(i - 1)])
                    uu.append(1)
                    # Update the GS coefficients.
                    u[k - 1, 0:i] = u[k - 1, 0:i] - q * np.array(uu)

            # Check the Lovasz condition.
            if dot(b[:, k - 1], b[:, k - 1]) >=\
                    (delta - abs(u[k - 1, k - 2]) ** 2) *\
                    dot(b[:, (k - 2)], b[:, (k - 2)]):
                # Increment k if the Lovasz condition holds.
                k += 1
            else:
                # If the Lovasz condition fails,
                # swap the k-th and (k-1)-th basis vector
                v = a[:, k - 1].copy()
                a[:, k - 1] = a[:, k - 2].copy()
                a[:, k - 2] = v

                v_m = mapping[:, k - 1].copy()
                mapping[:, k - 1] = mapping[:, k - 2].copy()
                mapping[:, k - 2] = v_m

                # Update the Gram-Schmidt coefficients
                for s in range(k - 1, k + 1):
                    u[s - 1, 0:(s - 1)] = dot(a[:, s - 1].T,
                                              b[:, 0:(s - 1)]) / m[0:(s - 1)]
                    b[:, s - 1] = a[:, s - 1] - dot(b[:, 0:(s - 1)],
                                                    u[s - 1, 0:(s - 1)].T)
                    m[s - 1] = dot(b[:, s - 1], b[:, s - 1])

                if k > 2:
                    k -= 1
                else:
                    # We have to do p/q, so do lstsq(q.T, p.T).T instead.
                    p = dot(a[:, k:3].T, b[:, (k - 2):k])
                    q = np.diag(m[(k - 2):k])
                    result = np.linalg.lstsq(q.T, p.T)[0].T
                    u[k:3, (k - 2):k] = result

        return a.T, mapping.T

        
    lattif = np.array(calculate_lll(rec_matrix)[0])
    if np.linalg.det(lattif) < 0:
        lai = [lattif[1], lattif[0], lattif[2]]
    else:
        lai = [lattif[0], lattif[1], lattif[2]]
    
    angles = np.sort([ np.arcsin(np.linalg.norm(np.cross(lai[(i+1)%3], lai[(i+2)%3])) / (np.linalg.norm(lai[(i+1)%3]) * np.linalg.norm(lai[(i+2)%3]))) / np.pi*180 for i in range(3)])
    if angles[0] < 60:
        newtest = reduce_angle_GS(lai)
        anglesn = np.sort([ np.arcsin(np.linalg.norm(np.cross(newtest [(i+1)%3], newtest [(i+2)%3])) / (np.linalg.norm(newtest [(i+1)%3]) * np.linalg.norm(newtest [(i+2)%3]))) / np.pi*180 for i in range(3)])
        if anglesn[0] > 60:
            return newtest
        else:
            return lai
    else:
        return lai
    
        
        
        
        
        
        
def construct_surrounding_points_forplot(recmatrixprim):

    recmatrixprim1 = reduce_angle(recmatrixprim)
   
    surrounding = [0., 1., -1., 2, -2]
    #print recmatrixprim1
    count = 0
    kpointsi = []
    for ijk in itertools.product(surrounding,surrounding, surrounding):     
        kpointsi.append(ijk[0] *recmatrixprim1[0] + ijk[1] * recmatrixprim1[1] + ijk[2] * recmatrixprim1[2])
    return  kpointsi
    
    
def construct_surrounding_points(recmatrixprim):
    recmatrixprim1 = reduce_angle(recmatrixprim)
    #print recmatrixprim1
   
    surrounding = [0., 1., -1., 2, -2]

    count = 0
    kpointsi = []
    for ijk in itertools.product(surrounding,surrounding, surrounding):
        if count != 0:      
            kpointsi.append(ijk[0] *recmatrixprim1[0] + ijk[1] * recmatrixprim1[1] + ijk[2] * recmatrixprim1[2])
        count += 1
    return  kpointsi   
            
        
    
    
    
    
    
    
    
def analyse_lattice_points_kmesh_voronoi(matrix,kmesh, fast = True, prescreen_line_dens = True):
    """
    This method analyses the isotropy of a kpoint mesh from a real space lattice matrix matrix.
    The fast == True method was tested and is much faster than the ultimately correct method.
    In the fast method. only a limited number of supercells is created for the Voronoi construction.
    This might fail (Voronoi construction fails or gives a too asymmetric Voronoi cell) for very anisotropic
    meshes. But as we are anyways not interested in those, we make basically an error only in systems which are far away from the optimum
    isotropic mesh which we look for.
    """
    
    reccell = reciprocal_cell(matrix)
    
    
        
    bx = reccell[0]/kmesh[0]
    by = reccell[1]/kmesh[1]
    bz = reccell[2]/kmesh[2]
    recarr1 = np.array([ bx, by, bz ])
    
    ## here we make the most orthogonal representation of the rec lattice
    
    
    recarr = reduce_angle(recarr1)

    
    bx = recarr[0]
    by = recarr[1]
    bz = recarr[2]




    
    stepx = np.linalg.norm(bx)
    stepy = np.linalg.norm(by)
    stepz = np.linalg.norm(bz)
    
    
    lena = np.array([stepx, stepy, stepz])
    
    maxlen = max(lena)
    minlen = min(lena)
    '''
    scales = [int(np.ceil(i+0.1)) for i in maxlen/lena]
    scnew  = []
    for i in range(3):
        listi = range(scales[i]+1)
        listi += [-k for k in range(scales[i]+1) if k != 0]
        scnew.append(listi)
    '''   
    
    recvol = np.linalg.det(recarr)
    

    
    if prescreen_line_dens:
        #print lena
        #print 0.5* recvol**(1./3)
        if minlen < 0.3* recvol**(1./3):
            print "LINE DENS LINE DENS LINE DENSLINE DENSLINE DENS LINE DENS LINE DENS LINE DENSLINE DENSLINE DENSv"
            return False, 0,0,0
        else:
        
            # Construction of adjacent kpoints around [0,0,0]
            # This is rather heuristic and in principle problematic for highly mono/triclinic primitive cells 
            # of the kpoint mesh.
            # However, in all relevant cases, i.e. the targeted isotropic systems, the Voronoi 
            # cell will be consistently constructed.
            # As a remark. pymatgen.core.lattice.Lattice.get_wigner_seitz_cell
            # does it even worse!!! they only construct all neighbouring cells.
            # This corresponds to make_a_reasonable_supercell = np.array([0, 1, -1]).
    
            
            #print scales
            #print scnew
            ipoints = []
            
            ti = time.clock()
            surrounding = [0., 1., -1., 2., -2.]
            #for i, j, k in itertools.product(scnew[0], scnew[1], scnew[2]):
            for i, j, k in itertools.product(surrounding, surrounding, surrounding):
                ipoints.append(i * recarr[0] + j * recarr[1] + k * recarr[2])
            firsttime = round(time.clock()-ti, 7)
            #print 'finished cell construction after ', firsttime, ' sec'
        

            
            
            '''
            x = make_a_reasonable_supercell
            y = make_a_reasonable_supercell
            z = make_a_reasonable_supercell
            #print x, y, z
            X, Y, Z = np.meshgrid(x,y,z, indexing='ij')
            XV = np.ones_like(X)
            YV = np.ones_like(Y)
            ZV = np.ones_like(Z)
            for i in range(len(x)):
                for j in range(len(y)):
                    for k in range(len(z)):
                        position =  X[i,j, k]*bx + Y[i,j, k]*by +  Z[i,j, k]*bz
                                    
                        XV[i,j,k] = position[0]
                        YV[i,j,k] = position[1]
                        ZV[i,j,k] = position[2]
                    
            XVf = XV.flatten()
            YVf = YV.flatten()
            ZVf = ZV.flatten()
            '''
            
            # Recution of points for Voronoi decomposition for speedup.
            # This is rather heuristic again and but not problematic.
            # The reduction of points within a sphere of radius radmax is non-problematic for all interesting
            # cases i.e. isotropic systems. E.g. all neighbouring points in cubic system with lattice length of
            # stepx are within a sphere of = sqrt(3)*(stepx*stepy*stepz)**(1./3) = 1.44*stepx
            # here we accept points within a sphere of 3.5 times this optimum size.
            # In case of non-isotropic systems this might lead to a point cloud,
            # where the Vornoi cell construction fails. 
            # These exceptions are captured but this does not impact the result, as we are however not interested in those
            # kpoint meshes. There is no need for the construction to work out.
            # We save significant amount of time by limiting the size of the kpoint cloud for
            # Voronoi construction, as before for constructing large enough point clouds.
            if fast:
                radmax = 4.*recvol**(1./3)
                points = np.array([ip for ip in ipoints if np.linalg.norm(ip) < radmax ])
                print 'lenpoints' , len(points)

            else: 
            
            # This would be the right thing to do but this is much too slow
            # Here also very unreasonable cells are constructed with much too many points for the Voronoi analysis
            # In the former method the wrong voronoi cell is analysed (potentially 
            # the former algorythm does not make a large enough cell)
            # but in this case anyways the cell is very bad (very long vector
            # in one direction) and the cell will be rejected anyways. 
            #
                points = []
                for i in itertools.product(scnew[0],scnew[1],scnew[2]):
                    poi = np.dot(recarr, np.array(i))
                    if np.linalg.norm(poi) < maxlen * 1.1 :
                        points.append(np.dot(recarr), np.array(i))
                    
                points = np.array(points)
                
            
            try:
                vor = Voronoi(points)
                pointset = []
                vorvertices = vor.vertices
                for indi in vor.regions[vor.point_region[0]]:
                    pointset.append(vorvertices[indi])

                pointsa = np.array(pointset)
                #print np.shape(pointsa)
                pmean = pointsa - np.mean(pointsa, axis = 0)
                covarr = np.cov(pmean, rowvar=False)
                #print np.shape(covarr)
                coveigval, coveigv  = np.linalg.eig(covarr)
                #print coveigval
                #print coveigv
                coveigval = np.real(coveigval)
                stdev_EV = np.std(coveigval/np.mean(coveigval))
                # This checks if we have a 3d system of points or just coplanar
                # is probably not necessary as the voronoi construction will
                # already fail for 2d point cloud in 3d
                if all([abs(i) > 10**-5 for i in coveigval]):
                    #print kmesh, np.prod(kmesh),  len(pointsa), stdev_EV
                    if kmesh == [7, 7, 7]:                
                        #print points
                        #print pointset
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++='
                    return True, np.prod(kmesh), stdev_EV, coveigval.tolist()
                else:
                    return False, np.prod(kmesh), stdev_EV, coveigval.tolist()
            except:
                print "Problem with ", kmesh
                return False, 0,0,0
    else:
        return False, 0,0,0


        







def get_isotropy_optimized_kpoints_mesh_from_density(matrix, distance, offset=[0., 0., 0.],
                                  force_parity=False):
    import time
    #print matrix
    #print distance
    print 'starting cell construction'
    ti = time.clock()
    rec_cell = reciprocal_cell(matrix)    
    rec_orthoprojection = []
    for i in range(3):
        nv = np.cross(rec_cell[(i+1)%3], rec_cell[(i+2)%3])/ np.linalg.norm(rec_cell[(i+1)%3]) /np.linalg.norm(rec_cell[(i+2)%3])
        rec_orthoprojection.append(nv)
        
            
    size = np.linalg.det(rec_cell)
    sizecube = np.prod(np.array([ np.linalg.norm(b) for  b in rec_cell ]))
    factor = (sizecube/size)**(1./3)
    distance_rescaled = distance * factor
    kpointsmesh_standard = [
            max(int(np.ceil(round(np.linalg.norm(b) / distance, 5))), 1) for b in rec_cell]
    kpointsmesh_standard_norm = [
            max(int(np.ceil(round(np.linalg.norm(b) / distance_rescaled, 5))), 1) for b in rec_cell]
    
    trialmeshes = []
    targetxyz = np.prod(kpointsmesh_standard_norm)
    for besti in range(3):
        bestdir = [i for i in range(3) if i != besti]
        longbest = np.argsort([kpointsmesh_standard[i] for i in bestdir])
        diffmeshx_max = kpointsmesh_standard[bestdir[longbest[1]]]
        diffmeshy_max = kpointsmesh_standard[bestdir[longbest[0]]]
        for i in range(diffmeshx_max):
            for j in range(diffmeshy_max):
                dx = i+1
                dy = j+1
                dz = max(int(round(float(targetxyz)/(dx*dy))), 1)
                if abs(float(dx*dy*dz)/targetxyz-1) < 0.25:
                    
                    #print dx
                    #print dx*dy
                    vec = [0,0,0]
                    vec[besti] = dz
                    vec[bestdir[longbest[1]]] = dx
                    vec[bestdir[longbest[0]]] = dy
                    trialmeshes.append(vec) 
    print 'finished cell construction after ', round(time.clock()-ti, 3), ' sec'
    ti = time.clock()
    print 'starting optimization' 
    print 'target total number of kpoints: ' +  str(targetxyz)    
    print 'analysing ', len(trialmeshes) + 2, " meshes"    
    trialmeshes.append(kpointsmesh_standard_norm)
    
    ntm = []
    for i in trialmeshes:
        if i not in ntm:
            ntm.append(i)
    


    
    opt_allv = analyse_voronoi(matrix, ntm)
    normal_v = analyse_lattice_points_kmesh_voronoi(matrix,kpointsmesh_standard_norm)

        
        
    print 'finished grid optimization after ', round(time.clock()-ti, 3), ' sec'
    print 'normal grid', kpointsmesh_standard_norm, normal_v[1],normal_v[2]
    print 'optimized mesh voronoi', opt_allv[1],  opt_allv[2], opt_allv[3]
    print '################################################################################'
    return opt_allv[1]

        
        
        
        
        
def analyse_voronoi(matrix, trialmeshes):   
    opt_all = []
    for km in trialmeshes:
        verify, kmeshnumber, stdev_EV, coveigval = analyse_lattice_points_kmesh_voronoi(matrix,km)
        if verify:
            if opt_all == []:
                opt_all = [verify, km, kmeshnumber, stdev_EV, coveigval]
            elif stdev_EV < opt_all[3]:
                opt_all = [verify, km, kmeshnumber, stdev_EV, coveigval]
    return opt_all       