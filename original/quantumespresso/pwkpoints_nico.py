# -*- coding: utf-8 -*-
from aiida.orm.workflow import Workflow
from aiida.orm import DataFactory, CalculationFactory, Group
from aiida.orm.calculation.inline import make_inline
from aiida.workflows.user.epfl_theos.quantumespresso import helpers
from aiida.workflows.user.epfl_theos import TheosWorkflowFactory

from kpoints_generator import get_isotropy_optimized_kpoints_mesh_from_density


import numpy as np

__copyright__ = u"Copyright (c), This file is part of the AiiDA-EPFL Pro platform. For further information please visit http://www.aiida.net/. All rights reserved"
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file."
__version__ = "0.1.0"
__authors__ = "Nicolas Mounet."

UpfData = DataFactory('upf')
ParameterData = DataFactory('parameter')
KpointsData = DataFactory('array.kpoints')
StructureData = DataFactory('structure')
PwCalculation = CalculationFactory('quantumespresso.pw')
PwWorkflow = TheosWorkflowFactory('quantumespresso.pw')

_default_degauss_from_QE = 0. # Ry

@make_inline
def get_denser_kpoints_grid_inline(parameters,kpoints,structure):
    """
    Get new kpoints with a denser mesh than that in kpoints
    :param parameters: ParameterData object containing the dictionary
        {'distance_kpoints_step_factor': float indicating by which factor we multiply
                                 the current distance between k-points
         }
    :param kpoints: KPointsData object containing the current k-points 
        (must be defined from a mesh)
    :param structure: the structure (used to get the reciprocal cell)
    :return: dictionary of the form
        {'output_kpoints': KPointsData object with the new k-points,
         'output_parameters': ParameterData with dictionary
                             {'distance_kpoints_in_mesh': float with the new
                                                          distance between k-pts
                              }
         }
    """
    kpoints_mesh = kpoints.get_kpoints_mesh()[0]
    distance_step_factor = parameters.get_dict()['distance_kpoints_step_factor']
    pbcs = structure.pbc
    # get the current distance between k-points
    kpoints_dummy=KpointsData()
    kpoints_dummy.set_cell_from_structure(structure)
    distance_kpoints_in_mesh = np.average(np.array([np.linalg.norm(b)/float(k) 
                                for pbc,k,b in 
                                zip(pbcs,kpoints_mesh, kpoints_dummy.reciprocal_cell)
                                if pbc]))
    # update the distance between k-points and prepare the new kpoints
    the_kpoints_mesh = [int(np.ceil(round(np.linalg.norm(b)/(distance_kpoints_in_mesh*distance_step_factor),5)))
                        if pbc else 1 for pbc,b in zip(pbcs,kpoints_dummy.reciprocal_cell)]
    # force the mesh to be "even"
    the_kpoints_mesh = [k + (k % 2) if pbc else 1
                        for pbc,k in zip(pbcs,the_kpoints_mesh)]
    # force the mesh to increase in any case
    the_kpoints_mesh = [k if (k>kold or not pbc) else k+2 
                        for (pbc,k,kold) in zip(pbcs,the_kpoints_mesh,kpoints_mesh)]
    the_kpoints=KpointsData()
    # We keep the same mesh offset as before
    the_kpoints.set_kpoints_mesh(the_kpoints_mesh,kpoints.get_kpoints_mesh()[1])
    the_distance_kpoints_in_mesh = np.average(np.array([np.linalg.norm(b)/float(k) 
                                for pbc,k,b in 
                                zip(pbcs,the_kpoints_mesh, kpoints_dummy.reciprocal_cell)
                                if pbc]))
    return {'output_kpoints': the_kpoints,
             'output_parameters': ParameterData(dict={
                    'distance_kpoints_in_mesh': the_distance_kpoints_in_mesh})
            }








@make_inline
def get_energy_forces_stresses_inline(**kwargs):#structure, pwcalculations_renormalized, pwcalculations_optimized):
    """
    Get new kpoints with a denser mesh than that in kpoints
    :param parameters: ParameterData object containing the dictionary
        {'distance_kpoints_step_factor': float indicating by which factor we multiply
                                 the current distance between k-points
         }
    :param kpoints: KPointsData object containing the current k-points 
        (must be defined from a mesh)
    :param structure: the structure (used to get the reciprocal cell)
    :return: dictionary of the form
        {'output_kpoints': KPointsData object with the new k-points,
         'output_parameters': ParameterData with dictionary
                             {'distance_kpoints_in_mesh': float with the new
                                                          distance between k-pts
                              }
         }
    """
    
    nratoms = len(kwargs['structure'].sites)
    
    kmesh_renormalized_list =[]
    stress_renormalized_list = []
    forces_renormalized_list = []
    energy_renormalized_list = []
    
    kmesh_optimized_list =[]
    stress_optimized_list = []
    forces_optimized_list = []
    energy_optimized_list = []

    skmesh_renormalized_list =[]
    sstress_renormalized_list = []
    sforces_renormalized_list = []
    senergy_renormalized_list = []
    
    skmesh_optimized_list =[]
    sstress_optimized_list = []
    sforces_optimized_list = []
    senergy_optimized_list = []

    
    for k, v in kwargs.items():
	
        print 'here'
        
        if 'renormalized' in k:
            calc = v.inp.output_parameters
            kmesh_renormalized_list.append(calc.inp.kpoints.get_kpoints_mesh())
            stress_renormalized_list.append(calc.res.stress)
            forces_renormalized_list.append(calc.out.output_array.get_array('forces')[0].tolist())
            energy_renormalized_list.append(calc.res.energy/nratoms)
        
        elif 'optimized' in k:
            calc = v.inp.output_parameters
            kmesh_optimized_list.append(calc.inp.kpoints.get_kpoints_mesh())
            stress_optimized_list.append(calc.res.stress)
            forces_optimized_list.append(calc.out.output_array.get_array('forces')[0].tolist())
            energy_optimized_list.append(calc.res.energy/nratoms)
        
    rs = np.argsort([np.prod(i) for i in kmesh_renormalized_list])
    for i in rs:
        skmesh_renormalized_list.append(kmesh_renormalized_list[i])
        sstress_renormalized_list.append(stress_renormalized_list[i])
        sforces_renormalized_list.append(forces_renormalized_list[i])
        senergy_renormalized_list.append(energy_renormalized_list[i])
        
        
    rs = np.argsort([np.prod(i) for i in kmesh_optimized_list])
    for i in rs:
        skmesh_optimized_list.append(kmesh_optimized_list[i])
        sstress_optimized_list.append(stress_optimized_list[i])
        sforces_optimized_list.append(forces_optimized_list[i])
        senergy_optimized_list.append(energy_optimized_list[i])

    
    resdict = {'structure_uuid': kwargs['structure'].uuid, 
                'optimized_mesh': 
                   {'kmesh': skmesh_optimized_list, 'energy': senergy_optimized_list, 'stress': sstress_optimized_list, 'force': sforces_optimized_list}, 
                'renormalized_mesh': 
                    {'kmesh': skmesh_renormalized_list, 'energy': senergy_renormalized_list, 'stress': sstress_renormalized_list, 'force': sforces_renormalized_list}}
    

    return {'output_kp_convergence_parameters': ParameterData(dict=resdict)}




















def get_kpoints_from_density_renormalized(structure, distance, shift = [0,0,0]):
    """
    We assume that the line density transforms to a volume density as a cube.
    Therefore we rescale le line_density to the reciprocal volume.
    """
    kpoints_dummy=KpointsData()
    kpoints_dummy.set_cell_from_structure(structure)
    pbcs = structure.pbc
    distance_renormalized =  distance * (np.prod(np.array([np.linalg.norm(b) for b in kpoints_dummy.reciprocal_cell])) / np.linalg.det(np.array(kpoints_dummy.reciprocal_cell)))**(1./3)
    
    the_kpoints_mesh = [int(np.ceil(round(np.linalg.norm(b)/(distance_renormalized),5)))
                        if pbc else 1 for pbc,b in zip(pbcs,kpoints_dummy.reciprocal_cell)]
    kpoints_dummy.set_kpoints_mesh(the_kpoints_mesh, shift)
    kpoints_dummy.store()
    
    return kpoints_dummy
    
    
def get_reciprocal_cell(matrix):
    rec_cell = []
    for i in range(3):
        nv = 2*np.pi*np.cross(matrix[(i+1)%3], matrix[(i+2)%3])
        rec_cell.append(nv/np.linalg.det(matrix))
    return np.array(rec_cell)


def get_kpoints_mesh_from_density_renormalized(structure, distance, shift = [0,0,0]):
    """
    We assume that the line density transforms to a volume density as a cube.
    Therefore we rescale le line_density to the reciprocal volume.
    """

    reciprocal_cell = get_reciprocal_cell(structure)
    pbcs = structure.pbc
    distance_renormalized =  distance * (np.prod(np.array([np.linalg.norm(b) for b in reciprocal_cell])) / np.linalg.det(reciprocal_cell))**(1./3)
    
    the_kpoints_mesh = [int(np.ceil(round(np.linalg.norm(b)/(distance_renormalized),5)))
                        if pbc else 1 for pbc,b in zip(pbcs,reciprocal_cell)]
    
    return the_kpoints_mesh



def get_kpoints_from_density_standard(structure, distance, shift = [0,0,0]):
    
    kpoints_dummy=KpointsData()
    kpoints_dummy.set_cell_from_structure(structure)
    pbcs = structure.pbc
    the_kpoints_mesh = [int(np.ceil(round(np.linalg.norm(b)/(distance),5)))
                        if pbc else 1 for pbc,b in zip(pbcs,kpoints_dummy.reciprocal_cell)]
    kpoints_dummy.set_kpoints_mesh(the_kpoints_mesh, shift)
    kpoints_dummy.store()
    
    return kpoints_dummy
    
    
    
    
def get_kpoints_mesh_from_density_optimized(structure, distance, shift = [0,0,0]):
    """
    Check Steinhard parameters
    """

    return get_isotropy_optimized_kpoints_mesh_from_density(np.array(structure.cell), distance)
    
def get_kpoints_from_density_optimized(structure, distance, shift = [0,0,0]):
    """
    Check Steinhard parameters
    """
    kpoints_dummy=KpointsData()
    kpoints_dummy.set_cell_from_structure(structure)
    pbcs = structure.pbc
    kpoints_dummy.set_kpoints_mesh(get_kpoints_mesh_from_density_optimized(structure, distance, shift = shift), shift)
    kpoints_dummy.store()
    
    return kpoints_dummy
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

@make_inline
def get_denser_opt_kpoints_grid_inline(kpoints, parameters,structure):
    """
    kpoints is the renormalized kpoints
    Get new kpoints with a denser mesh than that in kpoints
    :param parameters: ParameterData object containing the dictionary
        {'distance_kpoints_step_factor': float indicating by which factor we multiply
                                 the current distance between k-points
         }
    :param kpoints: KPointsData object containing the current k-points 
        (must be defined from a mesh)
    :param structure: the structure (used to get the reciprocal cell)
    :return: dictionary of the form
        {'output_kpoints': KPointsData object with the new k-points,
         'output_parameters': ParameterData with dictionary
                             {'distance_kpoints_in_mesh': float with the new
                                                          distance between k-pts
                              }
         }
    """
    kpoints_mesh = kpoints.get_kpoints_mesh()[0]
    distance_step_factor = parameters.get_dict()['distance_kpoints_step_factor']
    pbcs = structure.pbc
    # get the current distance between k-points
    kpoints_dummy=KpointsData()
    kpoints_dummy.set_cell_from_structure(structure)
    distance_kpoints_in_mesh = np.average(np.array([np.linalg.norm(b)/float(k) 
                                for pbc,k,b in 
                                zip(pbcs,kpoints_mesh, kpoints_dummy.reciprocal_cell)
                                if pbc]))
    # update the distance between k-points and prepare the new kpoints
    the_kpoints_mesh = [int(np.ceil(round(np.linalg.norm(b)/(distance_kpoints_in_mesh*distance_step_factor),5)))
                        if pbc else 1 for pbc,b in zip(pbcs,kpoints_dummy.reciprocal_cell)]
    # force the mesh to be "even"
    the_kpoints_mesh = [k + (k % 2) if pbc else 1
                        for pbc,k in zip(pbcs,the_kpoints_mesh)]
    # force the mesh to increase in any case
    the_kpoints_mesh = [k if (k>kold or not pbc) else k+2 
                        for (pbc,k,kold) in zip(pbcs,the_kpoints_mesh,kpoints_mesh)]
    the_kpoints=KpointsData()
    # We keep the same mesh offset as before
    the_kpoints.set_kpoints_mesh(the_kpoints_mesh,kpoints.get_kpoints_mesh()[1])
    the_distance_kpoints_in_mesh = np.average(np.array([np.linalg.norm(b)/float(k) 
                                for pbc,k,b in 
                                zip(pbcs,the_kpoints_mesh, kpoints_dummy.reciprocal_cell)
                                if pbc]))
    return {'output_kpoints': the_kpoints,
             'output_parameters': ParameterData(dict={
                    'distance_kpoints_in_mesh': the_distance_kpoints_in_mesh})
            }














class Pwkpoints_nicoWorkflow(Workflow):
    """
    
    This is Nicos workflow and we run here only at fixed kpoint densities
    as input we get a list of densities
    'rec_distances': float
    
    Workflow to check the convergence of pw scf vs k-points, and to find an optimal
    value for the smearing parameter (the one that leads to the smallest number
    of k-points when converged).

    Additional inputs (compared to the pw workflow) are (all optional, but you
    should have at least one convergence threshold defined and higher than zero):
        'force_convergence_threshold': a float -> the convergence threshold on the forces (eV/angstrom),
        'energy_convergence_threshold': a float -> the convergence threshold on the energy (eV)
        'stress_convergence_threshold': a float -> the convergence threshold on the stress (GPa)
        'smearing_parameters_to_test': a list -> the list of smearing parameters to test (Ry) (can be empty),
    
    .. note:: The units on the thresholds are NOT those of etot_conv_thr, forc_conv_thr 
        and press_conv_thr in QE-PW (resp. Ry, Ry/Bohr and kbar). All convergence 
        criteria are on absolute values (i.e. not in relative).
    """
    # Default values
    
    def __init__(self,**kwargs):
        super(Pwkpoints_nicoWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        """
        Starting step only verifies the input parameters
        """
        self.append_to_report("Checking input parameters")
        
        # define the mandatory keywords and the corresponding description to be 
        # printed in case the keyword is missing, for the PW parameters
        mandatory_keys = []
        
        # retrieve and check the parameters
        params = self.get_parameters()
        helpers.validate_keys(params,mandatory_keys)
        
        # Note: I'm not checking extensively all the keys used by the workflow
        self.next(self.run_pw_smearing_loop)
        
    @Workflow.step
    def run_pw_smearing_loop(self):
        # do a loop on the list of smearings if present, otherwise do only
        # a single smearing (or no smearing), as defined in params['parameters']
        
        main_params = self.get_parameters()
        # retrieve PW parameters
        pw_params = {}
        for k,v in main_params.iteritems():
            if k.startswith('pw_'):
                new_k = k[3:] # remove pw_
                pw_params[new_k] = v
            elif k == 'group_name':
                # don't pass the group name (will be used to put only the final
                # calculations)
                pass
            else:
                pw_params[k] = v            
                
        if main_params.get('smearing_parameters_to_test',[]):
            
            for degauss in main_params['smearing_parameters_to_test']:
                # launch a wf for each smearing parameter
                update_input_dict = helpers.default_nested_dict()
                update_input_dict['SYSTEM']['degauss'] = degauss
                pw_params['parameters'] = helpers.update_nested_dict(pw_params['parameters'],
                                                                 update_input_dict)
                wf_pw = Pwkpoints_nicoconvergenceWorkflow(params=pw_params)
                wf_pw.store()
                self.append_to_report("Launching PW k-points convergence "
                                      "sub-workflow with smearing parameter= {} "
                                      "(pk: {})".format(
                                        pw_params['parameters']['SYSTEM']['degauss'],
                                        wf_pw.pk))
                self.attach_workflow(wf_pw)
                wf_pw.start()        
        
        else:
            # launch a single k-points convergence workflow
            wf_pw = Pwkpoints_nicoconvergenceWorkflow(params=pw_params)
            wf_pw.store()
            self.append_to_report("Launching PW k-points convergence "
                                  "sub-workflow (pk: {})".format(wf_pw.pk))
            self.attach_workflow(wf_pw)
            wf_pw.start()        
       
        self.next(self.final_step)
                    
    @Workflow.step
    def final_step(self):
        """
        Here we do the analysis of the convergence of the previous two calculations
        """
        
        
        main_params = self.get_parameters()
        structure = main_params['structure']
        # Retrieve the final PW calculation
        # Here I not just want the final PW calculation but all calculations
        wf_list = list(self.get_step(self.run_pw_smearing_loop).get_sub_workflows())
        # check what smearing parameter leads to the smallest kpoints mesh
        
        wf = wf_list[0]
        resultsdict = wf.get_results()
        _, resdi = get_energy_forces_stresses_inline(structure, resultsdict)

        self.add_result('output_kp_convergence_parameters', resdi['output_kp_convergence_parameters'])
    
        
        group_name = main_params.get('group_name',None)
        if group_name:
            # create or get the group
            group, created = Group.get_or_create(name=group_name)
            if created:
                self.append_to_report("Created group '{}'".format(group_name))
            # put the pw calculation into the group
            group.add_nodes(resdi['output_kp_convergence_parameters'])
            self.append_to_report("Adding output to group '{}'".format(group_name))
               
        self.append_to_report("kpoints + smearing convergence workflow completed")
        
        # then exit
        self.next(self.exit)
        

class Pwkpoints_nicoconvergenceWorkflow(Workflow):
    """
    Workflow to check the convergence of pw scf vs k-points (for a single
    smearing parameter).

    Additional inputs (compared to the pw workflow) are (all optional, but you
    should have at least one convergence threshold defined and higher than zero):
        'force_convergence_threshold': a float -> the convergence threshold on the forces (eV/angstrom),
        'energy_convergence_threshold': a float -> the convergence threshold on the energy (eV)
        'stress_convergence_threshold': a float -> the convergence threshold on the stress (GPa)
    
    .. note:: The units on the thresholds are NOT those of etot_conv_thr, forc_conv_thr 
        and press_conv_thr in QE-PW (resp. Ry, Ry/Bohr and kbar). All convergence 
        criteria are on absolute values (i.e. not in relative).
    """
    # Default values
    _default_distance_kpoints_step_factor = 0.8
    _max_kpoints_iterations = 20
    
    def __init__(self,**kwargs):
        super(Pwkpoints_nicoconvergenceWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        self.next(self.run_pw_loop_optimized)
        
        
    @Workflow.step
    def run_pw_loop_optimized(self):
        """
        loop of scf calculations to get convergence over number of k-points
        """
        params = self.get_parameters()
        # Retrieve the list of pw calculations already done in this step
        wf_pw_list = list(self.get_step(self.run_pw_loop_optimized).get_sub_workflows().order_by('ctime'))
        
        has_to_launch = False
        if wf_pw_list:
            if len(wf_pw_list) < len(params['kpoints_density_list']):
                has_to_launch = True
                
                
                self.append_to_report("number of k-points runs done {}".format(len(wf_pw_list)))
                
            
            if has_to_launch:
                # generate new kpoints
                the_distance_kpoints_in_mesh =  params['kpoints_density_list'][len(wf_pw_list)]
                the_kpoints = get_kpoints_from_density_optimized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])
            

        else:
            the_distance_kpoints_in_mesh =  params['kpoints_density_list'][0]
            the_kpoints = get_kpoints_from_density_optimized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])


            has_to_launch = True
            
        if has_to_launch:
            # retrieve PW parameters
            pw_params = {}
            for k,v in params.iteritems():
                if k.startswith('pw_'):
                    new_k = k[3:] # remove pw_
                    pw_params[new_k] = v
                else:
                    pw_params[k] = v
            
            pw_params['input']['relaxation_scheme'] = 'scf'
            pw_params['input']['finish_with_scf'] = False
    
            for namelist in ['IONS', 'CELL']:
                pw_params['parameters'].pop(namelist,None)
                
            update_input_dict = helpers.default_nested_dict()
            update_input_dict['CONTROL']['tstress'] = True
            update_input_dict['CONTROL']['tprnfor'] = True
            pw_params['parameters'] = helpers.update_nested_dict(pw_params['parameters'],
                                                                 update_input_dict)
            
            pw_params['kpoints'] = the_kpoints
            
            wf_pw = PwWorkflow(params=pw_params)
            wf_pw.store()
            self.append_to_report("Launching scf PW sub-workflow with "
                                  "distance between kpoints in mesh for optimized mesh = {} ,"
                                  " optimized kpoints mesh= {} (pk: {})"
                                  "".format(the_distance_kpoints_in_mesh,
                                    the_kpoints.get_kpoints_mesh(),wf_pw.pk))
            self.attach_workflow(wf_pw)
            wf_pw.start()
            self.next(self.run_pw_loop_optimized)
        
        else:
            self.next(self.run_pw_loop_renormalized)
        
        
        
    @Workflow.step
    def run_pw_loop_renormalized(self):
        """
        loop of scf calculations to get convergence over number of k-points
        """
        params = self.get_parameters()
        # Retrieve the list of pw calculations already done in this step
        wf_pw_list = list(self.get_step(self.run_pw_loop_renormalized).get_sub_workflows().order_by('ctime'))
        
        has_to_launch = False
        if wf_pw_list:
            if len(wf_pw_list) < len(params['kpoints_density_list']):
                has_to_launch = True
                
                
                self.append_to_report("number of k-points runs done {}".format(len(wf_pw_list)))
                
            
            if has_to_launch:
                # generate new kpoints
                the_distance_kpoints_in_mesh =  params['kpoints_density_list'][len(wf_pw_list)]
                the_kpoints = get_kpoints_from_density_renormalized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])
            

        else:
            the_distance_kpoints_in_mesh =  params['kpoints_density_list'][0]
            the_kpoints = get_kpoints_from_density_renormalized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])


            has_to_launch = True
            
        if has_to_launch:
            # retrieve PW parameters
            pw_params = {}
            for k,v in params.iteritems():
                if k.startswith('pw_'):
                    new_k = k[3:] # remove pw_
                    pw_params[new_k] = v
                else:
                    pw_params[k] = v
            
            pw_params['input']['relaxation_scheme'] = 'scf'
            pw_params['input']['finish_with_scf'] = False
    
            for namelist in ['IONS', 'CELL']:
                pw_params['parameters'].pop(namelist,None)
                
            update_input_dict = helpers.default_nested_dict()
            update_input_dict['CONTROL']['tstress'] = True
            update_input_dict['CONTROL']['tprnfor'] = True
            pw_params['parameters'] = helpers.update_nested_dict(pw_params['parameters'],
                                                                 update_input_dict)
            
            pw_params['kpoints'] = the_kpoints
            
            wf_pw = PwWorkflow(params=pw_params)
            wf_pw.store()
            self.append_to_report("Launching scf PW sub-workflow with "
                                  "distance between kpoints in mesh for renormalized mesh = {} ,"
                                  " renormalized kpoints mesh= {} (pk: {})"
                                  "".format(the_distance_kpoints_in_mesh,
                                    the_kpoints.get_kpoints_mesh(),wf_pw.pk))
            self.attach_workflow(wf_pw)
            wf_pw.start()
            self.next(self.run_pw_loop_renormalized)
        
        else:
            self.next(self.final_step)
        
    @Workflow.step
    def final_step(self):
        
        # Retrieve the two last PW calculations
        wf_pw_list_renormalized = list(self.get_step(self.run_pw_loop_renormalized).get_sub_workflows())
        # the following is the last calculation, over converged
        wf_pw_list_optimized = list(self.get_step(self.run_pw_loop_optimized).get_sub_workflows())
        for i, pa in enumerate(wf_pw_list_renormalized):
            self.add_result("pw_calculations_renormalized_output_parameters_" + str(i+1) , pa.get_result('pw_calculation').out.output_parameters)
        for i, pa in enumerate(wf_pw_list_optimized):
            self.add_result("pw_calculations_optimized_output_parameters_" + str(i+1) , pa.get_result('pw_calculation').out.output_parameters)
                              
        
        self.append_to_report("renormalized and optimized k-points convergence workflow completed")
        
        # then exit
        self.next(self.exit)
        

class Pwkpoints_nicoconvergenceFWorkflow(Workflow):
    """
    Workflow to check the convergence of pw scf vs k-points (for a single
    smearing parameter).

    Additional inputs (compared to the pw workflow) are (all optional, but you
    should have at least one convergence threshold defined and higher than zero):
        'force_convergence_threshold': a float -> the convergence threshold on the forces (eV/angstrom),
        'energy_convergence_threshold': a float -> the convergence threshold on the energy (eV)
        'stress_convergence_threshold': a float -> the convergence threshold on the stress (GPa)
    
    .. note:: The units on the thresholds are NOT those of etot_conv_thr, forc_conv_thr 
        and press_conv_thr in QE-PW (resp. Ry, Ry/Bohr and kbar). All convergence 
        criteria are on absolute values (i.e. not in relative).
    """
    # Default values
    _default_distance_kpoints_step_factor = 0.8
    _max_kpoints_iterations = 20
    
    def __init__(self,**kwargs):
        super(Pwkpoints_nicoconvergenceFWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        self.next(self.run_pw_loop_optimized)
        
        
    @Workflow.step
    def run_pw_loop_optimized(self):
        """
        loop of scf calculations to get convergence over number of k-points
        """
        params = self.get_parameters()
        # Retrieve the list of pw calculations already done in this step
        wf_pw_list = list(self.get_step(self.run_pw_loop_optimized).get_sub_workflows().order_by('ctime'))
        
        has_to_launch = False
        if wf_pw_list:
            if len(wf_pw_list) < len(params['kpoints_density_list']):
                has_to_launch = True
                
                
                self.append_to_report("number of k-points runs done {}".format(len(wf_pw_list)))
                
            
            if has_to_launch:
                # generate new kpoints
                the_distance_kpoints_in_mesh =  params['kpoints_density_list'][len(wf_pw_list)]
                the_kpoints = get_kpoints_from_density_optimized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])
            

        else:
            the_distance_kpoints_in_mesh =  params['kpoints_density_list'][0]
            the_kpoints = get_kpoints_from_density_optimized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])


            has_to_launch = True
            
        if has_to_launch:
            # retrieve PW parameters
            pw_params = {}
            for k,v in params.iteritems():
                if k.startswith('pw_'):
                    new_k = k[3:] # remove pw_
                    pw_params[new_k] = v
                else:
                    pw_params[k] = v
            
            pw_params['input']['relaxation_scheme'] = 'scf'
            pw_params['input']['finish_with_scf'] = False
    
            for namelist in ['IONS', 'CELL']:
                pw_params['parameters'].pop(namelist,None)
                
            update_input_dict = helpers.default_nested_dict()
            update_input_dict['CONTROL']['tstress'] = True
            update_input_dict['CONTROL']['tprnfor'] = True
            pw_params['parameters'] = helpers.update_nested_dict(pw_params['parameters'],
                                                                 update_input_dict)
            
            pw_params['kpoints'] = the_kpoints
            
            wf_pw = PwWorkflow(params=pw_params)
            wf_pw.store()
            self.append_to_report("Launching scf PW sub-workflow with "
                                  "distance between kpoints in mesh for optimized mesh = {} ,"
                                  " optimized kpoints mesh= {} (pk: {})"
                                  "".format(the_distance_kpoints_in_mesh,
                                    the_kpoints.get_kpoints_mesh(),wf_pw.pk))
            self.attach_workflow(wf_pw)
            wf_pw.start()
            self.next(self.run_pw_loop_optimized)
        
        else:
            self.next(self.run_pw_loop_renormalized)
        
        
        
    @Workflow.step
    def run_pw_loop_renormalized(self):
        """
        loop of scf calculations to get convergence over number of k-points
        """
        params = self.get_parameters()
        # Retrieve the list of pw calculations already done in this step
        wf_pw_list = list(self.get_step(self.run_pw_loop_renormalized).get_sub_workflows().order_by('ctime'))
        
        has_to_launch = False
        if wf_pw_list:
            if len(wf_pw_list) < len(params['kpoints_density_list']):
                has_to_launch = True
                
                
                self.append_to_report("number of k-points runs done {}".format(len(wf_pw_list)))
                
            
            if has_to_launch:
                # generate new kpoints
                the_distance_kpoints_in_mesh =  params['kpoints_density_list'][len(wf_pw_list)]
                the_kpoints = get_kpoints_from_density_renormalized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])
            

        else:
            the_distance_kpoints_in_mesh =  params['kpoints_density_list'][0]
            the_kpoints = get_kpoints_from_density_renormalized(params['structure'], the_distance_kpoints_in_mesh, shift = [0,0,0])


            has_to_launch = True
            
        if has_to_launch:
            # retrieve PW parameters
            pw_params = {}
            for k,v in params.iteritems():
                if k.startswith('pw_'):
                    new_k = k[3:] # remove pw_
                    pw_params[new_k] = v
                else:
                    pw_params[k] = v
            
            pw_params['input']['relaxation_scheme'] = 'scf'
            pw_params['input']['finish_with_scf'] = False
    
            for namelist in ['IONS', 'CELL']:
                pw_params['parameters'].pop(namelist,None)
                
            update_input_dict = helpers.default_nested_dict()
            update_input_dict['CONTROL']['tstress'] = True
            update_input_dict['CONTROL']['tprnfor'] = True
            pw_params['parameters'] = helpers.update_nested_dict(pw_params['parameters'],
                                                                 update_input_dict)
            
            pw_params['kpoints'] = the_kpoints
            
            wf_pw = PwWorkflow(params=pw_params)
            wf_pw.store()
            self.append_to_report("Launching scf PW sub-workflow with "
                                  "distance between kpoints in mesh for renormalized mesh = {} ,"
                                  " renormalized kpoints mesh= {} (pk: {})"
                                  "".format(the_distance_kpoints_in_mesh,
                                    the_kpoints.get_kpoints_mesh(),wf_pw.pk))
            self.attach_workflow(wf_pw)
            wf_pw.start()
            self.next(self.run_pw_loop_renormalized)
        
        else:
            self.next(self.final_step)
        
    @Workflow.step
    def final_step(self):
        params = self.get_parameters()
        # Retrieve the two last PW calculations
        wf_pw_list_renormalized = list(self.get_step(self.run_pw_loop_renormalized).get_sub_workflows())
        # the following is the last calculation, over converged
        wf_pw_list_optimized = list(self.get_step(self.run_pw_loop_optimized).get_sub_workflows())
        for i, pa in enumerate(wf_pw_list_renormalized):
	    try:
		wfinstance.add_result("pw_calculation_renormalized_kmesh_" + str(i+1) , pa.get_result('pw_calculation'))
	    except:
		ValueError
	for i, pa in enumerate(wf_pw_list_optimized):
	    try:
		wfinstance.add_result("pw_calculation_optimized_kmesh_" + str(i+1) , pa.get_result('pw_calculation'))
	    except:
		ValueError
	      
        resultsdict = {}
        for i, pa in enumerate(wf_pw_list_renormalized):
            try:
                resultsdict["pw_calculations_renormalized_output_parameters_" + str(i+1)] = pa.get_result('pw_calculation').out.output_parameters
            except:
                pass
        for i, pa in enumerate(wf_pw_list_optimized):
            try:
                resultsdict["pw_calculations_optimized_output_parameters_" + str(i+1)] = pa.get_result('pw_calculation').out.output_parameters
            except:
                pass

        structure = params['structure']
        resultsdict.update({'structure': structure})     
        _, resdi = get_energy_forces_stresses_inline(**resultsdict)

        self.add_result('opt_kpmesh_convergence_results', resdi['output_kp_convergence_parameters'])
                              
        
        self.append_to_report("renormalized and optimized k-points convergence workflow completed")
        
        
        
        group_name = params.get('output_group_name',None)
        if group_name:
            # create or get the group
            group, created = Group.get_or_create(name=group_name)
            if created:
                self.append_to_report("Created group '{}'".format(group_name))
            # put the pw calculation into the group
            group.add_nodes(resdi['output_kp_convergence_parameters'])
            self.append_to_report("Adding output to group '{}'".format(group_name))
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        # the exit
        self.next(self.exit)
