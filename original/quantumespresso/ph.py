# -*- coding: utf-8 -*-
import os, numpy
from aiida.orm.workflow import Workflow
from aiida.orm import DataFactory, CalculationFactory, Code, Group
from aiida.workflows.user.epfl_theos.quantumespresso import helpers
from aiida.common.datastructures import calc_states
from aiida.common.example_helpers import test_and_get_code
from aiida.orm.calculation.inline import make_inline
from aiida.common.exceptions import ValidationError
   
__copyright__ = u"Copyright (c), This file is part of the AiiDA-EPFL Pro platform. For further information please visit http://www.aiida.net/. All rights reserved"
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file."
__version__ = "0.1.0"
__authors__ = "Nicolas Mounet, Andrea Cepellotti, Giovanni Pizzi."

ParameterData = DataFactory('parameter')
KpointsData = DataFactory('array.kpoints')
StructureData = DataFactory('structure')
PhCalculation = CalculationFactory('quantumespresso.ph')


@make_inline
def distribute_qpoints_inline(structure,**kwargs):
    """
    :param retrieved: a FolderData object with the retrieved node of
            a PhCalculation
            
    :return: a dictionary of the form {'qpoints1': qpoint1, 'qpoints2': qpoint2}
        where each qpointN is a KpointsData object with a single qpoint
    """
    from aiida.parsers.plugins.quantumespresso.raw_parser_ph import parse_ph_text_output
    
    FolderData = DataFactory('folder')
    retrieved_ph = kwargs.pop("retrieved")
    
    if kwargs:
        raise ValueError("Unrecognized kwargs left")
    
    if not isinstance(retrieved_ph,FolderData):
        raise ValueError("Input should be a FolderData")
    
    dynmat_file = retrieved_ph.get_abs_path("{}{}".format(
                             PhCalculation()._OUTPUT_DYNAMICAL_MATRIX_PREFIX,0))
    with open(dynmat_file,'r') as f:
        lines = f.readlines()
    
    try:
        _ = [ float(i) for i in lines[0].split()]
    except IndexError:
        raise IndexError("File '{}' is empty (it should contain "
                         "the list of q-points)".format(dynmat_file))
    except ValueError:
        raise ValueError("File '{}' does not contain the list of "
                         "q-points".format(dynmat_file))
    
    # read also text output file to find the cell & lattice parameter
    #out_file = retrieved_ph.get_abs_path("{}".format(PhCalculation()._OUTPUT_FILE_NAME))
    #with open(out_file,'r') as f:
    #    out_lines = f.readlines()
    #out_data,_ = parse_ph_text_output(out_lines)
    #cell = out_data['cell']
    cell = structure.cell
    lattice_parameter = numpy.linalg.norm(cell[0])
    twopiovera = 2.*numpy.pi/lattice_parameter
    # read q-points, converting them from 2pi/a coordinates to angstrom-1
    qpoint_coordinates = [ [float(i)*twopiovera for i in j.split()] for j in lines[2:] ]
    
    qpoint_dict = {}
    for count,q in enumerate(qpoint_coordinates):
        qpoint = KpointsData()
        qpoint.set_cell(cell)
        qpoint.set_kpoints([q],cartesian=True)
        qpoint_dict['qpoint_{}'.format(count)] = qpoint
    
    return qpoint_dict


@make_inline
def recollect_qpoints_inline(**kwargs):
    """
    Collect dynamical matrix files into a single folder, putting a different
    number at the end of each final dynamical matrix file corresponding to
    its place in the list of q-points.
    
    :param **kwargs: all the folder data with the dynamical matrices
        (the initial calculation folder with the list of q-points should be in 
        kwargs['initial_folder'])
    :return: a dictionary of the form {'retrieved': folder}
        where folder is a FolderData with all the dynamical matrices.
    """
    # prepare an empty folder with the subdirectory of the dynamical matrices
    FolderData = DataFactory('folder')
    folder = FolderData()
    folder_path = os.path.normpath(folder.get_abs_path('.'))
    os.mkdir( os.path.join( folder_path, PhCalculation()._FOLDER_DYNAMICAL_MATRIX ) )
    
    initial_folder = kwargs.pop('initial_folder')
    
    # add the dynamical-matrix-0 file first
    file_dynmat0 = "{}{}".format(PhCalculation()._OUTPUT_DYNAMICAL_MATRIX_PREFIX,0)
    folder.add_path( initial_folder.get_abs_path(file_dynmat0), file_dynmat0)
    
    for k, src_folder in kwargs.iteritems():
        file_name = PhCalculation()._OUTPUT_DYNAMICAL_MATRIX_PREFIX
        new_file_name = "{}{}".format(PhCalculation()._OUTPUT_DYNAMICAL_MATRIX_PREFIX,
                                      int(k.split('_')[1])+1)
        folder.add_path( src_folder.get_abs_path(file_name), new_file_name )
    
    return {'retrieved': folder}


def get_ph_calculation(wf_params, only_initialization=False, parent_calc=None):
    """
    :return ph_calc: a stored PhCalculation, ready to be submitted
    """
    # default max number of seconds for a calculation with only_initialization=True
    # (should be largely sufficient)
    default_max_seconds_only_init = 1800

    # ph.x has 5% time less than the wall time, to avoid the scheduler kills 
    # the calculation before it safely stops
    ph_parameters = wf_params["parameters"]
    if 'max_seconds' not in ph_parameters['INPUTPH']:
        max_wallclock_seconds = wf_params['calculation_set']['max_wallclock_seconds']
        ph_parameters['INPUTPH']['max_seconds'] = int(0.95*max_wallclock_seconds)
    
    if parent_calc is None:
        code = Code.get_from_string(wf_params['codename'])
        calc = code.new_calc()
        parent_calc = wf_params['pw_calculation']
        calc.use_parent_calculation(parent_calc)
    else:
        calc = parent_calc.create_restart(force_restart=True)
        ph_parameters['INPUTPH']['recover'] = True
            
    # by default, set the resources as in the parent calculation
    calc.set_resources(parent_calc.get_resources())
    # then update resources & time using calculation_set
    calc = helpers.set_the_set(calc, wf_params['calculation_set'])

    calc.use_parameters(ParameterData(dict=ph_parameters))    
    calc.use_qpoints(wf_params['qpoints'])

    if ( ('settings' in wf_params or 'settings' in parent_calc.get_inputs_dict())
         or only_initialization ):
        settings_dict = parent_calc.get_inputs_dict().get('settings',
                                                          ParameterData(dict={})).get_dict()
        
        # clean-up unwanted variables
        # TODO: probably other variables need to be cleaned up
        try:
            del settings_dict['also_bands'] # Only accepted by pw
        except KeyError:
            # It was not there, all good
            pass

        settings_dict = helpers.update_nested_dict(settings_dict, wf_params.get('settings', {}))
        if only_initialization:
            settings_dict['ONLY_INITIALIZATION'] = only_initialization
            calc.set_max_wallclock_seconds(default_max_seconds_only_init)
        calc.use_settings(ParameterData(dict=settings_dict))
    
    calc.store_all()
    return calc
        

class PhWorkflow(Workflow):
    """
    General workflow to launch Phonon calculations
    
    Docs missing
    """
    _clean_workdir = False
    _use_qgrid_parallelization = False
    _default_QE_epsil = False
    
    def __init__(self,**kwargs):
        super(PhWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        """
        Check the input parameters of the Workflow
        """
        self.append_to_report("Checking PhWorkflow input parameters")
        
        mandatory_ph_keys = [('codename',basestring,'The name of the ph.x code'),
                             ('qpoints',KpointsData,'A KpointsData object with the qpoint mesh used by PHonon'),
                             ('calculation_set',dict,'A dictionary with resources, walltime, ...'),
                             ('parameters',dict,"A dictionary with the PH input parameters"),
                             ]
        
        main_params = self.get_parameters()
        
        helpers.validate_keys(main_params, mandatory_ph_keys)
        
        # check the ph code
        test_and_get_code(main_params['codename'], 'quantumespresso.ph',
                          use_exceptions=True)
        
        # ATM only qgrid parallelization is supported
        _ = main_params.get('input',{}).get('use_qgrid_parallelization', 
                                                              self._use_qgrid_parallelization)
        
        # Avoid a frequent crash due to input
        if ( main_params['pw_calculation'].res.smearing_method and 
              main_params['parameters']['INPUTPH'].get('epsil', self._default_QE_epsil) ):
            raise ValueError("Metals require parameter['INPUTPH']['epsil'] = False")
        
        self.next(self.run_ph)
        
    @Workflow.step
    def run_ph(self):
        """
        Launch the right subworkflow with parallelization or restart
        """
        main_params = self.get_parameters()
        
        use_qgrid = main_params.get('input',{}).get('use_qgrid_parallelization',
                                                    self._use_qgrid_parallelization)
        
        if use_qgrid:
            wf_ph = PhqgridWorkflow(params=main_params)
        else:
            wf_ph = PhrestartWorkflow(params=main_params)
        
        wf_ph.store()
        self.append_to_report("Launching PH sub-workflow (pk: {})".format(wf_ph.pk))
        self.attach_workflow(wf_ph)
        wf_ph.start()
        
        self.next(self.final_step)
        
    @Workflow.step   
    def final_step(self):
        """
        Save results
        """
        main_params = self.get_parameters()
        
        # Retrieve the PH result (calculation or folder)
        wf_ph = self.get_step(self.run_ph).get_sub_workflows()[0]
        
        try:
            ph_calc = wf_ph.get_result('ph_calculation')
            self.add_result("ph_calculation", ph_calc)
        except ValueError:
            # some workflows (parallel cases) do not finish with a PhCalculation
            pass
        ph_folder = wf_ph.get_result('ph_folder')
        self.add_result("ph_folder", ph_folder)
    
        group_name = main_params.get('group_name',None)
        if group_name is not None:
            # create or get the group, and add the calculation
            group, created = Group.get_or_create(name=group_name)
            if created:
                self.append_to_report("Creating group '{}'".format(group_name))
            self.append_to_report("Adding result to group '{}'"
                                  "".format(group_name))
            group.add_nodes(ph_folder)
        
        self.append_to_report("PH workflow completed")
        
        # clean scratch leftovers, if requested
        if main_params.get('input',{}).get('clean_workdir',self._clean_workdir):
            self.append_to_report("Cleaning scratch directories")
            try:
                save_calcs = [ ph_calc ]
            except NameError:
                save_calcs = []
            helpers.wipe_all_scratch(self, save_calcs)
            
        self.next(self.exit)


class PhqgridWorkflow(Workflow):
    """
    Subworkflow to run QE ph.x code with parallelization on a grid of q-points.
    ph.x is launched on individual q-points, and then the dynamical matrices are 
    collected.
    
    To be called through PhWorkflow.
    """
    try: 
        _default_QE_epsil = PhWorkflow._default_QE_epsil
    except (NameError,AttributeError):
        _default_QE_epsil = False

    def __init__(self,**kwargs):
        super(PhqgridWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        """
        Phonon initialization
        """
        self.append_to_report("Starting PH workflow with q-points parallelization")
        
        # runs a fake ph computation (stopped right away) to check the q-points list
        params = self.get_parameters()
        
        ph_calc = get_ph_calculation(params, only_initialization=True)
        self.append_to_report("Launching ph.x initialization (pk: {})"
                              "".format(ph_calc.pk))
        self.attach_calculation(ph_calc)
        self.next(self.run_ph_grid)
        
    @Workflow.step
    def run_ph_grid(self):
        """
        Launch parallel runs on the q-grid
        """
        # runs the parallel ph jobs for each q-point 
        main_params = self.get_parameters()
        
        # Retrieve the list of q-points from the previous step
        ph_init_calc = self.get_step_calculations(self.start)[0]
        
        try:
            structure = ph_init_calc.inp.parent_calc_folder.inp.remote_folder.out.output_structure
        except AttributeError:
            structure=ph_init_calc.inp.parent_calc_folder.inp.remote_folder.inp.structure
        
        _, qpoints_dict = distribute_qpoints_inline(structure=structure,
                                                    retrieved=ph_init_calc.out.retrieved)

        compute_epsil = main_params['parameters']['INPUTPH'].get('epsil', self._default_QE_epsil)        
        for k in sorted(qpoints_dict.iterkeys()):
            qpoint = qpoints_dict[k]
            
            # Launch the PH computation for this q
            main_params['qpoints'] = qpoint
            
            if not all([_==0. for _ in qpoint.get_kpoints()[0]]):
                # if it's not gamma, we cannot compute the dielectric constant
                main_params['parameters']['INPUTPH']['epsil'] = False
            else:
                # We need to restore the parameter set by the user/default
                 main_params['parameters']['INPUTPH']['epsil'] = compute_epsil
            
            wf_ph = PhrestartWorkflow(params=main_params)
            wf_ph.store()
            self.append_to_report("Launching Phrestart pk: {}".format(wf_ph.pk))
            self.attach_workflow(wf_ph)
            wf_ph.start()

        self.next(self.final_step)
        
    @Workflow.step
    def final_step(self):
        """
        Collect all dynamical matrices back in one folder
        """
        # Find the FolderData with the dynmat-0 file
        init_calc = self.get_step_calculations(self.start)[0]
        retrieved_dict = {'initial_folder': init_calc.out.retrieved}
        
        # now the other retrieved folders
        ph_subworkflows = self.get_step(self.run_ph_grid).get_sub_workflows()
        all_retrieved = [ _.get_result('ph_folder') for _ in ph_subworkflows ]
        all_links = [ _.get_parameter('qpoints').get_inputs_dict().keys()[0] 
                      for _ in ph_subworkflows ]
        
        for retrieved, link in zip(all_retrieved, all_links):
            i_point = int(link.split('_')[1])
            retrieved_dict["retrieved_{}".format(i_point)] = retrieved
        
        # Launch the inline calculation to collect the dynamical matrices
        _, ret_dict = recollect_qpoints_inline(**retrieved_dict)
        self.append_to_report("Dynamical matrices recollected")
        
        ph_folder = ret_dict['retrieved']
        self.add_result("ph_folder", ph_folder)
        
        self.append_to_report("PH grid workflow completed (ph folder pk: {})"
                              "".format(ph_folder.pk))
        self.next(self.exit)


class PhrestartWorkflow(Workflow):
    """
    Subworkflow to handle a single QE ph.x run with a restart management in 
    case the wall time is exceeded or other kinds of failures.
    
    To be used through PhWorkflow or PhqgridWorkflow.
    """
    _max_restarts = 10
    _default_alpha_mix_from_QE = 0.7
    
    def __init__(self,**kwargs):
        super(PhrestartWorkflow, self).__init__(**kwargs)
    
    @Workflow.step
    def start(self):
        """
        Just the start
        """
        self.append_to_report("Starting PH restart workflow")
        self.next(self.run_ph_restart)
        
    @Workflow.step
    def run_ph_restart(self):
        """
        Looped step
        """
        
        # launch Phonon code, or restart it if maximum wall time was exceeded,
        # or if ph did not reach the normal end of execution.
        # go to final_step when computation succeeded in previous step.
        
        # retrieve PH parameters
        params = self.get_parameters()
        
        # Retrieve the list of PH calculations already done in this step
        ph_calc_list = list(self.get_step_calculations(self.run_ph_restart).order_by('ctime'))
            
        # retrieve attributes
        attr_dict = self.get_attributes()
        # check if previous calculation has failed unexpectedly (not due to time
        # limit nor with the warning 'QE ph run did not reach the end of the 
        # execution.')
        # when has_failed is True, we try to relaunch again ONLY ONCE
        has_failed = attr_dict.get('has_failed',False)
        submission_has_failed = attr_dict.get('submission_has_failed',False)
        # parameters to update, in case they need to be changed after this step
        update_params = helpers.default_nested_dict()
        
        has_finished = False
 
        if ph_calc_list: # if there is at least one previous calculation
            # Analyses what happened with the previous calculation
            
            # Retrieve the last PH calculation done inside this subworkflow
            ph_calc = ph_calc_list[-1]
            
            # test if it needs to be restarted
            if ph_calc.has_finished_ok():
                # computation succeeded -> go to final step
                # ph_calc is the new last clean calculation
                self.add_attribute('last_clean_calc',ph_calc)  
                # reinitialize has_failed and submission_has_failed attributes
                self.add_attribute('has_failed',False)
                self.add_attribute('submission_has_failed',False)
                
                has_finished = True
                                
            if ph_calc.get_state() in [calc_states.SUBMISSIONFAILED]:
                # case when submission failed, probably due to an IO error
                # -> try to restart once
                if submission_has_failed:
                    # this is already the second time the same submission
                    # fails -> workflow stops
                    self.append_to_report("ERROR: ph.x (pk: {0}) submission failed "
                                          "unexpectedly a second time".format(ph_calc.pk))
                    raise ValueError("ERROR: submission failed twice")
                else:
                    # first time this happens -> try to re-submit once
                    self.append_to_report("WARNING: ph.x (pk: {}) submission failed "
                                          "unexpectedly, will try to restart"
                                          " once again".format(ph_calc.pk))
                    # set submission_has_failed attribute
                    self.add_attribute('submission_has_failed',True)
                    
            else:
                # submission did not fail -> reinitialize the
                # submission_has_failed attribute
                self.add_attribute('submission_has_failed',False)
                
            if ph_calc.get_state() in [calc_states.FAILED]:
                if 'Maximum CPU time exceeded' in ph_calc.res.warnings:
                    # maximum CPU time was exceeded -> will restart
                    self.append_to_report("ph.x calculation (pk: {0}) stopped "
                                          "because max CPU time was "
                                          "reached; restarting computation "
                                          "where it stopped"
                                          "".format(ph_calc.pk))
                    # ph_calc is the new last clean calculation
                    self.add_attribute('last_clean_calc',ph_calc)
                    # reinitialize has_failed attribute (here it is not a 
                    # "real" failure)
                    self.add_attribute('has_failed',False)

                elif ( ("QE ph run did not reach the end of the execution." in 
                       ph_calc.res.parser_warnings) and len(ph_calc.res.warnings)==0 ):
                    # case of an unexpected stop during an scf step -> try again
                    # from the last clean calculation
                    max_sec = ph_calc.inp.parameters.get_dict()['INPUTPH']['max_seconds']
                    self.append_to_report("WARNING: ph.x (pk: {}) did not reach "
                                          "end of execution -> will try to restart"
                                          " with max_seconds={}".format(ph_calc.pk,
                                                                       int(max_sec*0.95)))
                    # Note: ph_calc is not a clean calculation, so we do
                    # not update the 'last_clean_calc' attribute

                    # we reduce the max wall time in the pw input file, to avoid
                    # stopping in the middle of an scf step
                    update_params['parameters']['INPUTPH']['max_seconds'] = int(max_sec*0.95)

                elif any(["read_namelists" in w 
                                        for w in ph_calc.res.warnings]):   
                    # any other case leads to stop on error message
                    self.append_to_report("ERROR: incorrect input file for ph.x "
                                          "calculation (pk: {0}) , stopping; "
                                          "list of warnings:\n {1}".format(
                                                ph_calc.pk,ph_calc.res.warnings))
                    raise ValueError("ERROR: incorrect input parameters")
            
                elif ( "Phonon did not reach end of self consistency" in 
                       ph_calc.res.warnings ):
                    # case of a too slow scf convergence -> try again with 
                    # smaller mixing beta
                    alpha_mix = ph_calc.inp.parameters.get_dict()['INPUTPH'].get(
                                    'alpha_mix(1)',self._default_alpha_mix_from_QE)
                    self.append_to_report("WARNING: ph.x (pk: {}) scf convergence"
                                          " too slow -> will try with "
                                          "alpha_mix={}".format(ph_calc.pk,
                                                                alpha_mix*0.9))
                    # ph_calc is the new last clean calculation
                    self.add_attribute('last_clean_calc',ph_calc)
                    
                    # update parameters
                    update_params["parameters"]["INPUTPH"]["alpha_mix(1)"] = alpha_mix*0.9
                    
                    # reinitialize has_failed attribute (here it is not a 
                    # "real" failure)
                    self.add_attribute('has_failed',False)

                else:
                    # case of a real failure
                    if has_failed:
                        # this is already the second time the same calculation
                        # fails -> workflow stops
                        self.append_to_report("ERROR: ph.x (pk: {0}) failed "
                                              "unexpectedly a second time-> "
                                              "stopping;\n list of warnings:\n {1} "
                                              "\n list of parser warnings:\n {2}"
                                              "".format(ph_calc.pk,
                                                        ph_calc.res.warnings,
                                                        ph_calc.res.parser_warnings))
                        raise Exception("ERROR: the same calculation failed "
                                        "twice")
                    else:
                        # first time this happens -> try to restart from the 
                        # last clean calculation
                        self.append_to_report("WARNING: ph.x (pk: {}) failed "
                                              "unexpectedly, will try to restart"
                                              " once again".format(ph_calc.pk))
                        # set has_failed attribute
                        self.add_attribute('has_failed',True)
                        # Note: ph_calc is not a clean calculation, so we do
                        # not update the 'last_clean_calc' attribute
                
            if (ph_calc.get_state() not in [calc_states.FINISHED, 
                                            calc_states.FAILED, 
                                            calc_states.SUBMISSIONFAILED]):   
                # any other case leads to stop on error message
                self.append_to_report("ERROR: unexpected state ({0}) of ph.x "
                                      "(pk: {1}) calculation, stopping"
                                      "".format(ph_calc.get_state(),ph_calc.pk))
                raise Exception("ERROR: unexpected state")

        
        # decide what to do next
        if has_finished:
            self.next(self.final_step)
        
        else:        
            # Stop if we reached the max. number of restarts
            if len(ph_calc_list) >= params.get('input',{}).get('max_restarts',
                                                     self._max_restarts):
                # when we exceed the max. number of restarts -> stop on error message
                self.append_to_report("ERROR: Max number of restarts reached "
                                      "(last calc={})".format(ph_calc.pk))
                raise Exception("ERROR: maximum number of restarts reached "
                                "(increase 'max_restarts')")
        
            # retrieve attributes again
            attr_dict = self.get_attributes()
            old_update_params = attr_dict.get('update_params',{})
            # new set of parameters to update
            update_params = helpers.update_nested_dict(old_update_params,update_params)
            self.add_attribute('update_params',update_params)
            # new parameters
            params = helpers.update_nested_dict(params,update_params)
            
            if attr_dict.get('last_clean_calc',None) is None:
                # Initial PH computation
                
                # Launch the phonon code (first trial)
                ph_calc = get_ph_calculation(params)
                self.append_to_report("Launching ph.x (pk: {})".format(ph_calc.pk))
                self.attach_calculation(ph_calc)
                
                # loop step on itself
                self.next(self.run_ph_restart)
            
            else:
                # Restart PH computation
                # retrieve last clean ph calculation
                last_clean_ph_calc = self.get_attribute('last_clean_calc')
                # prepare restarted calculation
                ph_new_calc = get_ph_calculation(params,parent_calc=last_clean_ph_calc)
                # Launch restarted calculation
                self.append_to_report("Launching ph.x (pk: {}) from previous "
                                      "calculation (pk: {})".format(ph_new_calc.pk,
                                                                   last_clean_ph_calc.pk))
                self.attach_calculation(ph_new_calc)

                # loop step on itself
                self.next(self.run_ph_restart)
                
    @Workflow.step   
    def final_step(self):
        """
        Create the results
        """
        ph_calc_list = list(self.get_step_calculations(self.run_ph_restart).order_by('ctime'))
        ph_calc = ph_calc_list[-1]
        self.add_result("ph_calculation", ph_calc)
        self.add_result("ph_folder", ph_calc.out.retrieved)
        
        self.append_to_report("PH restart workflow completed")
        self.next(self.exit)

