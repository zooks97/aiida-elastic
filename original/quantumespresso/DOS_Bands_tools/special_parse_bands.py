


import xml.dom.minidom
import os
import string
from aiida.parsers.plugins.quantumespresso.constants import ry_to_ev,hartree_to_ev,bohr_to_ang,ry_si,bohr_si
from aiida.parsers.plugins.quantumespresso import QEOutputParsingError




from aiida.orm.calculation.job.quantumespresso.pw import PwCalculation
from aiida.parsers.plugins.quantumespresso.raw_parser_pw import (
                            parse_raw_output,QEOutputParsingError)
from aiida.orm.data.parameter import ParameterData
from aiida.parsers.parser import Parser#, ParserParamManager
from aiida.parsers.plugins.quantumespresso import convert_qe2aiida_structure
from aiida.common.datastructures import calc_states
from aiida.common.exceptions import UniquenessError
from aiida.orm.data.array.bands import BandsData
from aiida.orm.data.array.bands import KpointsData




from aiida.orm import DataFactory
from aiida.orm.calculation.inline import make_inline,optional_inline
























# TODO: it could be possible to use info of the input file to parse output. 
# but atm the output has all the informations needed for the parsing.

# parameter that will be used later for comparisons

__copyright__ = u"Copyright (c), This file is part of the AiiDA platform. For further information please visit http://www.aiida.net/.. All rights reserved."
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file"
__version__ = "0.6.0"
__authors__ = "The AiiDA team."

lattice_tolerance = 1.e-5

default_energy_units = 'eV'
units_suffix = '_units'
k_points_default_units = '2 pi / Angstrom'
default_length_units = 'Angstrom'
default_dipole_units = 'Debye'
default_magnetization_units = 'Bohrmag / cell'
default_charge_units = 'e'
default_force_units = 'ev / angstrom'
default_stress_units = 'GPascal'
default_polarization_units = 'C / m^2'

#a = load_workflow(40743)
#a = load_workflow(40744)
#a.get_result('pw_calculation')
#pw = a.get_result('pw_calculation')


#pwc = [293432, 293428]


def parse_bands_pw(pwcalc):

    outfile  = pwcalc.out.retrieved.get_abs_path() + '/path/aiida.out'

    with open(outfile,'r') as eigenval_f:
	data = eigenval_f.read()

	
    relax_steps = " ".join(data.split('End of self-consistent calculation')[1:][0].split('the Fermi energy is')[0].split(' bands (ev):')[1].split('\n')).split('occupation numbers')

    fermi = data.split('the Fermi energy is')[1]
    fermi_energy  = float(fermi[0:100].split('ev')[0])
   
    finaldata = {'bands': [], 'occupations': []}
    for count, i in enumerate(relax_steps):
	if count ==0:
	    
	    finaldata['bands']  = [float(k) for k in i.split(' ') if k != '']
	else:
	    finaldata['occupations']  = [float(k) for k in i.split(' ') if k != '']
	    
    for ii, i in enumerate(finaldata['occupations']):
	if i < 0.5:
	    finaldata['vbmax'] = finaldata['bands'][ii-1]
	    finaldata['cbmin'] = finaldata['bands'][ii]
	    finaldata['bandgap'] = finaldata['bands'][ii]- finaldata['bands'][ii-1]
	    finaldata['fermi_level_midgap'] =  (finaldata['bands'][ii] + finaldata['bands'][ii-1])/2
	    #print  i, ii-1, finaldata['bands'][ii-1], finaldata['bands'][ii], 'average: ', (finaldata['bands'][ii-1]+ finaldata['bands'][ii])/2
	    #print 'fermi_energy', fermi_energy, 'origparsed', pwcalc.res.fermi_energy
	    
	    break    
    
    finaldata['fermi_energy'] = pwcalc.res.fermi_energy
    try:
	finaldata['fermi_energy_env_shift'] = pwcalc.res.fermi_energy_env_shift
    except:
	finaldata['fermi_energy_env_shift'] = 0
    return  finaldata




def transform_step_data_to_bandsinput(stepdata, mag):
    write_to_k = False                                                                                                           
    write_to_occ = False      
    finaldata = {'bands': [], 'occupations': [], 'weights': []}
    if mag:
	banddata = []
	occudata = []
	for relax_steps in stepdata:
	    kpoint, k , l = [], [], []
            for ind , line in enumerate(relax_steps):                                   
		if 'k =' in line:                
		    write_to_k = True
		    write_to_occ = False     
		    k.append([])             
		    indref =  ind         
		    #print line.split('k =')[1].split('(')[0].split(' ')                             
		    re =   [i for i in line.split('k =')[1].split('(')[0].split(' ') if i != '']
		    te =   []
		    for kkk in re:
		        if len(kkk.split('-'))>1:
		            ddd = kkk.split('-')
		            fi = ddd.pop(0)
		            if fi != '':
		                te.append(float(fi))
		            te.extend([float("-"+ii) for ii in ddd])
		      
		        else:
		            te.append(float(kkk[0]))         
		    kpoint.append(te)
		elif 'occupation' in line:
		    write_to_occ = True
		    write_to_k = False                                             
		    l.append([])               
		    indref = ind                                                    
		if write_to_k and ind > indref:
		    testline = [float(kk) for kk in line.split(' ') if kk != '']
		    if testline !=[]:
			k[-1].extend(testline) 
		elif write_to_occ and ind > indref:   
		    testline = [float(kk) for kk in line.split(' ') if kk != '']
		    if testline !=[]:
		        l[-1].extend(testline) 
	    banddata.append(k)
	    occudata.append(l)
	    
	    
	return {'bands': banddata, 'occupations': occudata, 'kpoints' : kpoint, 'bands_units' : 'eV'}
    else:
        relax_steps = stepdata
        kpoint, k , l = [], [], []
        for ind , line in enumerate(relax_steps):                                   
	    if 'k =' in line:                
		write_to_k = True
		write_to_occ = False     
		k.append([])             
		indref =  ind         
		re =   [i for i in line.split('k =')[1].split('(')[0].split(' ') if i != '']
		te =   []
		for kkk in re:
		    if len(kkk.split('-'))>1:
		        ddd = kkk.split('-')
		        fi = ddd.pop(0)
		        if fi != '':
		            te.append(float(fi))
		        te.extend([float("-"+ii) for ii in ddd])
		      
		    else:
		        te.append(float(kkk[0]))         
		kpoint.append(te)
	    elif 'occupation' in line:
		write_to_occ = True
		write_to_k = False                                             
		l.append([])               
		indref = ind                                                    
	    if write_to_k and ind > indref:
		testline = [float(kk) for kk in line.split(' ') if kk != '']
		if testline !=[]:
		    k[-1].extend(testline) 
	    elif write_to_occ and ind > indref:   
		testline = [float(kk) for kk in line.split(' ') if kk != '']
		if testline !=[]:
		    l[-1].extend(testline) 
        return {'bands': [k], 'occupations': [l], 'kpoints' : kpoint, 'bands_units' : 'eV'}
      



def get_step_data(stringin, mag=False):
    if mag:
        stringinup = []
        stringindown = []
        runup = False
        rundown = False
        for line in stringin.split('\n'):
	    if 'SPIN UP' in line:
	        runup = True
	        rundown = False
	    elif 'SPIN DOWN' in line:
	        runup = False
	        rundown = True
	    elif runup and ('SPIN UP' not in line):
		stringinup.append(line)
	    elif rundown and ('SPIN DOWN' not in line):
	        stringindown.append(line)
	return [stringinup, stringindown]
    else:
      stringinup = []
      runup = True
      for line in stringin.split('\n'):
	    if runup:
		stringinup.append(line)
		
		
      return stringinup
    
    
def parse_bands_pw_morek_general(pwcalc):


    outfile  = pwcalc.out.retrieved.get_abs_path() + '/path/aiida.out'
    with open(outfile,'r') as eigenval_f:                                       
	data = eigenval_f.read()     
    # in case of relax we want the last states
    # assume the 
    relax_steps = data.split('End of self-consistent calculation')[1:]
    
    mag = False
    
    if 'SPIN UP' in relax_steps[0]:
        mag = True
    
    
    relax_steps_i_data = [i.split('the Fermi energy is')[0] for i in relax_steps]
    
    last_step = relax_steps_i_data[-1]
    
    datalines = get_step_data(last_step, mag=mag)
    return transform_step_data_to_bandsinput(datalines, mag)

    

    
    
    
@optional_inline    
def build_bandsdata_postproc_inline(output_kpoints =None):
    pwcalc = output_kpoints.inp.output_kpoints

    bands_data = None
    bands_data = parse_bands_pw_morek_general(pwcalc)
    if bands_data:
        import numpy
        # converting bands into a BandsData object (including the kpoints)

        kpoints_for_bands = pwcalc.out.output_kpoints

        try:
            bands_data['occupations'][1]
            the_occupations = bands_data['occupations']
        except IndexError:
            the_occupations = 2.*numpy.array(bands_data['occupations'][0])

        try:
            bands_data['bands'][1]
            bands_energies = bands_data['bands']
        except IndexError:
            bands_energies = bands_data['bands'][0]

        the_bands_data = BandsData()
        the_bands_data.set_kpointsdata(kpoints_for_bands)
        the_bands_data.set_bands(bands_energies,
                                 units = bands_data['bands_units'],
                                 occupations = the_occupations)

        return {'output_band':the_bands_data}
                           
    else:
        raise Exception("Reading_Bands_problem")
    

def build_bandsdata_postproc_newlink(pwcalc =None):
    bands_data = None
    bands_data = parse_bands_pw_morek_general(pwcalc)
    if bands_data:
        import numpy
        # converting bands into a BandsData object (including the kpoints)

        kpoints_for_bands = pwcalc.out.output_kpoints

        try:
            bands_data['occupations'][1]
            the_occupations = bands_data['occupations']
        except IndexError:
            the_occupations = 2.*numpy.array(bands_data['occupations'][0])

        try:
            bands_data['bands'][1]
            bands_energies = bands_data['bands']
        except IndexError:
            bands_energies = bands_data['bands'][0]

        the_bands_data = BandsData()
        the_bands_data.set_kpointsdata(kpoints_for_bands)
        the_bands_data.set_bands(bands_energies,
                                 units = bands_data['bands_units'],
                                 occupations = the_occupations)

        return {'output_band':the_bands_data}
                           
    else:
        raise Exception("Reading_Bands_problem")
        
        
        
        

        
    
    
    
def get_bands_data_results(pwcalc=None, store = False):
    from aiida.orm.calculation.inline import InlineCalculation
    
    inpkp = pwcalc.out.output_kpoints
    
    result_dict = None
    for ic in InlineCalculation.query(inputs=inpkp).order_by('ctime'):
        try:
            if ( ic.get_function_name() == 'build_bandsdata_postproc_inline'
                 and ic.inp.output_kpoints.pk == inpkp.pk
                 and 'output_band' in ic.get_outputs_dict()):
                result_dict = ic.get_outputs_dict()
        except AttributeError:
            pass

    if result_dict is not None:
        print " build_bandsdata_postproc_inline already run -> we do not re-run"
    else:
        print "Launch build_bandsdata_postproc_inline..."
        result_dict = build_bandsdata_postproc_inline(output_kpoints=inpkp,store=store)
    return result_dict
    
    
    
def add_bands_data_to_tree_results(pwcalc=None, store = False):
    from aiida.common.links import LinkType
    
    if 'output_band' in pwcalc.get_outputs_dict():
        result_dict = {'output_band': pwcalc.get_outputs_dict()['output_band']}
        print " add_bands_data_to_tree_results already run -> we do not re-run"
    else:
        print "Launch add_bands_data_to_tree_results..."
        result_dict = build_bandsdata_postproc_newlink(pwcalc=pwcalc)
        for label, n in result_dict.items():
            n.add_link_from(pwcalc, label=label,
                        link_type=LinkType.CREATE)
            if store:
                n.store()
        
    return result_dict    
    
    
    
