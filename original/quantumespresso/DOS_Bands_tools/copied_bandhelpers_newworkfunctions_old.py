# -*- coding: utf-8 -*-

""" Various auxiliary functions used by the pw and phonons workflows """

import collections
from aiida.orm import DataFactory,CalculationFactory
from aiida.orm.calculation.inline import make_inline,optional_inline

from aiida.orm.data.parameter import ParameterData
from aiida.orm.code import Code
from aiida.orm.group import Group
from aiida.orm import DataFactory, load_node
UpfData = DataFactory('upf')

import numpy as np
from aiida.backends.djsite.db import models

CifData = DataFactory('cif')
StructureData = DataFactory('structure')
ParameterData = DataFactory('parameter')
                                                                           
UpfData = DataFactory('upf')  

try:
    from aiida.workflows.user.epfl_theos.quantumespresso.helpers import *
except:
    pass
try:
    from aiida_quantumespresso_epfl.work_tools.new_helpers import *
except:
    pass



import pylab as plt




























__copyright__ = u"Copyright (c), This file is part of the AiiDA-EPFL Pro platform. For further information please visit http://www.aiida.net/. All rights reserved"
__license__ = "Non-Commercial, End-User Software License Agreement, see LICENSE.txt file."
__version__ = "0.1.1"
__authors__ = "Nicolas Mounet, Andrea Cepellotti, Giovanni Pizzi, Gianluca Prandini."


ParameterData = DataFactory('parameter')



	  
	  
	  
	  
	  
	  
	  
	  

@make_inline
def get_qpoints_from_kpoints_inline(parameters,kpoints):
    """
    Build qpoints from kpoints knowing the k/q mesh ratio (in parameters)
    """
    KpointsData = DataFactory('array.kpoints')
    koverq = parameters.get_dict()['k_over_q']
    
    kpointsmesh = kpoints.get_kpoints_mesh()
    qpointsmesh = [k/koverq if k > 1 else 1 for k in kpointsmesh[0]]
    qpoints = KpointsData()
    qpoints.set_kpoints_mesh(qpointsmesh)
    
    return {'qpoints': qpoints}

def apply_argsort(array, indices, axis=-1):
    """
    Apply indices from argsort of a multidimensional array,
    to a multidimensional array
    :param array: array for which one wants to apply the indices
    :param indices: array of indices (result of a np.argsort command on
        an array with the same shape as array)
    :param axis: axis used for the sorting
    :return : the array array[indices]
    
    Example:
        indices = array.argsort(axis)
        new_array = apply_argsort(array, indices, axis=axis)
        # 'new_array' is the sorted array w.r.t. the axis 'axis'
    """
    import numpy as np
    
    i = list(np.ogrid[[slice(x) for x in array.shape]])
    i[axis] = indices
    return array[i]

def default_nested_dict():
    """
    Default nested dictionary with undefined depth
    """
    return collections.defaultdict(default_nested_dict)

def update_nested_dict(orig_dict, update_dict):
    """
    Update a nested dictionary with the content of
    another nested dictionary with the same internal structure.
    :param orig_dict: initial dictionary, to be updated
    :param update_dict: dictionary used to update it
    :return: the updated dictionary
    
    .. note:: the function modifies orig_dict itself - it becomes the same
    as the updated dictionary.
    .. note:: the dictionaries can contain ParameterData objects; 
    in update_dict, they are replaced by their dictionary content; in orig_dict,
    they are kept as ParameterData (updated, unstored, and not linked to anything)
    If two ParameterData objects are found - corresponding to the same keys,
    then an inline calculation symbolising the update of the initial 
    dictionary, is launched (for provenance keeping purposes).
    """
    from copy import deepcopy
    
    @optional_inline
    def update_nested_dict_inline(original_parameters, update_parameters):
        """
        Inline function to update a nested ParameterData with the content of
        another nested ParameterData with the same structure
        :param original_parameters: ParameterData with initial dictionary,
            to be updated.
        :param update_parameters: ParameterData with dictionary used to 
            update it.
        :return: a dictionary of the form
            {'updated_parameters': ParameterData with the updated dictionary}
        """
        def update_nested_dict_recursive(orig_dict, update_dict):
            """
            Recursive function to update a nested dictionary with the content of
            another nested dictionary with the same structure
            :param orig_dict: initial dictionary, to be updated
            :param update_dict: dictionary used to update it
            :return orig_dict: the updated dictionary
            """
            the_orig_dict = deepcopy(orig_dict)
            for k, v in update_dict.iteritems():
                if (isinstance(v, collections.Mapping) and
                    k in the_orig_dict and isinstance(the_orig_dict[k], collections.Mapping)):
                    tmp_dict = update_nested_dict_recursive(the_orig_dict[k], v)
                    the_orig_dict[k] = tmp_dict
                else:
                    the_orig_dict[k] = update_dict[k]
            return the_orig_dict
        
        orig_dict = original_parameters.get_dict()
        update_dict = update_parameters.get_dict()
        # update dictionary orig_dict
        the_orig_dict = update_nested_dict_recursive(orig_dict, update_dict)
        
        return {'updated_parameters': ParameterData(dict=the_orig_dict)}
    
    if isinstance(orig_dict,ParameterData) and isinstance(update_dict,ParameterData):
        result_dict = update_nested_dict_inline(original_parameters=orig_dict,
                                                update_parameters=update_dict,
                                                store=True)
        return result_dict['updated_parameters']
    
    is_orig_param = False
    try:
        the_orig_dict = orig_dict.get_dict()
        is_orig_param = True
    except AttributeError:
        the_orig_dict = orig_dict
    try:
        update_dict = update_dict.get_dict()
    except AttributeError:
        pass
    
    for k, v in update_dict.iteritems():
        if ((isinstance(v, collections.Mapping) or isinstance(v, ParameterData))
            and k in the_orig_dict and (isinstance(the_orig_dict[k], collections.Mapping)
                or isinstance(the_orig_dict[k], ParameterData))):
            tmp_dict = update_nested_dict(the_orig_dict[k], v)
            the_orig_dict[k] = tmp_dict
        else:
            the_orig_dict[k] = update_dict[k]
    
    if is_orig_param:
        return ParameterData(dict=the_orig_dict)
    else:
        return the_orig_dict

def set_the_set(calc,the_set):
    """
    Call ``'calc.set_[key]'`` methods using keys from 
    the additional_set dictionary, then store  calc
    :param calc: an unstored calculation
    :param the_set: a dictionary with the keys and values to be set
    :return calc: an unstored calculation
    """
    if not isinstance(the_set,dict):
        raise ValueError("set must be a dictionary, found instead a {}".format(the_set))
    try:
        calc.set(**the_set)
    except ValueError as e:
        raise ValueError("A key in params['calc_set'] does not correspond"
                         " to any calc.set_[key] method\n{}".format(e.message))
    return calc

def validate_keys(my_dict,mandatory_keys):
    """
    check that a dictionary contains all mandatory keywords.
    :param my_dict: dictionary to check and update
    :param list_mandatory_keywords: list of length-3 tuples of the form
    (keyword required, class type, its description). The description is used in case the 
    keyword is missing from mydict (prints an error message including the
    description). If type is None, it will not check the type of the value of the keyword, type can be a tuple.
    """
    # check the mandatory keyword (raise an error if not present)
    for k,the_type,description in mandatory_keys:
        if k not in my_dict:
            raise KeyError("Mandatory key '{}' is required (value: {})".format(description,k))
        if the_type is not None:
            if not isinstance(my_dict[k],the_type):
                raise TypeError("The value of '{}' should be of type(s) {}".format(k, the_type))

def wipe_all_scratch(w, results_to_save):
    """
    Wipe out all the scratch on the remote cluster used by a workflow and all 
    its subworkflows (found recursively)
    
    :param results_to_save: a list of Calculation objects that will be skipped
    :w: the workflow instance to clean
    """
    from aiida.orm.workflow import Workflow
    from aiida.orm.calculation.job import JobCalculation
    
    if not isinstance(w, Workflow):
        raise TypeError("Parameter w should be a workflow")
    try:
        if not all( [ isinstance(_,JobCalculation) for _ in results_to_save ] ):
            raise TypeError("Parameter results_to_save should be a list of calculations")
    except TypeError:
        raise TypeError("Parameter results_to_save should be a list of calculations")
    
    steps = w.dbworkflowinstance.steps.all()  
    this_calcs = JobCalculation.query(workflow_step__in=steps)
    this_wfs = Workflow.query(parent_workflow_step__in=steps)
    
    for c in this_calcs:
        if c.pk not in [_.pk for _ in results_to_save]:
            try:
                c.out.remote_folder._clean()
            except AttributeError:
                # remote folder does not exist (probably submission of calc. failed)
                pass
            except OSError:
                # work directory was already removed
                pass
    for this_wf in this_wfs:
        wipe_all_scratch(this_wf, results_to_save)

def get_pwparameterdata_from_wfparams(wf_params, 
                                      restart_mode='from_scratch',
                                      calculation = 'scf'):
    """
    Generate parameters for a pw calculation
    :param wf_params: the workflow parameters dictionary
    :param restart_mode: 'from_scratch' or 'restart'
    :param calculation: one of 'scf', 'nscf', 'bands', 'relax', 'vc-relax', etc.
    
    .. note:: wf_params['parameters'] can be a dictionary OR a 
    ParameterData object. In the latter case, the output is also a
    ParameterData object and the process is stored inside an inline calc.
    """
    import numpy as np
   
    @optional_inline
    def build_pw_input_parameters_inline(parameters,pw_parameters):
        """
        Build the parameters of a pw calculation.
        :param parameters: some additional parameters (calculation, restart_mode)
        :param pw_parameters: PW input parameters, to be updated with the
        previous parameters.
        :return: a dictionary of the form
            {'output_pw_parameters': ParameterData with full set of parameters}
        """
        params_dict = parameters.get_dict()
        calculation = params_dict['calculation']
        restart_mode = params_dict['restart_mode']
        
        pw_params_dict = pw_parameters.get_dict()
    
        try:
            pw_params_dict['CONTROL']
        except KeyError:
            pw_params_dict['CONTROL'] = {}
        
        pw_params_dict['CONTROL']['calculation'] = calculation
        pw_params_dict['CONTROL']['restart_mode'] = restart_mode
        
        if calculation in ['scf','nscf','bands']:
            for namelist in ['IONS', 'CELL']:
                pw_params_dict.pop(namelist,None)
        
        if (calculation == 'bands' and 
            'diago_full_acc' not in pw_params_dict.get('ELECTRONS',{})):
            pw_params_dict['ELECTRONS']['diago_full_acc'] = True
        
        if ( 'max_seconds' not in pw_params_dict['CONTROL'] or
             pw_params_dict['CONTROL']['max_seconds'] > 
                0.97*params_dict.get('max_wallclock_seconds',np.inf) ):
            # automatically set max_seconds in the intut if it was not set, and if it
            # was, change it if it has been set too large
            try:
                max_sec = int(params_dict['max_wallclock_seconds']*0.97)
                # 3% time less to avoid  the scheduler kills pw before it safely stops
                pw_params_dict['CONTROL']['max_seconds'] = max_sec
            except KeyError:
                pass
        
        return {'output_pw_parameters': ParameterData(dict=pw_params_dict)}
    
    parameters_dict = {'calculation': calculation,
                       'restart_mode': restart_mode}
    if ('calculation_set' in wf_params and 
        'max_wallclock_seconds' in wf_params['calculation_set']):
        parameters_dict['max_wallclock_seconds'] = wf_params['calculation_set']['max_wallclock_seconds']
    parameters = ParameterData(dict=parameters_dict)
    
    pw_parameters = wf_params['parameters']
    store = isinstance(pw_parameters,ParameterData)
    the_pw_parameters = ParameterData(dict=pw_parameters) \
                        if not store else pw_parameters
    result_dict = build_pw_input_parameters_inline(parameters=parameters,
                                            pw_parameters=the_pw_parameters,
                                            store=store)
    
    return result_dict['output_pw_parameters']

def take_out_npools_from_cmdline(cmdline):
    """
    Wipe out any indication about npools from the cmdline settings
    :param cmdline: list of strings with the cmdline options (as specified in
        pw input settings)
    :return : the new cmdline with the options about npools
    """
    return  [e for i,e in enumerate(cmdline) 
             if (e not in ('-npools','-npool','-nk') 
                 and cmdline[i-1] not in ('-npools','-npool','-nk'))]

def get_pw_calculation(wf_params, only_initialization=False, parent_calc=None,
                         parent_remote_folder=None):
    """
    Returns a stored calculation
    """
    # default max number of seconds for a calculation with only_initialization=True
    # (should be largely sufficient)
    default_max_seconds_only_init = 1800
    
    from aiida.orm.code import Code
    
    calculation = wf_params['input']['relaxation_scheme']
    
    if only_initialization:
        wf_params['calculation_set'] = update_nested_dict(
            wf_params.get('calculation_set',{}),
            {'max_wallclock_seconds': default_max_seconds_only_init,
             'resources': {'num_machines': 1}})
    
    if parent_calc is None:
        code = Code.get_from_string(wf_params["codename"])
        calc = code.new_calc()
        calc.use_structure(wf_params["structure"])
        calc.use_pseudos_from_family(wf_params["pseudo_family"])
        if parent_remote_folder is None:
            pw_parameters = get_pwparameterdata_from_wfparams(wf_params, calculation=calculation)
        else:
            # restart from a remote folder (typically, with charge density)
            pw_parameters = get_pwparameterdata_from_wfparams(wf_params, restart_mode='restart',
                                                              calculation=calculation)
            calc._set_parent_remotedata(parent_remote_folder)
    else:
        if calculation in ['bands']:
            calc = parent_calc.create_restart(force_restart=True,use_output_structure=True)
        else:
            calc = parent_calc.create_restart(force_restart=True)
        pw_parameters = get_pwparameterdata_from_wfparams(wf_params, 
                                restart_mode='restart', calculation=calculation)
    
    if 'vdw_table' in wf_params:
        calc.use_vdw_table(wf_params['vdw_table'])
    calc.use_parameters(pw_parameters)
    calc.use_kpoints(wf_params['kpoints'])
    calc = set_the_set(calc,wf_params.get('calculation_set',{}))
    
    # set the settings if present        
    try:
        settings_dict = wf_params['settings']
    except KeyError:
        try:
            settings_dict = parent_calc.inp.settings.get_dict()
            if calc.inp.parameters.get_dict()['CONTROL']['calculation'] == 'bands':
                # for bands calculation we take out the npools specification
                # from the parent settings as the number of kpoints will
                # be different
                cmdline = settings_dict.get('cmdline',[])
                the_cmdline = take_out_npools_from_cmdline(cmdline)
                if the_cmdline:
                    settings_dict['cmdline'] = the_cmdline
        except AttributeError:
            settings_dict = {}

    if calc.inp.parameters.get_dict()['CONTROL']['calculation'] == 'bands':
        settings_dict['also_bands'] = True
    if only_initialization:
        settings_dict['ONLY_INITIALIZATION'] = only_initialization
        _ = settings_dict.pop('also_bands',None)
    if settings_dict:
        settings = ParameterData(dict=settings_dict)
        calc.use_settings(settings)
    
    calc.store_all()
    return calc






#####################################
# Update by Nicolas Hoermann






@optional_inline
def get_bandgap_inline(parameters,bands):
    """
    Get the band-gap from a BandsData object
    """
    from aiida.orm.data.array.bands import find_bandgap
    BandsData = DataFactory('array.bands')
    
    if not isinstance(bands,BandsData):
        raise ValueError("bands should be a BandsData object")
    
    number_electrons = parameters.get_dict()['number_of_electrons']
    
    is_insulator, band_gap = find_bandgap(bands,
                                          number_electrons=number_electrons)
    
    return {'output_parameters': ParameterData(dict={'is_insulator': is_insulator,
                                                     'band_gap': band_gap,
                                                     'band_gap_units': bands.units }) }




def get_bandgap_function(parameters=None,bands=None, output='dict', **kwargs):
    """
    Get the band-gap from a BandsData object
    """
    from aiida.orm.data.array.bands import find_bandgap
    BandsData = DataFactory('array.bands')
    
    if not isinstance(bands,BandsData):
        raise ValueError("bands should be a BandsData object")
    
    number_electrons = parameters.get_dict()['number_of_electrons']
    
    is_insulator, band_gap = find_bandgap(bands,
                                          number_electrons=number_electrons)

    paramdict={'is_insulator': is_insulator,
                                                     'band_gap': band_gap,
                                                     'band_gap_units': bands.units }

    if output == 'ParameterData':
        return ParameterData(dict=paramdict)         
    else:
        return paramdict 

try:
    from aiida.orm.data.base import Bool, Str, Float, Int
    from aiida.work.workfunction import workfunction

    @workfunction
    def get_bandgap_wf(parameters,bands, **kwargs):
	"""
	Get the results from the cohesive_energy_inline function:
	- if there exists already an inline calculation with the same inputs,
	it does not relaunch it, it gets instead 
	the output dictionary of the previously launched function,
	- otherwise, launches the formation_energy_inline
	function and gets its result.
	
	needs tp be updated
	"""
	md = {}
	if 'metaparams' in kwargs:
	    md = kwargs['metaparams'].get_dict()
	#pm = ParametersGeneratorBinaries()

	
	return get_bandgap_function(parameters=parameters,bands=bands, output='ParameterData', **md)
      


except:
    pass








def find_optical_band_gap(bandsdata, number_electrons=None,
        also_homo_lumo_bandgap=False):
    """
    Tries to guess the optical band gap, i.e. the minimum distance between
    the 'lumo' and the 'homo' (i.e. the direct band gap).
    This method is meant to be used only for electronic bands (not phonons)
    By default, it will try to use the occupations to guess the number of
    electrons, otherwise, it can be provided
    explicitely.
    Also, there is an implicit assumption that the kpoints grid is
    "sufficiently" dense, so that the bandsdata are not missing the
    intersection between valence and conduction band if present.
    Use this function with care!
    
    :param (BandsData): electronic bands
    :param (float) number_electrons: (optional) number of electrons in the unit cell
    :param (boolean) also_homo_lumo_bandgap: (optional) True to also output
        the homo (top of valence bands), lumo (bottom of conduction band)
        and "usual" band gap (i.e. the indirect band gap).

    :note: By default, the algorithm uses the occupations array
      to guess the number of electrons and the occupied bands. This is to be
      used with care, because the occupations could be smeared so at a
      non-zero temperature, with the unwanted effect that the conduction bands
      might be occupied in an insulator.
      Prefer to pass the number_of_electrons explicitly
    :note: bug for band-gap in case of odd number of electrons & 
        non spin-polarized, was fixed on Nov. 22nd, 2017
        (now same as find_bandgap function in file with BandsData class)
    TODO: optical band-gap is completely WRONG!!! homo and lumo are not computed
        correctly for metals!!!

    :return: optical_gap, the minimum distance between homo and lumo in eV
        (it can be negative). If also_lumo_homo_bandgap, also returns
        the homo, lumo and ordinary band gap.
    """
    import numpy
    
    try:
        stored_bands = bandsdata.get_bands()
    except KeyError:
        raise KeyError("Cannot do much of a band analysis without bands")

    if len(stored_bands.shape) == 3:
        # I write the algorithm for the generic case of having both the
        # spin up and spin down array

        # put all spins on one band per kpoint
        bands = numpy.concatenate([_ for _ in stored_bands], axis=1)
    else:
        bands = stored_bands

    num_kpoints = len(bands)

    if number_electrons is None:
        # analysis on occupations to get the number of electrons
        try:
            _, stored_occupations = bandsdata.get_bands(also_occupations=True)
        except KeyError:
            raise KeyError("Cannot determine metallicity if I don't have "
                           "either fermi energy, or occupations")

        # put the occupations in the same order of bands, also in case of multiple bands
        if len(stored_occupations.shape) == 3:
            # I write the algorithm for the generic case of having both the
            # spin up and spin down array

            # put all spins on one band per kpoint
            occupations = numpy.concatenate([_ for _ in stored_occupations], axis=1)
        else:
            occupations = stored_occupations

        # now sort the bands by energy
        # Note: I am sort of assuming that I have an electronic ground state

        # sort the bands by energy, and reorder the occupations accordingly
        # since after joining the two spins, I might have unsorted stuff
        bands, occupations = [numpy.array(y) for y in zip(*[zip(*j) for j in
                                                            [sorted(zip(i[0].tolist(), i[1].tolist()),
                                                                    key=lambda x: x[0])
                                                             for i in zip(bands, occupations)]])]
        number_electrons = int(round(sum([sum(i) for i in occupations]) / num_kpoints))

    else:
        bands = numpy.sort(bands)
        number_electrons = int(number_electrons)

    # find the zero-temperature occupation per band (1 for spin-polarized
    # calculation, 2 otherwise)
    number_electrons_per_band = 4 - len(stored_bands.shape)  # 1 or 2
    # gather the energies of the homo band, for every kpoint
    homo = numpy.array([i[number_electrons / number_electrons_per_band - 1] for i in bands])  # take the nth level
    try:
        # gather the energies of the lumo band, for every kpoint
        lumo = numpy.array([i[number_electrons / number_electrons_per_band] for i in bands])  # take the n+1th level
    except IndexError:
        
        raise ValueError("To compute the optical band gap, "
                         "need more bands than n_band=number_electrons")

    optical_band_gap = numpy.min(lumo-homo)
    
    if not also_homo_lumo_bandgap:
        return optical_band_gap
    else:
        top_valence = numpy.max(homo)
        bottom_conduction = numpy.min(lumo)
        band_gap = None if (number_electrons%2==1 and len(stored_bands.shape)==2) else bottom_conduction-top_valence
        return optical_band_gap,top_valence,bottom_conduction,\
            band_gap if band_gap>=0 else None


            
def optimize_bandwidth(allbands, fermi_energy, window = 10, method = 'standard'):
    relevant_bands = np.sort([i for i in allbands if (i < fermi_energy+0.001 and i > fermi_energy - window)])
    relevant_distances =  [relevant_bands[i+1] - relevant_bands[i]  for i in range(len(relevant_bands)-1)]
    if method == 'primitive':
        avedist = float(window)/len(relevant_distances)
        #med = np.median(relevant_distances)
        #print med
        #plt.show()
        return max(min(avedist, 0.015), 0.004)
    else:

        perc = np.percentile(relevant_distances, q = 85)
        return max(min(perc, 0.010), 0.004)


def return_banddata_for_analysis(bandsdata, parameterdata, number_electrons=None,
        also_homo_lumo_bandgap=False, fermi_energy = None, no_atoms = None,):
  
    import numpy
    
    import scipy.stats
    from weighted_kde import gaussian_kde
    
    
    try:
        stored_bands = bandsdata.get_bands()
    except KeyError:
        raise KeyError("Cannot do much of a band analysis without bands")

    if len(stored_bands.shape) == 3:
        # I write the algorithm for the generic case of having both the
        # spin up and spin down array

        # put all spins on one band per kpoint
        bands = numpy.concatenate([_ for _ in stored_bands], axis=1)

    else:
        bands = stored_bands

    num_kpoints = len(bands)

    if number_electrons is None:
        # analysis on occupations to get the number of electrons
        try:
            _, stored_occupations = bandsdata.get_bands(also_occupations=True)
        except KeyError:
            raise KeyError("Cannot determine metallicity if I don't have "
                           "either fermi energy, or occupations")

        # put the occupations in the same order of bands, also in case of multiple bands
        if len(stored_occupations.shape) == 3:
            # I write the algorithm for the generic case of having both the
            # spin up and spin down array

            # put all spins on one band per kpoint
            occupations = numpy.concatenate([_ for _ in stored_occupations], axis=1)
        else:
            occupations = stored_occupations

        # now sort the bands by energy
        # Note: I am sort of assuming that I have an electronic ground state

        # sort the bands by energy, and reorder the occupations accordingly
        # since after joining the two spins, I might have unsorted stuff
        bands, occupations = [numpy.array(y) for y in zip(*[zip(*j) for j in
                                                            [sorted(zip(i[0].tolist(), i[1].tolist()),
                                                                    key=lambda x: x[0])
                                                             for i in zip(bands, occupations)]])]
        number_electrons = int(round(sum([sum(i) for i in occupations]) / num_kpoints))

    else:
        bands = numpy.sort(bands)
        number_electrons = int(number_electrons)

    # find the zero-temperature occupation per band (1 for spin-polarized
    # calculation, 2 otherwise)
    if fermi_energy == None:
        if parameterdata == None:
	    fermi_energy = bandsdata.inp.output_band.get_outputs_dict()['output_parameters'].get_dict()['fermi_energy']
	else:
	    fermi_energy = parameterdata.get_dict()['fermi_energy']
    if no_atoms == None:
        if parameterdata == None:
            no_atoms = len(bandsdata.inp.output_band.inp.structure.sites)
        else:
	    no_atoms = len(parameterdata.inp.output_parameters.inp.structure.sites)
        
    weights =  np.array([bandsdata.get_array('weights') for i in range(bands.shape[1])]).T
    
    
    number_electrons_per_band = 4 - len(stored_bands.shape)  # 1 or 2
    # gather the energies of the homo band, for every kpoint
    homo = numpy.array([i[number_electrons / number_electrons_per_band - 1] for i in bands])  # take the nth level
    try:
        # gather the energies of the lumo band, for every kpoint
        lumo = numpy.array([i[number_electrons / number_electrons_per_band] for i in bands])  # take the n+1th level
    except IndexError:
        raise ValueError("To compute the optical band gap, "
                         "need more bands than n_band=number_electrons")

    singleweights  = bandsdata.get_array('weights') 
    allbandsweights = weights.flatten()
    allbands = bands.flatten()


    datawindow = [min(allbands), max(allbands)]
    bins = np.arange(np.floor(datawindow[0]-0.2),np.ceil(datawindow[1]+0.2), 0.1 )

    #plt.hist(allbands, bins = bins, weights = allbandsweights/len(allbands)*0.1, normed = False)
    #plt.hist(homo, weights=singleweights/len(homo)*0.1, bins = bins, normed = False)
    
    
    #basically kde smears our by h and adds up gaussians
    # take h as related to the average distance in energy within a range of 1 eV below the fermilevel?
    # but problem with localized states: there fore make a statistical analysis of the clusters
    
    window = 5
    relevant_bands = np.sort([i-fermi_energy for i in allbands if (i < fermi_energy+0.001 and i > fermi_energy - window)])
    neg_bands = np.sort([i-fermi_energy for i in allbands if (i < fermi_energy+0.001)])
    all_bands = np.sort([i-fermi_energy for i in allbands])
    return relevant_bands, neg_bands, all_bands












def get_dos_from_bands_kde_functions(bandsdata, parameterdata, number_electrons=None,
        also_homo_lumo_bandgap=False, fermi_energy = None, no_atoms = None,):


    import numpy
    import pylab as plt
    import scipy.stats
    from weighted_kde import gaussian_kde
    
    
    try:
        stored_bands = bandsdata.get_bands()
    except KeyError:
        raise KeyError("Cannot do much of a band analysis without bands")

    number_electrons, fermi_energy, datawindow, opth, no_atoms, normalization, avgdosfunction = get_dos_from_bands_avg_kde(bandsdata, parameterdata, number_electrons=number_electrons,also_homo_lumo_bandgap=also_homo_lumo_bandgap, fermi_energy = fermi_energy, no_atoms = no_atoms)
    
    if len(stored_bands.shape) < 3:
        relevant_data = {"dos": avgdosfunction, "datawindow": datawindow, "no_atoms": no_atoms, "fermi_energy": fermi_energy, "number_electrons": number_electrons }
        bands = stored_bands
        number_electrons_per_band = 2
        homoindex = int(number_electrons / number_electrons_per_band - 1)

        for bandi in range(bands.shape[1]):
            pi = bands.shape[1]-bandi-1
            if any(bands[:, pi]) <= fermi_energy:
                homoindex = pi
                break

        homoindexall = [homoindex, homoindex]    
        singleweights  = bandsdata.get_array('weights') 
        colors = ['r', 'g']
        for ispin in range(2):
            bands = stored_bands
            num_kpoints = len(bands)

  
            bands = numpy.sort(bands)
            number_electrons = int(number_electrons)
            
            weights =  np.array([singleweights for i in range(bands.shape[1])]).T
    
    
            
            # gather the energies of the homo band, for every kpoint
            #This is maybe wrong for magnetic systems
            
            homo = numpy.array([i[homoindexall[ispin]] for i in bands])  # take the nth level
            try:
                # gather the energies of the lumo band, for every kpoint
                lumo = numpy.array([i[homoindexall[ispin]+1] for i in bands])  # take the n+1th level
            except IndexError:
                lumo = numpy.array([i[homoindexall[ispin]] for i in bands])
                print "To compute the optical band gap, " +  "need more bands than n_band=number_electrons"

            allbandsweights = weights.flatten()
            allbands = bands.flatten()
        

            
            kde = gaussian_kde(allbands, bw_method = opth, weights = allbandsweights)
            def get_kde(kde, x):
                return kde.evaluate(x)
 
	    if ispin == 0:
		relevant_data["dos_up"] = lambda x: get_kde(kde, x)*normalization/2
	    else:
	        relevant_data["dos_down"] = lambda x: get_kde(kde, x)*normalization/2

      
        return  relevant_data
    else:
        
        
        relevant_data = {"dos": avgdosfunction, "datawindow": datawindow, "no_atoms": no_atoms, "fermi_energy": fermi_energy, "number_electrons": number_electrons }
        bands = stored_bands[0]
        number_electrons_per_band = 1
        homoindex = int(number_electrons / number_electrons_per_band / 2 - 1)

        for bandi in range(bands.shape[1]):
            pi = bands.shape[1]-bandi-1
            if any(bands[:, pi]) <= fermi_energy:
                homoindex = pi
                break

        homoindexall = [homoindex, (number_electrons -2)-homoindex]    
        singleweights  = bandsdata.get_array('weights') 
        colors = ['r', 'g']
        for ispin in range(2):
            bands = stored_bands[ispin]
            num_kpoints = len(bands)

  
            bands = numpy.sort(bands)
            number_electrons = int(number_electrons)

            weights =  np.array([singleweights for i in range(bands.shape[1])]).T
    
    
            
            # gather the energies of the homo band, for every kpoint
            #This is maybe wrong for magnetic systems
            
            homo = numpy.array([i[homoindexall[ispin]] for i in bands])  # take the nth level
            try:
                # gather the energies of the lumo band, for every kpoint
                lumo = numpy.array([i[homoindexall[ispin]+1] for i in bands])  # take the n+1th level
            except IndexError:
                lumo = numpy.array([i[homoindexall[ispin]] for i in bands])
                print "To compute the optical band gap, " +  "need more bands than n_band=number_electrons"

            allbandsweights = weights.flatten()
            allbands = bands.flatten()
        

            
            kde = gaussian_kde(allbands, bw_method = opth, weights = allbandsweights)
            def get_kde(kde, x):
                return kde.evaluate(x)
 
	    if ispin == 0:
		relevant_data["dos_up"] = lambda x: get_kde(kde, x)*normalization/2
	    else:
	        relevant_data["dos_down"] = lambda x: get_kde(kde, x)*normalization/2

      
        return  relevant_data  
            

import pickle



def export_dos_data(data, filename):
    with open(filename, 'w') as o:
        pickle.dump(data, o)

def import_dos_data(filename):
    with open(filename, 'r') as o:
        data = pickle.load(o)
    return data

def plot_dos_from_bands_kde_functions(bandsdata, parameterdata, ax, number_electrons=None,
        also_homo_lumo_bandgap=False, fermi_energy = None, no_atoms = None, incldata = True):

    import numpy
    import pylab as plt
    import scipy.stats
    from weighted_kde import gaussian_kde
    
    
    try:
        stored_bands = bandsdata.get_bands()
    except KeyError:
        raise KeyError("Cannot do much of a band analysis without bands")

    number_electrons, fermi_energy, datawindow, opth, no_atoms, normalization, avgdosfunction = get_dos_from_bands_avg_kde(bandsdata, parameterdata, number_electrons=number_electrons,also_homo_lumo_bandgap=also_homo_lumo_bandgap, fermi_energy = fermi_energy, no_atoms = no_atoms)
    testx = np.linspace(-10, 5, 301)
    ax.plot(testx, avgdosfunction(testx+fermi_energy), color = 'k')
    testx = np.linspace(-10, 0, 350)
    ax.fill_between(testx, 0, avgdosfunction(testx+fermi_energy), color = 'grey', alpha = 0.2)
    ax.text(0.2,0.9, 'sigma=' + str(np.round(opth,3)), transform=ax.transAxes)
        
    if len(stored_bands.shape) < 3:
      
        
      
        
        bands = stored_bands
        number_electrons_per_band = 2
        homoindex = int(number_electrons / number_electrons_per_band - 1)

        for bandi in range(bands.shape[1]):
            pi = bands.shape[1]-bandi-1
            if any(bands[:, pi]) <= fermi_energy:
                homoindex = pi
                break

        homoindexall = [homoindex, homoindex]    
        singleweights  = bandsdata.get_array('weights') 
        colors = ['r', 'g']
        for ispin in range(2):
            bands = stored_bands
            num_kpoints = len(bands)

  
            bands = numpy.sort(bands)
            number_electrons = int(number_electrons)
            
            weights =  np.array([singleweights for i in range(bands.shape[1])]).T
    
    
            
            # gather the energies of the homo band, for every kpoint
            #This is maybe wrong for magnetic systems
            
            homo = numpy.array([i[homoindexall[ispin]] for i in bands])  # take the nth level
            try:
                # gather the energies of the lumo band, for every kpoint
                lumo = numpy.array([i[homoindexall[ispin]+1] for i in bands])  # take the n+1th level
            except IndexError:
                lumo = numpy.array([i[homoindexall[ispin]] for i in bands])
                print "To compute the optical band gap, " +  "need more bands than n_band=number_electrons"

            allbandsweights = weights.flatten()
            allbands = bands.flatten()
        
            
                
            
            kde = gaussian_kde(allbands, bw_method = opth, weights = allbandsweights)
            def get_kde(x):
                return kde.evaluate(x)
   
            testx = np.linspace(-10, 5, 301)
            ax.plot(testx, kde.evaluate(testx+fermi_energy)*(2*ispin-1)*normalization/2, color = colors[ispin])
            if incldata:
                hist, binedgeds = np.histogram(allbands, weights=allbandsweights/2/0.1, bins = np.linspace(-10+fermi_energy, 5+fermi_energy, 151))
                w = binedgeds[1] -binedgeds[0]
                binedges =binedgeds[1:]- w/2-fermi_energy
                ax.bar(binedges, hist*(2*ispin-1), width =w,color = 'blue', alpha = 0.3)
            testx = np.linspace(-10, 0, 350)
            if ispin == 0:
                ax.fill_between(testx, 0, kde.evaluate(testx+fermi_energy)*(2*ispin-1)*normalization/2, color = colors[ispin])
            else:
                ax.fill_between(testx, kde.evaluate(testx+fermi_energy)*(2*ispin-1)*normalization/2, 0, color = colors[ispin])
      
      


      
        return  {"dos": avgdosfunction, "datawindow": datawindow, "no_atoms": no_atoms, "fermi_energy": fermi_energy, "number_electrons": number_electrons }
    else:
        
        dod = {"dos": avgdosfunction, "datawindow": datawindow, "no_atoms": no_atoms, "fermi_energy": fermi_energy, "number_electrons": number_electrons }
        bands = stored_bands[0]
        number_electrons_per_band = 1
        homoindex = int(number_electrons / number_electrons_per_band / 2 - 1)

        for bandi in range(bands.shape[1]):
            pi = bands.shape[1]-bandi-1
            if any(bands[:, pi]) <= fermi_energy:
                homoindex = pi
                break

        homoindexall = [homoindex, (number_electrons -2)-homoindex]    
        singleweights  = bandsdata.get_array('weights') 
        colors = ['r', 'g']
        for ispin in range(2):
            bands = stored_bands[ispin]
            num_kpoints = len(bands)

  
            bands = numpy.sort(bands)
            number_electrons = int(number_electrons)

            weights =  np.array([singleweights for i in range(bands.shape[1])]).T
    
    
            
            # gather the energies of the homo band, for every kpoint
            #This is maybe wrong for magnetic systems
            
            homo = numpy.array([i[homoindexall[ispin]] for i in bands])  # take the nth level
            try:
                # gather the energies of the lumo band, for every kpoint
                lumo = numpy.array([i[homoindexall[ispin]+1] for i in bands])  # take the n+1th level
            except IndexError:
                lumo = numpy.array([i[homoindexall[ispin]] for i in bands])
                print "To compute the optical band gap, " +  "need more bands than n_band=number_electrons"

            allbandsweights = weights.flatten()
            allbands = bands.flatten()
        

            
            kde = gaussian_kde(allbands, bw_method = opth, weights = allbandsweights)
            def get_kde(x):
                return kde.evaluate(x)
   
            testx = np.linspace(-10, 5, 301)
            ax.plot(testx, kde.evaluate(testx+fermi_energy)*(2*ispin-1)*normalization/2, color = colors[ispin])
            if incldata:
                hist, binedgeds = np.histogram(allbands, weights=allbandsweights/0.1, bins = np.linspace(-10+fermi_energy, 5+fermi_energy, 151))
                w = binedgeds[1] -binedgeds[0]
                print 'width', w
                binedges =binedgeds[1:]- w/2-fermi_energy
                ax.bar(binedges, hist*(2*ispin-1), width =w,color = 'blue', alpha = 0.3)
            testx = np.linspace(-10, 0, 350)
            if ispin == 0:
                ax.fill_between(testx, 0, kde.evaluate(testx+fermi_energy)*(2*ispin-1)*normalization/2, color = colors[ispin])
            else:
                ax.fill_between(testx, kde.evaluate(testx+fermi_energy)*(2*ispin-1)*normalization/2, 0, color = colors[ispin])
            



def get_bands_info_data(bandsdata, parameterdata, number_electrons=None,
        also_homo_lumo_bandgap=False, fermi_energy = None, no_atoms = None):
    import numpy
    
    try:
        stored_bands, stored_occupations = bandsdata.get_bands(also_occupations=True)
        bands = stored_bands
        occupations  = stored_occupations
        if len(stored_occupations.shape) < 3:
            num_kpoints = len(bands)
        else: 
            num_kpoints = len(bands[0])
            
    except KeyError:
        raise KeyError("Cannot do much of a band analysis without bands")
    if number_electrons == None:
        
        number_electrons = parameterdata.get_dict()['number_of_electrons']


    if len(stored_occupations.shape) < 3:
        number_electrons_per_band = 2
        electrons_per_spin_channel = [float(number_electrons)/2, float(number_electrons)/2]
        
        bands_i, occupations_i = [numpy.array(y) for y in zip(*[zip(*j) for j in
                                                            [sorted(zip(i[0].tolist(), i[1].tolist()),
                                                                    key=lambda x: x[0])
                                                             for i in zip(bands, occupations)]])]
        homos_i = []
        lumos_i = [] 
        for kpointi in range(len(bands_i)):
            b_prev = list(reversed(bands_i[kpointi]))[0]
            for bandind, b,o in zip(np.arange(len(bands_i[kpointi])), list(reversed(bands_i[kpointi])), list(reversed(occupations_i[kpointi]))):
                if o>0.5:
                    homos_i.append([b, len(bands_i[kpointi])-bandind-1])
                    lumos_i.append([b_prev, len(bands_i[kpointi])-bandind])
                    break
                b_prev = b
        homos_i = np.array(homos_i)
        lumos_i = np.array(lumos_i)
        
        bandgap_i = min(lumos_i[:,0]) - max(homos_i[:,0])
        direct_bandgap_i = min(lumos_i[:,0] - homos_i[:,0])
        
        system_is_metallic_i = (bandgap_i <= 0 or any([homos_i[0][1] != j[1] for j in homos_i]))
        if parameterdata == None:
            fermi_energy0 = bandsdata.inp.output_band.get_outputs_dict()['output_parameters'].get_dict()['fermi_energy']
        else:
            fermi_energy0 = parameterdata.get_dict()['fermi_energy']   
        
        
        fermi_energy_i = [max(homos_i[:,0]), fermi_energy0]
        
        
        if system_is_metallic_i:
            
            lumo_eband_bandindex_kpoint_i = None
            homo_eband_bandindex_kpoint_i = None
            bandgap_is_direct_i = None

            
        else:
            lumo_eband_bandindex_kpoint_i = [ np.min(lumos_i[:,0]), int(lumos_i[np.argmin(lumos_i[:,0]), 1]), np.argmin(lumos_i[:,0])]            
            homo_eband_bandindex_kpoint_i = [ np.max(homos_i[:,0]), int(homos_i[np.argmax(homos_i[:,0]), 1]), np.argmax(homos_i[:,0])]
            bandgap_is_direct_i = lumo_eband_bandindex_kpoint_i[-1] == homo_eband_bandindex_kpoint_i[-1]
                            
        bandgap_i = max(bandgap_i, 0)
        direct_bandgap_i = max(direct_bandgap_i, 0)
        
        dadict =  {'number_electrons': number_electrons,
                   'number_of_spin_channels': 3-number_electrons_per_band,
                   'electrons_per_spin_channel' : electrons_per_spin_channel,
                   'bandgap' : [bandgap_i],
                   'direct_bandgap': [direct_bandgap_i],
                   'system_is_metallic'  : [system_is_metallic_i],
                   'fermi_energy' :  [fermi_energy_i],
                   'lumo_eband_bandindex_kpoint' : [lumo_eband_bandindex_kpoint_i],
                   'homo_eband_bandindex_kpoint': [homo_eband_bandindex_kpoint_i],
                   'bandgap_is_direct': [bandgap_is_direct_i],
                   }
        
        return dadict
        
                                      
    else:
        number_electrons_per_band = 1
        electrons_per_spin_channel = []
        bandgap = []
        direct_bandgap = []
        system_is_metallic =[]
        fermi_energy = []
        lumo_eband_bandindex_kpoint = []
        homo_eband_bandindex_kpoint = []
        bandgap_is_direct = []
        homos = []
        lumos = []

                   
        weights =  bandsdata.get_array('weights')                              
        for ispin in range(2):
                        
                                      
            bands_i, occupations_i = [numpy.array(y) for y in zip(*[zip(*j) for j in
                                                                [sorted(zip(i[0].tolist(), i[1].tolist()),
                                                                        key=lambda x: x[0])
                                                                 for i in zip(bands[ispin], occupations[ispin])]])]
            homos_i = []
            lumos_i = [] 
            for kpointi in range(len(bands_i)):
                b_prev = list(reversed(bands_i[kpointi]))[0]
                for bandind, b,o in zip(np.arange(len(bands_i[kpointi])), list(reversed(bands_i[kpointi])), list(reversed(occupations_i[kpointi]))):
                    if o>0.5:
                        homos_i.append([b, len(bands_i[kpointi])-bandind-1])
                        lumos_i.append([b_prev, len(bands_i[kpointi])-bandind])
                        break
                
                    b_prev = b
            
            homos_i = np.array(homos_i)
            lumos_i = np.array(lumos_i)
            
            bandgap_i = min(lumos_i[:,0]) - max(homos_i[:,0])
            direct_bandgap_i = min(lumos_i[:,0] - homos_i[:,0])
            
            system_is_metallic_i = (bandgap_i <= 0 or any([homos_i[0][1] != j[1] for j in homos_i]))
            if parameterdata == None:
                fermi_energy0 = bandsdata.inp.output_band.get_outputs_dict()['output_parameters'].get_dict()['fermi_energy']
            else:
                fermi_energy0 = parameterdata.get_dict()['fermi_energy']   
            
            
            fermi_energy_i = [max(homos_i[:,0]), fermi_energy0]
            
            
            if system_is_metallic_i:
                
                lumo_eband_bandindex_kpoint_i = None
                homo_eband_bandindex_kpoint_i = None
                bandgap_is_direct_i = None
    
                
            else:
                lumo_eband_bandindex_kpoint_i = [ np.min(lumos_i[:,0]), int(lumos_i[np.argmin(lumos_i[:,0]), 1]), np.argmin(lumos_i[:,0])]            
                homo_eband_bandindex_kpoint_i = [ np.max(homos_i[:,0]), int(homos_i[np.argmax(homos_i[:,0]), 1]), np.argmax(homos_i[:,0])]
                bandgap_is_direct_i = (lumo_eband_bandindex_kpoint_i[-1] == homo_eband_bandindex_kpoint_i[-1])
                                
            bandgap_i = max(bandgap_i, 0)
            direct_bandgap_i = max(direct_bandgap_i, 0)
            electrons_per_spin_channel_i = round(sum([sum(i)*w for i, w in zip(occupations_i,weights)]), 3)
        
            electrons_per_spin_channel.append(electrons_per_spin_channel_i)
            bandgap.append(bandgap_i)
            direct_bandgap.append(direct_bandgap_i)
            system_is_metallic.append(system_is_metallic_i)
            fermi_energy.append(fermi_energy_i)
            lumo_eband_bandindex_kpoint.append(lumo_eband_bandindex_kpoint_i)
            homo_eband_bandindex_kpoint.append(homo_eband_bandindex_kpoint_i)
            bandgap_is_direct.append(bandgap_is_direct_i)
            homos.append(homos_i)
            lumos.append(lumos_i)
            

          
            
        def get_overall_direct_bandgap(homos, lumos):
            #print len(homos)
            
            allhomos = numpy.array([np.array([i,j])  for i,j in zip(homos[0], homos[1])])
            alllumos = numpy.array([np.array([i,j])  for i,j in zip(lumos[0], lumos[1])])
            #print len(allhomos), allhomos.shape()
            bandgap_all = max(min(alllumos[:,:,0].flatten()) - max(allhomos[:,:,0].flatten()), 0)
            direct_bandgap_all = max(min([min(alllumos[i,:,0])-max(allhomos[i,:,0]) for i in range(len(alllumos))]), 0)
            
            #this is the kpoint where max occurs for the u and d spin bands
            ud  = [np.argmax(homos[0][:,0]), np.argmax(homos[1][:,0])]
            #this is the overall max 
            select = np.argmax([homos[0][ud[0],0], homos[1][ud[1],0]])
            real_homo_eband_bandindex_kpoint_spin = [ homos[select][ud[select], 0], int(homos[select][ud[select], 1]) , ud[select], select]

            #this is the kpoint where max occurs for the u and d spin bands
            ud  = [np.argmin(lumos[0][:,0]), np.argmin(lumos[1][:,0])]
            select = np.argmin([lumos[0][ud[0],0], lumos[1][ud[1],0]])

            real_lumo_eband_bandindex_kpoint_spin = [ lumos[select][ud[select], 0], int(lumos[select][ud[select], 1]) , ud[select], select]
                                         
            return bandgap_all, direct_bandgap_all,  real_homo_eband_bandindex_kpoint_spin, real_lumo_eband_bandindex_kpoint_spin
                                                            

            
            
            
            
            
        bandgap_all, direct_bandgap_all,  real_homo_eband_bandindex_kpoint_spin, real_lumo_eband_bandindex_kpoint_spin  = get_overall_direct_bandgap(homos, lumos)    

        bandgap.append(bandgap_all)
        direct_bandgap.append(direct_bandgap_all)
        system_is_metallic.append(any(system_is_metallic))
        homo_eband_bandindex_kpoint.append(real_homo_eband_bandindex_kpoint_spin)
        lumo_eband_bandindex_kpoint.append(real_lumo_eband_bandindex_kpoint_spin)
        bandgap_is_direct.append(np.round(bandgap_all,8) == np.round(direct_bandgap_all,8))
        fermi_energy.append(max(fermi_energy))
        
  
        
        dadict =  {'number_electrons': number_electrons,
                   'number_of_spin_channels': 3-number_electrons_per_band,
                   'electrons_per_spin_channel' : electrons_per_spin_channel,
                   'bandgap' : bandgap,
                   'direct_bandgap': direct_bandgap,
                   'system_is_metallic'  : system_is_metallic,
                   'fermi_energy' :  fermi_energy,
                   'lumo_eband_bandindex_kpoint' : lumo_eband_bandindex_kpoint,
                   'homo_eband_bandindex_kpoint': homo_eband_bandindex_kpoint,
                   'bandgap_is_direct': bandgap_is_direct,
                   }        
        
        return dadict
    

            


def get_dos_from_bands_avg_kde(bandsdata, parameterdata, number_electrons=None,
        also_homo_lumo_bandgap=False, fermi_energy = None, no_atoms = None):

    import numpy
    
    import scipy.stats
    from weighted_kde import gaussian_kde
    
    
    try:
        stored_bands = bandsdata.get_bands()
    except KeyError:
        raise KeyError("Cannot do much of a band analysis without bands")

    if len(stored_bands.shape) == 3:
        # I write the algorithm for the generic case of having both the
        # spin up and spin down array

        # put all spins on one band per kpoint
        bands = numpy.concatenate([_ for _ in stored_bands], axis=1)

    else:
        bands = stored_bands

    num_kpoints = len(bands)

    if number_electrons is None:
        # analysis on occupations to get the number of electrons
        try:
            _, stored_occupations = bandsdata.get_bands(also_occupations=True)
        except KeyError:
            raise KeyError("Cannot determine metallicity if I don't have "
                           "either fermi energy, or occupations")

        # put the occupations in the same order of bands, also in case of multiple bands
        if len(stored_occupations.shape) == 3:
            # I write the algorithm for the generic case of having both the
            # spin up and spin down array

            # put all spins on one band per kpoint
            occupations = numpy.concatenate([_ for _ in stored_occupations], axis=1)
        else:
            occupations = stored_occupations

        # now sort the bands by energy
        # Note: I am sort of assuming that I have an electronic ground state

        # sort the bands by energy, and reorder the occupations accordingly
        # since after joining the two spins, I might have unsorted stuff
        bands, occupations = [numpy.array(y) for y in zip(*[zip(*j) for j in
                                                            [sorted(zip(i[0].tolist(), i[1].tolist()),
                                                                    key=lambda x: x[0])
                                                             for i in zip(bands, occupations)]])]
        number_electrons = int(round(sum([sum(i) for i in occupations]) / num_kpoints))

    else:
        bands = numpy.sort(bands)
        number_electrons = int(number_electrons)

    # find the zero-temperature occupation per band (1 for spin-polarized
    # calculation, 2 otherwise)
    if fermi_energy == None:
        bandsl = bands.flatten()
        occupationsl = occupations.flatten()
        for b,o in zip(list(reversed(bandsl)), list(reversed(occupationsl))):
            if o>0.5:
                fermi_energy = b
                break
    if fermi_energy == None:
        #This si sthe calculated fermi energy rather not!!
        #Choose the highest level that is more than 0.5 occupied
        if parameterdata == None:
            fermi_energy = bandsdata.inp.output_band.get_outputs_dict()['output_parameters'].get_dict()['fermi_energy']
        else:
            fermi_energy = parameterdata.get_dict()['fermi_energy']
    



    if no_atoms == None:
        if parameterdata == None:
            no_atoms = len(bandsdata.inp.output_band.inp.structure.sites)
        else:
	    no_atoms = len(parameterdata.inp.output_parameters.inp.structure.sites)
        
    weights =  np.array([bandsdata.get_array('weights') for i in range(bands.shape[1])]).T
    
    
    number_electrons_per_band = 4 - len(stored_bands.shape)  # 1 or 2
    # gather the energies of the homo band, for every kpoint
    homo = numpy.array([i[number_electrons / number_electrons_per_band - 1] for i in bands])  # take the nth level
    try:
        # gather the energies of the lumo band, for every kpoint
        lumo = numpy.array([i[number_electrons / number_electrons_per_band] for i in bands])  # take the n+1th level
    except IndexError:
        raise ValueError("To compute the optical band gap, "
                         "need more bands than n_band=number_electrons")

    singleweights  = bandsdata.get_array('weights') 
    allbandsweights = weights.flatten()
    
    allbands = bands.flatten()


    datawindow = [min(allbands), max(allbands)]
    bins = np.arange(np.floor(datawindow[0]-0.2),np.ceil(datawindow[1]+0.2), 0.1 )

    #plt.hist(allbands, bins = bins, weights = allbandsweights/len(allbands)*0.1, normed = False)
    #plt.hist(homo, weights=singleweights/len(homo)*0.1, bins = bins, normed = False)
    
    
    #basically kde smears our by h and adds up gaussians
    # take h as related to the average distance in energy within a range of 1 eV below the fermilevel?
    # but problem with localized states: there fore make a statistical analysis of the clusters
    
    
    
    
    opth = optimize_bandwidth(allbands, fermi_energy, window = 15, method = 'standard')
    kde = gaussian_kde(allbands, bw_method = opth, weights = allbandsweights)
    def get_kde(x):
        return kde.evaluate(x)
    
    from scipy.integrate import quad
    normalization = float(number_electrons)/quad(get_kde, np.floor(datawindow[0]-5), fermi_energy)[0]
    
    testx = np.linspace(-20, 5, 501)
    #plt.plot(testx, kde.evaluate(testx)*normalization)
    

    return number_electrons, fermi_energy, datawindow, opth, no_atoms,normalization, lambda x: get_kde(x)*normalization             







def get_consistent_DosData(bandsdata, number_electrons=None,
        also_homo_lumo_bandgap=False, fermi_energy = None):
    """
    This function is inspired by the implementation of the Parser class for Dos.
    """
    _dos_name = 'output_dos'
    _units_name = 'output_units'

    
    array_names = [[], []]
    array_units = [[], []]
    array_names[0] = ['dos_energy', 'dos',
                      'integrated_dos']  # When spin is not displayed
    array_names[1] = ['dos_energy', 'dos_spin_up', 'dos_spin_down',
                      'integrated_dos']  # When spin is displayed
    array_units[0] = ['eV', 'states/eV',
                      'states']  # When spin is not displayed
    array_units[1] = ['eV', 'states/eV', 'states/eV',
                      'states']  # When spin is displayed

    # grabs parsed data from aiida.dos
    array_data, spin = parse_raw_dos(dos_file, array_names, array_units)
    
    energy_units = 'eV'
    dos_units = 'states/eV'
    int_dos_units = 'states'        
    xy_data = XyData()
    xy_data.set_x(array_data["dos_energy"],"dos_energy", energy_units)
    y_arrays = []
    y_names = []
    y_units = []
    y_arrays  += [array_data["integrated_dos"]]
    y_names += ["integrated_dos"]
    y_units += ["states"]
    if spin:
        y_arrays  += [array_data["dos_spin_up"]]
        y_arrays  += [array_data["dos_spin_down"]]
        y_names += ["dos_spin_up"]
        y_names += ["dos_spin_down"]
        y_units += ["states/eV"]*2
    else:
        y_arrays  += [array_data["dos"]]
        y_names += ["dos"]
        y_units += ["states/eV"]
    xy_data.set_y(y_arrays,y_names,y_units)

    # grabs the parsed data from aiida.out
    parsed_data = parse_raw_out_basic(out_file, "DOS")
    output_params = ParameterData(dict=parsed_data)
    # Adds warnings
    for message in parsed_data['warnings']:
        self.logger.error(message)
    # Create New Nodes List
    new_nodes_list = [(self.get_linkname_outparams(), output_params),
                      (self.get_linkname_dos(), xy_data)]
    return successful,new_nodes_list




########################################################

#These are tools for DOS from reduced bandstructure calculations (iired zone)
#
#
#
#First
#
#
#
#
import spglib
def get_mapping_extended_irred_zone(structure, bandsdata, kpoints, kpointsmesh=None):
    
    usedkpoints = np.round(kpoints.get_array('kpoints'),6)%1 
    ases = structure.get_ase()
    offset = [0.,0.,0.]
    
    if kpointsmesh == None:
        while 'mesh' not in kpoints.get_attrs():
            kpoints = kpoints.inp.output_kpoints.inp.kpoints
        
        kpointsmesh = kpoints.get_attrs()['mesh']
        offset = kpoints.get_attrs()['offset']
        #here we would maybe also know the right structure
    
    mapping, grid = spglib.get_ir_reciprocal_mesh(kpointsmesh, ases, is_shift=offset)
    print("Number of ir-kpoints: %d" % len(np.unique(mapping))) 
    print len(grid)
    print(len(usedkpoints))
    #print np.ravel(np.reshape(usedkpoints, kpointsmesh, order='F'), order='F')
    
    if len(np.unique(mapping)) == len(usedkpoints):
        
        spg_kpoints = (grid.astype(float)/kpointsmesh)%1
        
        
        
        distlist = [ [np.linalg.norm(d-kp) for d in spg_kpoints] for kp in usedkpoints] 
        if not all([min(di) < 0.001 for di in  distlist ]):
            #print  spg_kpoints
            #print usedkpoints
            print [min(di) for di in  distlist ]
            
            raise Exception
        else:
            index_orig_to_spg = [np.argmin(di) for di in  distlist ]
                                 
            index_irredspg_to_orig = [mapping[ki] for ki in index_orig_to_spg]

            index_orig_tospg = [index_irredspg_to_orig.index(ki) for ki in mapping]

                           
            return index_orig_tospg, spg_kpoints, kpointsmesh

        
    else:
        raise Exception





def multiply_kpointgrid(index_orig_tospg, spg_kpoints, scale = None):
    #supdate scale
    import itertools
    scale = [-1,0,1]
    scales=[]
    for i in itertools.product([-1, 0, 1], repeat=3):
        if np.linalg.norm(i) != 0:
            
            scales.append(i)
    #scales = sorted(scales, key=lambda x: np.linalg.norm(x))
    longarray = spg_kpoints.copy()
    for mult in scales:
        longarray = np.concatenate( (longarray, spg_kpoints + np.array(mult)))
    return longarray, index_orig_tospg*(len(scales)+1)
    

'''
def make_supercellgrid(index_orig_tospg, spg_kpoints, kpointsmeshin, bandsdata ,scale = 2):
    #scale 2 is not safe because it might be not sufficient for the final voronoi cell
    # use a scale of 3 and then we take the middle matrix that should be safe 
    # anysaysm only the indices count or better we will have data bwtween 0 and 1 probably, that depends
    # maybe not acturally. Let 2
    # probably one should make voronoi decomostion just as in neighbors of materials project
    # then we know which neighbours to include.
    #reshaped_kpoints = spg_kpoints.reshape(list(reversed(kpointsmeshin)) + [3])
    reshaped_index = np.array(index_orig_tospg).reshape(kpointsmeshin)
    kpointsmesh = np.array(kpointsmeshin)
    kpointsmeshr = np.array(list(reversed(kpointsmeshin)))
    reshaped_index = np.array(index_orig_tospg).reshape(list(reversed(kpointsmeshin)))
    multiplied_kpoints = scale*kpointsmesh
    multiplied_kpointsr = scale*kpointsmeshr
    bands = bandsdata.get_array('bands')
    if len(bands.shape) == 3:
        bandi = bands[0]
    else:
        bandi=bands
    bandnum = len(bandi[0])
    print bandi.shape
    biga = np.zeros(multiplied_kpointsr+[bandnum])
    
    
    #bigk = np.zeros(list(multiplied_kpoints)+[3])
    d = (np.mgrid[0:multiplied_kpointsr[0],0:multiplied_kpointsr[1],0:multiplied_kpointsr[2] ]).astype(float)
    bigk = d.T/np.array(kpointsmesh) - np.array([1.,1.,1.])
    #this meshgrid has the sameorder and is just inside the positive direction of the BZ
    #d = (np.mgrid[0:kpointsmesh[0],0:kpointsmesh[1],0:kpointsmesh[2] ]).astype(float)
    #dt = d.T/np.array(kpointsmesh)
        
    for i in range(multiplied_kpointsr[0]):
        for j in range(multiplied_kpointsr[1]):
            for k in range(multiplied_kpointsr[2]):
                ijk = np.array([i,j,k])
                origindex = reshaped_index.item(tuple(ijk%kpointsmesh))
                #mult = (ijk/kpointsmesh).astype(int)
                print origindex
                biga[i,j,k] = bandi[origindex]

                #bigk[i,j,k] = reshaped_kpoints[] -np.array([1.,1.,1]) + np.divmod(x, y)
    


    return bigk, biga
'''
    
def make_supercellgrid(index_orig_tospg, spg_kpoints, kpointsmeshin, bandsdata ,scale = 2):
    #scale 2 is not safe because it might be not sufficient for the final voronoi cell
    # use a scale of 3 and then we take the middle matrix that should be safe 
    # anysaysm only the indices count or better we will have data bwtween 0 and 1 probably, that depends
    # maybe not acturally. Let 2
    # probably one should make voronoi decomostion just as in neighbors of materials project
    # then we know which neighbours to include.
    #reshaped_kpoints = spg_kpoints.reshape(list(reversed(kpointsmeshin)) + [3])
    reshaped_index = np.array(index_orig_tospg).reshape(kpointsmeshin, order= 'F')
    kpointsmesh = np.array(kpointsmeshin)
    
    multiplied_kpoints = scale*kpointsmesh
   
    bands = bandsdata.get_array('bands')
    if len(bands.shape) == 3:
        bandi = bands[0]
    else:
        bandi=bands
    bandnum = len(bandi[0])
    print bandi.shape
    biga = np.zeros(list(multiplied_kpoints)+[bandnum])
    print biga.shape
    
    #bigk = np.zeros(list(multiplied_kpoints)+[3])
    #d = (np.mgrid[0:multiplied_kpoints[2],0:multiplied_kpoints[1],0:multiplied_kpoints[0] ]).astype(float)
    #bigk = d.T/np.array(kpointsmesh) - np.array([1.,1.,1.])
    
    
    #I think this is better?!
    bigk = np.array(np.meshgrid(np.linspace(0, scale, scale*kpointsmesh[0], endpoint=False) -1. , 
                         np.linspace(0, scale, scale*kpointsmesh[1], endpoint=False) -1. ,
                        np.linspace(0, scale, scale*kpointsmesh[2], endpoint=False) -1.)).T
    
    #this meshgrid has the sameorder and is just inside the positive direction of the BZ
    #d = (np.mgrid[0:kpointsmesh[0],0:kpointsmesh[1],0:kpointsmesh[2] ]).astype(float)
    #dt = d.T/np.array(kpointsmesh)
    print bigk.shape
    for i in range(multiplied_kpoints[0]):
        for j in range(multiplied_kpoints[1]):
            for k in range(multiplied_kpoints[2]):
                ijk = np.array([i,j,k])
                origindex = reshaped_index.item(tuple(ijk%kpointsmesh))
                #mult = (ijk/kpointsmesh).astype(int)
                print origindex
                biga[i,j,k,:] = bandi[origindex][:]

                #bigk[i,j,k] = reshaped_kpoints[] -np.array([1.,1.,1]) + np.divmod(x, y)
    


    return bigk, biga

    
from scipy.interpolate import RegularGridInterpolator
from eqtools.trispline import Spline
    
    
class interpolated_multi_D():
    def __init__(self,grid1, grid2, grid3, data):    
        self.interpolationfunction = []
        ba = data #np.swapaxes(data, 0, 2)
        self.interpolationfunction = []
        
        for i in range(data.shape[-1]):
            print ba[:,:,:,i].shape
            interpolating_function_tricubic = Spline(grid1,grid2,grid3, ba[:,:,:,i])
            self.interpolationfunction.append(interpolating_function_tricubic)
        
    def evaluate(self,xt, yt, zt, dim='all'):
        
        if dim == 'all':
            
            return np.array([self.interpolationfunction[k].ev(xt, yt, zt) for k in range(len(self.interpolationfunction))])
        else:
            return self.interpolationfunction[dim].ev(xt, yt, zt)
        
    

def get_interpolated_function(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, fermi_level=0):
    from scipy.interpolate import RegularGridInterpolator
    from eqtools.trispline import Spline
    #the order of the gridpoints in spglib for the uniform grids is as by C standard
    # the data can be reshaped by resh = spg_kpoints.reshape(kpointsmesh + [3])
    #resh[0][0] is then the first few elements [ 0  0  0]
         #[ 1  0  0]
         #[ 2  0  0]
         #[-1  0  0]
    #but the whole grid is shifted to 0. that is why the data is potentially to be aligned in real space rather +0.5. For periodic data that makes no difference
    #but for the interpolation it could make a problem. potentially one has to increase the size for good interpolation
    
    
    
    bigk, biga = make_supercellgrid(index_orig_tospg, spg_kpoints, kpointsmesh ,bandsdata, scale = scale)
    tb = bigk.T
    #xgrid = tb[0]
    grid  = []
    for i in range(3):
        grid.append(np.linspace(0, scale, scale*kpointsmesh[i], endpoint=False) -1.)
    
    
    print [grid[i].shape for i in range(3)]
    print biga.shape
        
    interpolating_function = interpolated_multi_D(grid[0],grid[1],grid[2], biga)
    
    
    return interpolating_function
    
    
    
    
    
    
            

def plot_interpolated_bandsdata(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, fermi_level=0):

    interpolating_function = get_interpolated_function(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = scale, fermi_level=fermi_level)

    #longarray, larger = multiply_kpointgrid(index_orig_tospg, spg_kpoints, scale = scale)
    
    #m is the grid to evaluate the function on 
    m=10
    xgrid_d = np.linspace(-0.5, 0.5, m)
    ygrid_d = np.linspace(-0.5, 0.5, m)
    zgrid_d = np.linspace(-0.5, 0.5, m)
    
    from mpl_toolkits.mplot3d import Axes3D
    import pylab as plt
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    
    xt, yt, zt = np.meshgrid(np.linspace(-0.5, 0.5, m) , 
                         np.linspace(-0.5, 0.5, m), 
                        np.linspace(-0.5, 0.5, m))
    
    
    import matplotlib.cm as cm
    
    
    
    values =  interpolating_function.evaluate(xt.flatten(), yt.flatten(), zt.flatten(), dim = 1)
    

    ax.scatter(xt.flatten(), yt.flatten(), zt.flatten(), values-fermi_level, c=cm.jet(values) , s = abs(values))



    plt.show()

            
            
            
def plot_interpolated_bandsdata2d(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, fermi_level=0):
    interpolating_function = get_interpolated_function(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = scale, fermi_level=fermi_level)
    

    #longarray, larger = multiply_kpointgrid(index_orig_tospg, spg_kpoints, scale = scale)
    
    #m is the grid to evaluate the function on 
    m=100
    #xgrid_d = np.linspace(-0.5, 0.5, m)
    #ygrid_d = np.linspace(-0.5, 0.5, m)
    #zgrid_d = np.linspace(-0.5, 0.5, m)
    
    from mpl_toolkits.mplot3d import Axes3D
    import pylab as plt
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    
    #xt, yt, zt = np.meshgrid(np.linspace(-0.5, 0.5, m) , 
    #                     np.linspace(-0.5, 0.5, m), 
    #                    np.linspace(-0.5, 0.5, m))
    
    
    import matplotlib.cm as cm
    xt,yt  = np.zeros(m), np.zeros(m)
    zt  = np.linspace(-0.5, 0.5, m)
    #xt, yt, zt = np.array([i*np.array([1.,1.,1.]) for i in np.linspace(-0.5, 0.5, 20)]).T
    
    values =  interpolating_function.evaluate(xt, yt, zt, dim ='all')
    print values.shape
    for i in range(values.shape[0]):
        ax.plot(zt, values[i,:]-fermi_level, c='k')

    ax.set_ylim(-10,7)

    plt.show()            
            
            
            
            
            
            




    '''
    optical_band_gap = numpy.min(lumo-homo)
    #there is a problem if that is a metal clearly
    if not also_homo_lumo_bandgap:
        return optical_band_gap
    else:
        top_valence = numpy.max(homo)
        bottom_conduction = numpy.min(lumo)
        band_gap = None if (number_electrons%2==1 and len(stored_bands.shape)==2) else bottom_conduction-top_valence
        return optical_band_gap,top_valence,bottom_conduction,\
            band_gap if band_gap>=0 else None



    '''

def get_derivative_num(interpolating_function, mesh=(100,100,100), function_name = 'Spline', dim = 0, order = 1):   
    #for the derivatives we use central differences, this is done by image filtering
    #first derivative is done via 1d convolutions
    #second derivative is done by 2d convolution
    
    
    import scipy.ndimage.filters as filters
    
    xfilter = np.array([-0.5,0,0.5])
    xxfilter = np.array([[[-0.5]],[[0]], [[0.5]]])
    
    xx2filter = np.array([[[1.]],[[-2.]], [[1.]]])
    yy2filter = np.array([[[1.], [-2.], [1.]]])
    zz2filter = np.array([[[1., -2., 1.]]])
    #xy2filter = 0.25 * [[[0,0,0], [0,0,0], [0,0,0]],\
    #                    [[0,0,0], [0,0,0], [0,0,0]],
    #                    [[0,0,0], [0,0,0], [0,1,0]]]
    xy2filter = 0.25 * np.array([[[1], [0], [-1]],                                                                                                           
                        [[0], [0], [0]],                                                                                                            
                        [[-1], [0], [1]]])
    xz2filter = 0.25 * np.array([[[1,0,-1]], [[0,0,0]], [[-1,0,1]]])


    yz2filter = 0.25 * np.array([[[1,0,-1],[0,0,0], [-1,0,1]]]) 


     
    Hessianf =        [ [xx2filter,xy2filter, xz2filter],
                         [xy2filter,yy2filter, yz2filter], 
                         [xz2filter,yz2filter, zz2filter] ]
                         



    
    #                                     j-1               j          j+1
    #...:     xy2filter = 0.25 * [[[k-1 ,k,k+1], [0,j-1,0], [0,0,0]],\                                                                                                           
    #...:                         [[0,0,0], [0,0,0], [0,0,0]],  i                                                                                                          
    #...:                         [[0,0,0], [0,0,0], [0,0,1]]]  i+1
    
    #formulas
    #u_xy(x_i, y_j) ≈ 1/4hk (u(i+1,j+1) − u(i+1,j−1) − u(i−1,j+1) + u(i−1,j−1)  
    



    if function_name == 'Spline':
        v = []
        



        
        xt, yt, zt = np.meshgrid(np.linspace(-0.5, 0.5, mesh[0]) , 
                         np.linspace(-0.5, 0.5, mesh[0]), 
                        np.linspace(-0.5, 0.5, mesh[0]))
    
        if dim != all:
            outarray =  interpolating_function.evaluate(xt.flatten(), yt.flatten(), zt.flatten(), dim = dim).reshape(list(mesh))   
        else:
            outarrayo =  interpolating_function.evaluate(xt.flatten(), yt.flatten(), zt.flatten(), dim = dim)
            outarray = np.array([i.reshape(list(mesh)) for i in outarrayo])
            
        print 'interpolation result', len(outarray), outarray.shape
        
        if dim == 'all':
            r = []
            for i in range(outarray.shape()[-1]):
                r.append(np.gradient(outarray[:,:,:,i]))
            return xt, yt, zt, np.array(r)
            
        else:
            return xt, yt, zt, np.gradient(outarray)
            

def get_derivative_details(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, dim = 200):

    interpolating_function = get_interpolated_function(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = scale, fermi_level=fermi_level)

    x,y,z,me = get_derivative_num(interpolating_function, mesh=(20,20,20), function_name = 'Spline', dim = dim)
    return x,y,z, me
    

    
    
    #m1, m2, m3, derivative =   get_derivative_num(mesh=(100,100,100), interp, function_name = 'Spline', dim = 0) 



    
def pre_analyse_bands_data(calc):
    bandsdata = calc.out.output_parameters.out.pw_output_parameters.out.output_band  
    structure = calc.out.output_structure
    kpoints = calc.out.output_kpoints 
    efermi = calc.res.fermi_energy
    
    
    
    


def plot_derivative_ds(calc, dim =100):                      
    bandsdata = calc.out.output_parameters.out.pw_output_parameters.out.output_band  
    structure = calc.out.output_structure
    kpoints = calc.out.output_kpoints 
    efermi = calc.res.fermi_energy
    index_orig_tospg, spg_kpoints, kpointsmesh = get_mapping_extended_irred_zone(structure, bandsdata, kpoints, kpointsmesh=None) 
    xt,yt,zt, values = get_derivative_details(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, 2, dim=dim)
    
    from mpl_toolkits.mplot3d import Axes3D
    import pylab as plt
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    
    import matplotlib.cm as cm
    
    #print len(values), len(values[0])
    #print [np.linalg.norm([i,j,k]) for i,j,k in zip(values[0,:,:,:].flatten(), values[1,:,:,:].flatten(), values[2,:,:,:].flatten())]

    ax.scatter(xt.flatten(), yt.flatten(), zt.flatten(), alpha = 0.3, c=cm.jet([np.linalg.norm([i,j,k])*10 for i,j,k in zip(values[0,:,:,:].flatten(), values[1,:,:,:].flatten(), values[2,:,:,:].flatten())] ))
    
    
def plot_interpolated_surface(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, fermi_level=0, contours = -5):
    from mayavi import mlab
    interpolating_function = get_interpolated_function(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = scale, fermi_level=fermi_level)
    xt, yt, zt, data = get_interpolated_surface(interpolating_function, mesh=(100,100,100), function_name = 'Spline', dim = 10, fermi_level=contours-0.1)
    
    mlab.figure(1, size=(600,800), bgcolor=(1, 1, 1))
    '''
    A = [[1,0,0], [0,1,0], [0,0,1]]
    for i1, a in enumerate(A):
        i2 = (i1 + 1) % 3
        i3 = (i1 + 2) % 3
        for b in [np.zeros(3), A[i2]]:
            for c in [np.zeros(3), A[i3]]:
                p1 = b + c
                p2 = p1 + a
                mlab.plot3d([p1[0], p2[0]],
                            [p1[1], p2[1]],
                            [p1[2], p2[2]],
                            tube_radius=0.001)
    
    '''
    cp = mlab.contour3d(data, contours=[0.1], transparent=True,
                   opacity=0.3, color=(0.4, 0.8, 0.01))
    #cf = mlab.gcf()
    mlab.show()
     
     
     
def get_interpolated_surface(interpolating_function, mesh=(100,100,100), function_name = 'Spline', dim = 0, fermi_level=0): 
    xt, yt, zt = np.meshgrid(np.linspace(-0.5, 0.5, mesh[0]) , 
               np.linspace(-0.5, 0.5, mesh[1]), np.linspace(-0.5, 0.5, mesh[2]))
    
    if dim != all:
        outarray =  interpolating_function.evaluate(xt.flatten(), yt.flatten(), zt.flatten(), dim = dim).reshape(list(mesh))   - fermi_level
    else:
        outarrayo =  interpolating_function.evaluate(xt.flatten(), yt.flatten(), zt.flatten(), dim = dim) - fermi_level
        outarray = np.array([i.reshape(list(mesh)) for i in outarrayo])
        
    print 'interpolation result', len(outarray), outarray.shape
    
    return xt, yt, zt, outarray

    

    

def plot_bands(calc, plot_type = 'a = 2d'):                      
    bandsdata = calc.out.output_parameters.out.pw_output_parameters.out.output_band  
    structure = calc.out.output_structure
    kpoints = calc.out.output_kpoints 
    efermi = calc.res.fermi_energy
    index_orig_tospg, spg_kpoints, kpointsmesh = get_mapping_extended_irred_zone(structure, bandsdata, kpoints, kpointsmesh=None) 

    
    if plot_type == '2d':
        plot_interpolated_bandsdata2d(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, 2, efermi)
    elif plot_type == 'fermi_surface':
        plot_interpolated_surface(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, fermi_level=efermi, contours =4)






def return_data_bands(calc, plot_type = 'a = 2d'):                      
    bandsdata = calc.out.output_parameters.out.pw_output_parameters.out.output_band  
    structure = calc.out.output_structure
    kpoints = calc.out.output_kpoints 
    efermi = calc.res.fermi_energy
    index_orig_tospg, spg_kpoints, kpointsmesh = get_mapping_extended_irred_zone(structure, bandsdata, kpoints, kpointsmesh=None) 

    
    if plot_type == '2d':
        plot_interpolated_bandsdata2d(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, 2, efermi)
    elif plot_type == 'fermi_surface':
        interpolating_function = get_interpolated_function(bandsdata, index_orig_tospg, spg_kpoints, kpointsmesh, scale = 2, fermi_level=efermi)
        xt, yt, zt, data = get_interpolated_surface(interpolating_function, mesh=(100,100,100), function_name = 'Spline', dim = 36, fermi_level=4.-0.1)
        return xt, yt, zt, data














def get_bandgap_results(parameters=None,bands=None,store=True,print_progress=True):
    """
    Get the results from the get_bandgap_inline function:
    - if there exists already an inline calculation with the same input bands, 
    it does not relaunch it, it gets instead 
    the output dictionary of the previously launched function,
    - otherwise, launches the get_bandgap_inline function
    and gets its result.
    """
    from aiida.orm.calculation.inline import InlineCalculation
    from old_utils import objects_are_equal
    params_dict = parameters.get_dict()
    result_dict = None
    for ic in bands.get_outputs(InlineCalculation):
        try:
            if ( ic.get_function_name() == 'get_bandgap_inline'
                 and objects_are_equal(ic.inp.parameters.get_dict(), params_dict)
                 and 'output_parameters' in ic.get_outputs_dict() ):
                result_dict = ic.get_outputs_dict()
        except AttributeError:
            pass
    
    if result_dict is not None:
        if print_progress:
            print " get_bandgap_inline already run for bands with pk {} -> " \
              "we do not re-run".format(bands.pk)
    else:
        if print_progress:
            print "Launch get_bandgap_inline for bands with pk {} ... " \
              "".format(bands.pk)
        result_dict = get_bandgap_inline(bands=bands,
                                         parameters=parameters,
                                         store=store)
    return result_dict


def get_wfs_with_parameter(parameter, wf_class='Workflow'):
    """
    Find workflows of a given class, with a given parameter (which must be a
    node)
    :param parameter: an AiiDA node
    :param wf_class: the name of the workflow class
    :return: an AiiDA query set with all workflows that have this parameter
    """
    from aiida.common.datastructures import wf_data_types
    from aiida.orm.workflow import Workflow
    try:
        from aiida.backends.djsite.db import models
    except ImportError:
        from aiida.djsite.db import models
    # Find attributes with this name
    qdata = models.DbWorkflowData.objects.filter(aiida_obj=parameter,
        data_type=wf_data_types.PARAMETER)
    # Find workflows with those attributes
    if wf_class == 'Workflow':
        qwf = Workflow.query(data__in=qdata)
    else:
        qwf = Workflow.query(module_class=wf_class,data__in=qdata)
    #q2 = wf_class.query(data__in=q1)
    # return a Django QuerySet with the resulting class instances
    return qwf.distinct().order_by('ctime')

def take_out_keys_from_dictionary(dictionary,keys):
    """
    Take out some keys from a dictionary.
    :param dictionary: a dictionary
    :param keys:a list of keys to be poped out. A '|' in the key means 
        descending into sub-dictionaries.
    Acts on dictionary itself.
    """
    for key in keys:
        if '|' not in key:
            dictionary.pop(key,None)
        else:
            d = dictionary
            while '|' in key:
                try:
                    d = d[key.split('|')[0]]
                except KeyError:
                    break
                key = "|".join(key.split('|')[1:])
            d.pop(key,None)

def replace_all_parameterdata_with_dict(dictionary):
    """
    Replace all ParameterData objects in the top level of the dictionary,
    by their corresponding dictionary
    :param dictionary: a dictionary (modifies it 'in-place')
    """
    for key,value in dictionary.iteritems():
        if isinstance(value,ParameterData):
            dictionary[key] = value.get_dict()

def get_pw_wfs_with_parameters(wf_params,also_bands=False,
                ignored_keys=['codename','group_name','band_group_name',
                'calculation_set','settings','band_calculation_set','band_settings',
                'input|automatic_parallelization','input|clean_workdir','input|max_restarts',
                'input|final_scf_remove_use_all_frac',
                'parameters|SYSTEM|use_all_frac',
                'parameters|ELECTRONS|electron_maxstep','parameters|ELECTRONS|mixing_beta',
                'parameters|ELECTRONS|mixing_mode','parameters|ELECTRONS|mixing_ndim',
                'parameters|ELECTRONS|diagonalization',
                'band_input|automatic_parallelization','band_input|clean_workdir',
                'band_parameters_update|ELECTRONS|diagonalization']):
    """
    Find all PwWorkflow already run with the same parameters.
    :param wf_params: a dictionary with all the parameters (can contain
        dictionaries, structure and kpoints)
    :param also_bands: if True, check that a band structure is also in
        the workflow results
    :param ignored_keys: list of keys of wf_params that are ignored in the 
        comparison (a '|' means descending into a sub-dictionary)
    :return: the list of workflows.
    """
    from copy import deepcopy
    from old_utils import objects_are_equal
    the_params = deepcopy(wf_params)
    replace_all_parameterdata_with_dict(the_params)
    take_out_keys_from_dictionary(the_params,ignored_keys)
    structure_ref = the_params.pop('structure')
    kpoints_ref = the_params.pop('kpoints',None)
    band_kpoints_ref = the_params.pop('band_kpoints',None)
    input_pw_calc_ref = the_params.pop('pw_calculation',0)
    if kpoints_ref:
        try:
            kpoints_ref = kpoints_ref.get_kpoints_mesh()
        except AttributeError:
            kpoints_ref = kpoints_ref.get_kpoints()
    if band_kpoints_ref:
        try:
            band_kpoints_ref = band_kpoints_ref.get_kpoints_mesh()
        except AttributeError:
            band_kpoints_ref = band_kpoints_ref.get_kpoints()
    if input_pw_calc_ref:
        input_pw_calc_ref = input_pw_calc_ref.pk
    
    wfs_pw = get_wfs_with_parameter(structure_ref,'PwWorkflow')
    wfs = []
    for wf_pw in wfs_pw:
        if (('pw_calculation' in wf_pw.get_results() or 'pw_calculation' in wf_pw.get_parameters())
            and (not also_bands or 'band_structure' in wf_pw.get_results())):
            params = deepcopy(wf_pw.get_parameters())
            replace_all_parameterdata_with_dict(params)
            take_out_keys_from_dictionary(params,ignored_keys+['structure'])
            kpoints = params.pop('kpoints',None)
            band_kpoints = params.pop('band_kpoints',None)
            input_pw_calc = params.pop('pw_calculation',0)                
            if kpoints:
                try:
                    kpoints = kpoints.get_kpoints_mesh()
                except AttributeError:
                    kpoints = kpoints.get_kpoints()
            if band_kpoints:
                try:
                    band_kpoints = band_kpoints.get_kpoints_mesh()
                except AttributeError:
                    band_kpoints = band_kpoints.get_kpoints()
            if input_pw_calc:
                input_pw_calc = input_pw_calc.pk
            
            if (objects_are_equal(kpoints,kpoints_ref)
                and objects_are_equal(band_kpoints,band_kpoints_ref)
                and objects_are_equal(the_params,params)
                and input_pw_calc_ref==input_pw_calc):
                wfs.append(wf_pw)
    
    return wfs

def get_phrestart_wfs_with_parameters(wf_params,
                ignored_keys=['codename','calculation_set','settings',
                'input|max_restarts','input|clean_workdir',
                'parameters|INPUTPH|alpha_mix(1)','parameters|INPUTPH|alpha_mix',
                'parameters|INPUTPH|niter_ph',
                'parameters|INPUTPH|nmix_ph','parameters|INPUTPH|fildrho',
                'parameters|INPUTPH|fildvscf']):
    """
    Find all PhrestartWorkflow already run with the same parameters.
    :param wf_params: a dictionary with all the parameters (can contain
        dictionaries, structure and kpoints)
    :param ignored_keys: list of keys of wf_params that are ignored in the 
        comparison (a '|' means descending into a sub-dictionary)
    :return: the list of workflows.
    """
    from copy import deepcopy
    from old_utils import objects_are_equal
    from old_utils import get_farthest_node
    PhCalculation = CalculationFactory('quantumespresso.ph')
    the_params = deepcopy(wf_params)
    replace_all_parameterdata_with_dict(the_params)
    take_out_keys_from_dictionary(the_params,ignored_keys)
    qpoints_ref = the_params.pop('qpoints',None)
    input_pw_calc_ref = the_params.pop('pw_calculation')
    if qpoints_ref:
        try:
            qpoints_ref = qpoints_ref.get_kpoints_mesh()
        except AttributeError:
            qpoints_ref = qpoints_ref.get_kpoints()
    
    wfs_ph = get_wfs_with_parameter(input_pw_calc_ref,'PhrestartWorkflow')
    wfs = []
    for wf_ph in wfs_ph:
        if ('ph_folder' in wf_ph.get_results() or 'last_clean_calc' in wf_ph.get_attributes()):
            params = deepcopy(wf_ph.get_parameters())
            replace_all_parameterdata_with_dict(params)
            take_out_keys_from_dictionary(params,ignored_keys+['pw_calculation'])
            qpoints = params.pop('qpoints',None)
            input_ph_calc = params.pop('ph_calculation',None)                
            if qpoints:
                try:
                    qpoints = qpoints.get_kpoints_mesh()
                except AttributeError:
                    qpoints = qpoints.get_kpoints()
            if input_ph_calc:
                qph = get_farthest_node(input_ph_calc,PhCalculation)
            
            if (objects_are_equal(qpoints,qpoints_ref)
                and objects_are_equal(the_params,params)
                and (input_ph_calc is None or 
                     (qph.count()==1 and qph[0].inp.parent_calc_folder.inp.remote_folder.pk==input_pw_calc_ref.pk))):
                wfs.append(wf_ph)
    
    return wfs

def get_phonondispersion_wfs_with_parameters(wf_params,also_dispersion=False, also_pw_restart=False,
                ignored_keys=['pw_codename', 'pw_calculation_set','pw_settings',
                'pw_input|automatic_parallelization','pw_input|clean_workdir','pw_input|max_restarts',
                'pw_input|final_scf_remove_use_all_frac','pw_input|finish_with_scf',
                'pw_parameters|SYSTEM|use_all_frac',
                'pw_parameters|ELECTRONS|electron_maxstep','pw_parameters|ELECTRONS|mixing_beta',
                'pw_parameters|ELECTRONS|mixing_mode','pw_parameters|ELECTRONS|diagonalization',
                'pw_parameters|ELECTRONS|conv_thr', # conv_thr is added by me
                'ph_codename', 'ph_calculation_set','ph_settings', 'ph_input|use_qgrid_parallelization',
                'dispersion_matdyn_codename', 'dispersion_q2r_codename', 'dispersion_calculation_set',
                'dispersion_settings', 'dispersion_group_name']):
    """
    Find all PhonondispersionWorkflow already run with the same parameters or, if not found any, 
    find all PwWorkflow for the 'run_pw' step already run with the same parameters.
    :param wf_params: a dictionary with all the parameters (can contain
        dictionaries, structure and kpoints)
    :param also_dispersion: if True, check that a phonon dispersion is also in
        the workflow results
    :param ignored_keys: list of keys of wf_params that are ignored in the 
        comparison (a '|' means descending into a sub-dictionary)
    :return: Dictionary of the form:
        {'Phonondispersion': list of PhonondispersionWorkflow}, if previous Phonondispersion workflows were found
        {'Pw': list of PwWorkflow}, if previous Pw workflows were found
        {}, if no previous workflows were found
    """
    from copy import deepcopy
    from old_utils import objects_are_equal
    the_params = deepcopy(wf_params)
    replace_all_parameterdata_with_dict(the_params)
    take_out_keys_from_dictionary(the_params,ignored_keys)
    structure_ref = the_params.pop('structure')
    kpoints_ref = the_params.pop('pw_kpoints',None)
    qpoints_ref = the_params.pop('ph_qpoints',None)
    input_pw_calc_ref = the_params.pop('pw_calculation',0)
    input_ph_calc_ref = the_params.pop('ph_calculation',0)
    input_ph_folder_ref = the_params.pop('ph_folder',0)
    if kpoints_ref:
        try:
            kpoints_ref = kpoints_ref.get_kpoints_mesh()
        except AttributeError:
            kpoints_ref = kpoints_ref.get_kpoints()
    if qpoints_ref:
        try:
            qpoints_ref = qpoints_ref.get_kpoints_mesh()
        except AttributeError:
            qpoints_ref = qpoints_ref.get_kpoints()
    if input_pw_calc_ref:
        input_pw_calc_ref = input_pw_calc_ref.pk
    if input_ph_calc_ref:
        input_ph_calc_ref = input_ph_calc_ref.pk  
    if input_ph_folder_ref:
        input_ph_folder_ref = input_ph_folder_ref.pk
    
    wfs_phondisp = get_wfs_with_parameter(structure_ref,'PhonondispersionWorkflow')
    wfs = []
    for wf_phondisp in wfs_phondisp:
        if (('ph_calculation' in wf_phondisp.get_results() or 'ph_calculation' in wf_phondisp.get_parameters())
            or ('ph_folder' in wf_phondisp.get_results() or 'ph_folder' in wf_phondisp.get_parameters())
            and (not also_dispersion or 'phonon_dispersion' in wf_phondisp.get_results())):
            params = wf_phondisp.get_parameters()
            replace_all_parameterdata_with_dict(params)
            take_out_keys_from_dictionary(params,ignored_keys+['structure'])
            kpoints = params.pop('pw_kpoints',None)
            qpoints = params.pop('ph_qpoints',None)
            input_pw_calc = params.pop('pw_calculation',0)       
            input_ph_calc = params.pop('ph_calculation',0)
            input_ph_folder = params.pop('ph_folder',0)         
            if kpoints:
                try:
                    kpoints = kpoints.get_kpoints_mesh()
                except AttributeError:
                    kpoints = kpoints.get_kpoints()
            if qpoints:
                try:
                    qpoints = qpoints.get_kpoints_mesh()
                except AttributeError:
                    qpoints = qpoints.get_kpoints()
            if input_pw_calc:
                input_pw_calc = input_pw_calc.pk
            
            if (objects_are_equal(kpoints,kpoints_ref)
                and objects_are_equal(qpoints,qpoints_ref)
                and objects_are_equal(the_params,params)
                and input_pw_calc_ref==input_pw_calc
                and input_ph_calc_ref==input_ph_calc
                and input_ph_folder_ref==input_ph_folder):
                wfs.append(wf_phondisp)
    if wfs:
        return {'Phonondispersion': wfs}
    else:
        if also_pw_restart:
            the_params = deepcopy(wf_params)
            pw_params = {'structure': the_params['structure'], 'pseudo_family': the_params['pseudo_family']}
            for k,v in the_params.iteritems():
                if k.startswith('pw_'):
                    new_k = k[3:] # remove 'pw_'
                    pw_params[new_k] = v
            # pw_params['input'].update({'final_scf_remove_use_all_frac': True})  # always present in the PhonondispersionWf     
            wfs = get_pw_wfs_with_parameters(pw_params,also_bands=False)
            if wfs:
                return {'Pw': wfs} 
            else:
                return {}
        else:
            return {}

def get_starting_magnetization_pw(pw_output):
    """
    From the output of a PW calculation, get the atomic magnetic moment
    per unit charge and build the corresponding restart magnetization
    to be applied to a subsequent calculation.
    :param pw_output: dictionary with the output of a PW calc.
    :return: a dictionary of the form:
        {'starting_magnetization': {specie_name_a: starting mag. for a,
                                    specie_name_b: starting mag. for b}
         'angle1' (optional, for SOC calc.): {specie_name_a: angle1 for a,
                                              specie_name_b: angle1 for b}
         'angle2' (optional, for SOC calc.): {specie_name_a: angle2 for a,
                                              specie_name_b: angle2 for b}
         }
    """
    import numpy as np

    if 'atomic_magnetic_moments' not in pw_output:
        return {}
    
    mag_moments = (np.array(pw_output['atomic_magnetic_moments'])/
                  np.array(pw_output['atomic_charges'])).tolist()
    species_name = pw_output['atomic_species_name']
    start_mag = dict([(kind_name,round(np.average([mom 
                for k,mom in zip(species_name,mag_moments)
                if k==kind_name]),3)) for kind_name in set(species_name)])
    result_dict = {'starting_magnetization': start_mag}
    
    if ('atomic_magnetic_theta' in pw_output and
        'atomic_magnetic_phi' in pw_output):
        theta = pw_output['atomic_magnetic_theta']
        phi = pw_output['atomic_magnetic_phi']
        result_dict['angle1'] = dict([(kind_name,round(np.average([th 
                for k,th in zip(species_name,theta)
                if k==kind_name]),3)) for kind_name in set(species_name)])
        result_dict['angle2'] = dict([(kind_name,round(np.average([ph 
                for k,ph in zip(species_name,phi)
                if k==kind_name]),3)) for kind_name in set(species_name)])        
    
    return result_dict

def get_from_parameterdata_or_dict(params,key,**kwargs):
    """
    Get the value corresponding to a key from an object that can be either
    a ParameterData or a dictionary.
    :param params: a dict or a ParameterData object
    :param key: a key
    :param default: a default value. If not present, and if key is not
        present in params, a KeyError is raised, as in params[key]
    :return: the corresponding value
    """
    if isinstance(params,ParameterData):
        params = params.get_dict()
    
    if 'default' in kwargs:
        return params.get(key,kwargs['default'])
    else:
        return params[key] 
